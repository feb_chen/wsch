package com.farm.iproject.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/* *
 *功能：项目字段类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "Field")
@Table(name = "wsch_p_field")
public class Field implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "STOREIS", length = 1, nullable = false)
	private String storeis;
	@Column(name = "TYPE", length = 32, nullable = false)
	private String type;
	@Column(name = "PROJECTID", length = 32, nullable = false)
	private String projectid;
	@Column(name = "KEYID", length = 128, nullable = false)
	private String keyid;
	@Column(name = "NAME", length = 128, nullable = false)
	private String name;
	@Column(name = "PCONTENT", length = 128)
	private String pcontent;
	@Column(name = "PSTATE", length = 2, nullable = false)
	private String pstate;
	@Column(name = "CUSER", length = 32, nullable = false)
	private String cuser;
	@Column(name = "CTIME", length = 16, nullable = false)
	private String ctime;
	@Column(name = "SORT", length = 10, nullable = false)
	private Integer sort;
	@Column(name = "SEARCHTYPE", length = 2, nullable = false)
	private String searchtype;
	@Column(name = "BOOST", length = 10)
	private Integer boost;
	@Column(name = "TEXTTYPE", length = 2, nullable = false)
	private String texttype;
	@Column(name = "MAXLENGHT", length = 10)
	private Integer maxlenght;
	@Column(name = "SORTABLE", length = 2, nullable = false)
	private String sortable;
	
	public String getSortable() {
		return sortable;
	}

	public void setSortable(String sortable) {
		this.sortable = sortable;
	}

	public Integer getBoost() {
		return boost;
	}

	public String getTexttype() {
		return texttype;
	}

	public void setTexttype(String texttype) {
		this.texttype = texttype;
	}

	public Integer getMaxlenght() {
		return maxlenght;
	}

	public void setMaxlenght(Integer maxlenght) {
		this.maxlenght = maxlenght;
	}

	public void setBoost(Integer boost) {
		this.boost = boost;
	}

	public String getSearchtype() {
		return searchtype;
	}

	public void setSearchtype(String searchtype) {
		this.searchtype = searchtype;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getStoreis() {
		return this.storeis;
	}

	public void setStoreis(String storeis) {
		this.storeis = storeis;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProjectid() {
		return this.projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getKeyid() {
		return this.keyid;
	}

	public void setKeyid(String keyid) {
		this.keyid = keyid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPcontent() {
		return this.pcontent;
	}

	public void setPcontent(String pcontent) {
		this.pcontent = pcontent;
	}

	public String getPstate() {
		return this.pstate;
	}

	public void setPstate(String pstate) {
		this.pstate = pstate;
	}

	public String getCuser() {
		return this.cuser;
	}

	public void setCuser(String cuser) {
		this.cuser = cuser;
	}

	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}