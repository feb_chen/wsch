package com.farm.iproject.service;

import com.farm.iproject.domain.Pdocument;
import com.farm.core.sql.query.DataQuery;

import java.io.IOException;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：业务文档服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface PdocumentServiceInter {

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 * @throws IOException
	 */
	public void delDocument(String projectid, String appid, LoginUser user, boolean isDelIndex) throws IOException;

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Pdocument getPdocumentEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createPdocumentSimpleQuery(DataQuery query);

	/**
	 * 提交一个文档
	 * 
	 * @param projectid
	 * @param paras
	 * @param currentUser
	 * @return
	 * @throws IOException
	 */
	public Pdocument submitDocument(String projectid, String taskkey, Map<String, String> paras, LoginUser currentUser)
			throws IOException;

	/**
	 * 删除记录
	 * 
	 * @param id
	 * @param user
	 * @throws IOException
	 */
	public void deletePdocumentEntity(String id, LoginUser user, boolean isDelIndex) throws IOException;

	/**
	 * 立即创建索引（合并批量创建索引）
	 * 
	 * @param taskkey
	 * @throws IOException
	 */
	public void runIndex(String taskkey, String projectid) throws IOException;

	/**
	 * 单个批量创建索引
	 * 
	 * @param taskkey
	 * @param projectid
	 * @throws IOException
	 */
	public void runIndexBySingle(String taskkey, String projectid) throws IOException;

	/**
	 * 获得文档
	 * 
	 * @param appid
	 * @return
	 */
	public Pdocument getDocument(String appid, String projectid);

	/**
	 * 获得docjson
	 * 
	 * @param doc
	 * @return
	 */
	public Map<String, Object> getDocMap(Pdocument document);

	/**
	 * 获得文档得值（包括虚拟字段，同时对html内容格式化）
	 * 
	 * @param document
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getDocumentInfo(Pdocument document) throws Exception;

	/**
	 * 删除所有的索引（不删除数据记录）
	 * 
	 * @param projectid
	 * @param appkeyid    可空，非空时只删除改业务类型下的索引
	 * @param currentUser
	 * @throws IOException
	 */
	public void deleteAllIndex(String projectid, String appkeyid, LoginUser currentUser) throws IOException;

}