package com.farm.iproject.service.impl;

import com.farm.iproject.domain.Dictype;
import com.farm.core.time.TimeTool;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.farm.iproject.dao.AppdicDaoInter;
import com.farm.iproject.dao.DictypeDaoInter;
import com.farm.iproject.service.DictypeServiceInter;
import com.farm.wsch.index.pojo.ResultDicNum;
import com.farm.wsch.index.pojo.SearchRule;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：字典类型服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class DictypeServiceImpl implements DictypeServiceInter {
	@Resource
	private DictypeDaoInter dictypeDaoImpl;
	@Resource
	private AppdicDaoInter appdicDaoImpl;
	private static final Logger log = Logger.getLogger(DictypeServiceImpl.class);

	@Override
	@Transactional
	public Dictype insertDictypeEntity(Dictype entity, LoginUser user) {
		String parentid = entity.getParentid();
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		if (StringUtils.isBlank(parentid)) {
			// 类型
			entity.setParentid("NONE");
			// entity.setKeyid(entity.getFieldkey());
			entity.setSuperid("NONE");
			entity.setTreecode("NONE");
		} else {
			// 类型子集
			Dictype parent = dictypeDaoImpl.getEntity(parentid);
			entity.setSuperid(parent.getSuperid());
			entity.setPstate("1");
			entity.setTreecode("NONE");
		}
//		if (entity.getSort() == null) {
//			entity.setSort(1);
//		}
		if (StringUtils.isBlank(entity.getPstate())) {
			entity.setPstate("1");
		}
		entity = dictypeDaoImpl.insertEntity(entity);
		if (StringUtils.isBlank(parentid)) {
			entity.setSuperid(entity.getId());
			entity.setTreecode(entity.getId());
		} else {
			Dictype parent = dictypeDaoImpl.getEntity(parentid);
			entity.setTreecode(parent.getTreecode() + entity.getId());
		}
		dictypeDaoImpl.editEntity(entity);
		return entity;
	}

	@Override
	@Transactional
	public Dictype editDictypeEntity(Dictype entity, LoginUser user) {
		Dictype entity2 = dictypeDaoImpl.getEntity(entity.getId());
		entity2.setSuperid(entity.getSuperid());
		entity2.setProjectid(entity.getProjectid());
		entity2.setFieldkey(entity.getFieldkey());
		entity2.setLiketype(entity.getLiketype());
		entity2.setKeyid(entity.getKeyid());
		entity2.setName(entity.getName());
		// entity2.setTreecode(entity.getTreecode());
		// entity2.setParentid(entity.getParentid());
		entity2.setSort(entity.getSort());
		entity2.setSumable(entity.getSumable());
		entity2.setPcontent(entity.getPcontent());
		entity2.setAbletype(entity.getAbletype());
		if (entity2.getParentid().equals("NONE")) {
			entity2.setKeyid(entity.getFieldkey());
			entity2.setPstate(entity.getPstate());
		} else {

		}
		dictypeDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteDictypeEntity(String id, LoginUser user) {
		// 判断是否有子分类引用父分类
//		if (dictypeDaoImpl.countEntitys(DBRuleList.getInstance().add(new DBRule("PARENTID", id, "=")).toList()) > 0) {
//			throw new RuntimeException("有子字典引用该字典,无法删除！");
//		}
		deleteAllSubDictype(id);
		appdicDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("DICID", id, "=")).toList());
		dictypeDaoImpl.deleteEntity(dictypeDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Dictype getDictypeEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return dictypeDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createDictypeSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WSCH_P_DICTYPE",
				"ID,SUPERID,PROJECTID,FIELDKEY,LIKETYPE,KEYID,NAME,TREECODE,PARENTID,SORT,PCONTENT,PSTATE,CUSER,CTIME,ABLETYPE");
		return dbQuery;
	}

	@Override
	@Transactional
	public List<Dictype> getDictypes(String projectid) {
		return dictypeDaoImpl.selectEntitys(DBRuleList.getInstance().add(new DBRule("PSTATE", "1", "="))
				.add(new DBRule("PROJECTID", projectid, "=")).toList());
	}

	@Override
	@Transactional
	public List<Dictype> getRootDictype(String projectid) {
		return dictypeDaoImpl.selectEntitys(DBRuleList.getInstance().add(new DBRule("PSTATE", "1", "="))
				.add(new DBRule("PROJECTID", projectid, "=")).add(new DBRule("PARENTID", "NONE", "=")).toList());
	}

	@Override
	@Transactional
	public List<ResultDicNum> getResultDicNums(String projectid, String appkey) {
		List<ResultDicNum> list = dictypeDaoImpl.getResultDicNums(projectid, appkey);
		return list;
	}

	@Override
	@Transactional
	public SearchRule getSearchRule(String dictypeid) {
		if (StringUtils.isBlank(dictypeid)) {
			return null;
		}
		Dictype type = dictypeDaoImpl.getEntity(dictypeid);
		if (type == null || type.getSuperid().equals(type.getId())) {
			return null;
		}
		Dictype supertype = dictypeDaoImpl.getEntity(type.getSuperid());
		if (supertype == null) {
			return null;
		}
		SearchRule rule = new SearchRule();
		rule.setFieldkey(supertype.getFieldkey());
		if (supertype.getLiketype().equals("1")) {
			rule.setLike(false);
		} else {
			rule.setLike(true);
		}
		rule.setVal(type.getKeyid());
		rule.setSmart(true);
		return rule;
	}

	@Override
	@Transactional
	public Dictype getDictype(String projectid, String keyid) {
		List<Dictype> types = dictypeDaoImpl.selectEntitys(DBRuleList.getInstance()
				.add(new DBRule("projectid", projectid, "=")).add(new DBRule("keyid", keyid.trim(), "=")).toList());
		for (Dictype node : types) {
			return node;
		}
		return null;
	}

	@Override
	@Transactional
	public void deleteAllSubDictype(String dictypeid) {
		dictypeDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("SUPERID", dictypeid, "="))
				.add(new DBRule("ID", dictypeid, "!=")).toList());
	}
}
