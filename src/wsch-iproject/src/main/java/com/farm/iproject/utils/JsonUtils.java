package com.farm.iproject.utils;

import java.util.Map;
import java.util.Map.Entry;

import com.alibaba.fastjson.JSON;

public class JsonUtils {

	public static Map<String, String> toMap(String jsonstr) throws Exception {
		@SuppressWarnings("unchecked")
		Map<String, String> map = (Map<String, String>) JSON.parse(jsonstr);
		for (Entry<String, String> node : map.entrySet()) {
			node.setValue(XmlEx(node.getValue()));
		}
		return map;
	}

	private static String XmlEx(String str) {
		str = str.replace("&lt;", "<");
		str = str.replace("&gt;", ">");
		str = str.replace("&amp;", "&");
		str = str.replace("&aops;", "'");
		str = str.replace("&quot;", "\"");
		return str;
	}

	public static String toJsonStr(Map<String, String> paras) {
		return JSON.toJSONString(paras);
	}
}
