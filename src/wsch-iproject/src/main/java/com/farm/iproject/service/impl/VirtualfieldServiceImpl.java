package com.farm.iproject.service.impl;

import com.farm.iproject.domain.Appmodel;
import com.farm.iproject.domain.Virtualfield;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.farm.iproject.dao.VirtualfieldDaoInter;
import com.farm.iproject.service.AppmodelServiceInter;
import com.farm.iproject.service.VirtualfieldServiceInter;
import com.farm.wsch.index.pojo.IndexResult;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：虚拟字段服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class VirtualfieldServiceImpl implements VirtualfieldServiceInter {
	@Resource
	private VirtualfieldDaoInter virtualfieldDaoImpl;
	@Resource
	private AppmodelServiceInter appModelServiceImpl;
	private static final Logger log = Logger.getLogger(VirtualfieldServiceImpl.class);

	@Override
	@Transactional
	public Virtualfield insertVirtualfieldEntity(Virtualfield entity, LoginUser user) {
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return virtualfieldDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Virtualfield editVirtualfieldEntity(Virtualfield entity, LoginUser user) {
		Virtualfield entity2 = virtualfieldDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setAppmodelid(entity.getAppmodelid());
		entity2.setType(entity.getType());
		entity2.setFormula(entity.getFormula());
		entity2.setName(entity.getName());
		entity2.setId(entity.getId());
		virtualfieldDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteVirtualfieldEntity(String id, LoginUser user) {
		virtualfieldDaoImpl.deleteEntity(virtualfieldDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Virtualfield getVirtualfieldEntity(String id) {
		if (id == null) {
			return null;
		}
		return virtualfieldDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createVirtualfieldSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query, "WSCH_P_VIRTUALFIELD", "ID,APPMODELID,TYPE,FORMULA,NAME");
		return dbQuery;
	}

	@Override
	@Transactional
	public void initVirtualField(IndexResult result, String projectid) {
		Map<String, List<Virtualfield>> map = getAllVirtualField(projectid);
		for (Map<String, Object> document : result.getList()) {
			initVirtualField(document, map);
		}
	}

	@Override
	@Transactional
	public void initVirtualField(Map<String, Object> document, Map<String, List<Virtualfield>> VirtualFields) {
		Map<String, Object> dic = new HashMap<>();
		dic.putAll(document);
		List<Virtualfield> list = VirtualFields.get(document.get("APPKEYID"));
		if (list != null) {
			for (Virtualfield vfield : list) {
				boolean isHaveVal = false;
				String newVal = vfield.getFormula();
				for (Map.Entry<String, Object> unit : dic.entrySet()) {
					if (newVal.indexOf("$[" + unit.getKey() + "]") >= 0
							&& StringUtils.isNotBlank((String) unit.getValue())) {
						isHaveVal = true;
						newVal = newVal.replace("$[" + unit.getKey() + "]", (String) unit.getValue());
					}
				}
				if (isHaveVal) {
					document.put(vfield.getType(), newVal.replace("&amp;", "&"));
				}
			}
		}
	}

	@Override
	@Transactional
	public Map<String, List<Virtualfield>> getAllVirtualField(String projectid) {
		Map<String, List<Virtualfield>> map = new HashMap<String, List<Virtualfield>>();
		List<Appmodel> models = appModelServiceImpl.getAppmodels(projectid);
		for (Appmodel model : models) {
			List<Virtualfield> list = map.get(model.getKeyid());
			if (list == null) {
				list = new ArrayList<>();
			}
			list.addAll(virtualfieldDaoImpl.selectEntitys(
					DBRuleList.getInstance().add(new DBRule("APPMODELID", model.getId(), "=")).toList()));
			map.put(model.getKeyid(), list);
		}
		return map;
	}

	@Override
	@Transactional
	public Virtualfield getVirtualfield(String projectid, String appkey, String type) {
		Appmodel model = appModelServiceImpl.getAppmodel(projectid, appkey);
		if (model != null) {
			List<Virtualfield> field = virtualfieldDaoImpl
					.selectEntitys(DBRuleList.getInstance().add(new DBRule("APPMODELID", model.getId(), "=")).toList());
			for (Virtualfield node : field) {
				if (node.getType().trim().equals(type.trim())) {
					return node;
				}
			}
		}
		return null;
	}

}
