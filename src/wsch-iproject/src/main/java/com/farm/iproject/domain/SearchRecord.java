package com.farm.iproject.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/* *
 *功能：检索记录类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "SearchRecord")
@Table(name = "wsch_r_search")
public class SearchRecord implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "RESULTID", length = 64)
	private String resultid;
	@Column(name = "SEARCHTIME", length = 12)
	private Float searchtime;
	@Column(name = "TIMES", length = 2048)
	private String times;
	@Column(name = "RULES", length = 2048)
	private String rules;
	@Column(name = "WORD", length = 512)
	private String word;
	@Column(name = "CTIME", length = 16, nullable = false)
	private String ctime;
	@Column(name = "CUSER", length = 32)
	private String cuser;
	@Column(name = "PROJECTID", length = 32)
	private String projectid;
	@Column(name = "SIZE", length = 10)
	private Integer size;
	@Column(name = "PSTATE", length = 2, nullable = false)
	private String pstate;
	
	public String getProjectid() {
		return projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getResultid() {
		return this.resultid;
	}

	public void setResultid(String resultid) {
		this.resultid = resultid;
	}

	public Float getSearchtime() {
		return this.searchtime;
	}

	public void setSearchtime(Float searchtime) {
		this.searchtime = searchtime;
	}

	public String getTimes() {
		return this.times;
	}

	public void setTimes(String times) {
		this.times = times;
	}

	public String getRules() {
		return this.rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public String getWord() {
		return this.word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getCuser() {
		return this.cuser;
	}

	public void setCuser(String cuser) {
		this.cuser = cuser;
	}

	public Integer getSize() {
		return this.size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getPstate() {
		return this.pstate;
	}

	public void setPstate(String pstate) {
		this.pstate = pstate;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}