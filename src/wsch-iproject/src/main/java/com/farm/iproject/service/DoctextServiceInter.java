package com.farm.iproject.service;

import com.farm.iproject.domain.Doctext;
import com.farm.iproject.domain.Field;
import com.farm.iproject.domain.Pdocument;
import com.farm.core.sql.query.DataQuery;

import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：大文本服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface DoctextServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Doctext insertDoctextEntity(Doctext entity, LoginUser user);

	public Doctext insertText(Map<String, String> paras, Map<String, Field> fieldDic,String projectid, LoginUser currentUser);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Doctext editDoctextEntity(Doctext entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteDoctextEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Doctext getDoctextEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createDoctextSimpleQuery(DataQuery query);

	/**
	 * @param textid
	 * @return
	 */
	public Map<String, Object> getDocMap(Pdocument document, boolean isOtext_Html);

}