package com.farm.iproject.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：扩展字典类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "Wordex")
@Table(name = "wsch_s_wordex")
public class Wordex implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "TYPE", length = 2, nullable = false)
        private String type;
        @Column(name = "WORD", length = 64, nullable = false)
        private String word;
        @Column(name = "PSTATE", length = 2, nullable = false)
        private String pstate;
        @Column(name = "PCONTENT", length = 128)
        private String pcontent;
        @Column(name = "CTIME", length = 16, nullable = false)
        private String ctime;
        @Column(name = "CUSER", length = 32, nullable = false)
        private String cuser;

        public String  getType() {
          return this.type;
        }
        public void setType(String type) {
          this.type = type;
        }
        public String  getWord() {
          return this.word;
        }
        public void setWord(String word) {
          this.word = word;
        }
        public String  getPstate() {
          return this.pstate;
        }
        public void setPstate(String pstate) {
          this.pstate = pstate;
        }
        public String  getPcontent() {
          return this.pcontent;
        }
        public void setPcontent(String pcontent) {
          this.pcontent = pcontent;
        }
        public String  getCtime() {
          return this.ctime;
        }
        public void setCtime(String ctime) {
          this.ctime = ctime;
        }
        public String  getCuser() {
          return this.cuser;
        }
        public void setCuser(String cuser) {
          this.cuser = cuser;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}