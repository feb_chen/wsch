package com.farm.iproject.controller;

import com.farm.iproject.domain.Field;
import com.farm.iproject.domain.Project;
import com.farm.iproject.service.FieldServiceInter;
import com.farm.iproject.service.ProjectServiceInter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import com.farm.wsch.index.WschIndexServerInter.FieldType;

import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：项目字段控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/field")
@Controller
public class FieldController extends WebUtils {
	private final static Logger log = Logger.getLogger(FieldController.class);
	@Resource
	private FieldServiceInter fieldServiceImpl;
	@Resource
	private ProjectServiceInter projectServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(String projectid, DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("projectid", projectid, "="));
			query.addDefaultSort(new DBSort("A.SORT", "ASC"));
			DataResult result = fieldServiceImpl.createFieldSimpleQuery(query).search();
			result.runDictionary("1:是,0:否", "STOREIS");
			result.runDictionary("0:无,1:关键字检索", "SEARCHTYPE");
			result.runDictionary("0:无,1:支持排序", "SORTABLE");
			result.runDictionary("1:文本,2:HTML", "TEXTTYPE");
			result.runDictionary(FieldType.getDic(), "TYPE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Field entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = fieldServiceImpl.editFieldEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Field entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = fieldServiceImpl.insertFieldEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				fieldServiceImpl.deleteFieldEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(String projectid, HttpSession session) {
		return ViewMode.getInstance().putAttr("projectid", projectid).returnModelAndView("iproject/FieldResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(String projectid, RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				Field field = fieldServiceImpl.getFieldEntity(ids);
				Project project = projectServiceImpl.getProjectEntity(field.getProjectid());
				return ViewMode.getInstance().putAttr("project", project).putAttr("pageset", pageset)
						.putAttr("entity", field).putAttr("types", FieldType.getList())
						.returnModelAndView("iproject/FieldForm");
			}
			case (1): {// 新增
				Project project = projectServiceImpl.getProjectEntity(projectid);
				return ViewMode.getInstance().putAttr("project", project).putAttr("pageset", pageset)
						.putAttr("types", FieldType.getList()).returnModelAndView("iproject/FieldForm");
			}
			case (2): {// 修改
				Field field = fieldServiceImpl.getFieldEntity(ids);
				Project project = projectServiceImpl.getProjectEntity(field.getProjectid());
				return ViewMode.getInstance().putAttr("project", project).putAttr("pageset", pageset)
						.putAttr("types", FieldType.getList()).putAttr("entity", field)
						.returnModelAndView("iproject/FieldForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().putAttr("types", FieldType.getList())
					.returnModelAndView("iproject/FieldForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("iproject/FieldForm");
		}
	}
}
