package com.farm.iproject.service;

import com.farm.iproject.domain.VisitRecord;
import com.farm.core.sql.query.DataQuery;

import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：访问记录服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface VisitrecordServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public VisitRecord insertVisitrecordEntity(VisitRecord entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public VisitRecord editVisitrecordEntity(VisitRecord entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteVisitrecordEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public VisitRecord getVisitrecordEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createVisitrecordSimpleQuery(DataQuery query);

	/**
	 * 记录
	 * 
	 * @param appid
	 * @throws Exception
	 */
	public void record(String appid, String projectid, String resultid, LoginUser user) throws Exception;

	/**
	 * 获得最热文档
	 * 
	 * @param projectid
	 * @param num
	 * @return
	 */
	public List<Map<String, Object>> getHotDoc(String projectid, int num, String userid);
}