package com.farm.iproject.dao;

import com.farm.iproject.domain.PopGroup;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* *
 *功能：权限组数据库持久层接口
 *详细：
 *
 *版本：v0.1
 *作者：Farm代码工程自动生成
 *日期：20150707114057
 *说明：
 */
public interface PopgroupDaoInter {
	/**
	 * 删除一个权限组实体
	 * 
	 * @param entity
	 *            实体
	 */
	public void deleteEntity(PopGroup popgroup);

	/**
	 * 由权限组id获得一个权限组实体
	 * 
	 * @param id
	 * @return
	 */
	public PopGroup getEntity(String popgroupid);

	/**
	 * 插入一条权限组数据
	 * 
	 * @param entity
	 */
	public PopGroup insertEntity(PopGroup popgroup);

	/**
	 * 获得记录数量
	 * 
	 * @return
	 */
	public int getAllListNum();

	/**
	 * 修改一个权限组记录
	 * 
	 * @param entity
	 */
	public void editEntity(PopGroup popgroup);

	/**
	 * 获得一个session
	 */
	public Session getSession();

	/**
	 * 执行一条权限组查询语句
	 */
	public DataResult runSqlQuery(DataQuery query);

	/**
	 * 条件删除权限组实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param rules
	 *            删除条件
	 */
	public void deleteEntitys(List<DBRule> rules);

	/**
	 * 条件查询权限组实体，依据对象字段值,当rules为空时查询全部(一般不建议使用该方法)
	 * 
	 * @param rules
	 *            查询条件
	 * @return
	 */
	public List<PopGroup> selectEntitys(List<DBRule> rules);

	/**
	 * 条件修改权限组实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param values
	 *            被修改的键值对
	 * @param rules
	 *            修改条件
	 */
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules);

	/**
	 * 条件合计权限组:count(*)
	 * 
	 * @param rules
	 *            统计条件
	 */
	public int countEntitys(List<DBRule> rules);

	/**获得用户检索权限KEY（包括匿名用户权限）
	 * @param projectid
	 * @param userid
	 * @return
	 */
	public Set<String> getUserSearchKeys(String projectid, String userid);
}