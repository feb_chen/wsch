package com.farm.iproject.controller;

import com.farm.iproject.domain.Project;
import com.farm.iproject.domain.Weburl;
import com.farm.iproject.service.WeburlServiceInter;
import com.farm.sfile.enums.FileModel;
import com.farm.sfile.service.FileBaseServiceInter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：友情链接控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/weburl")
@Controller
public class WeburlController extends WebUtils {
	private final static Logger log = Logger.getLogger(WeburlController.class);
	@Resource
	private WeburlServiceInter webUrlServiceImpl;

	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DataResult result = webUrlServiceImpl.createWeburlSimpleQuery(query).search();
			result.runDictionary("1:可用,0:禁用", "PSTATE");
			result.runDictionary("1:底部,2:顶部前,3:顶部后", "TYPE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Weburl entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = webUrlServiceImpl.editWeburlEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Weburl entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = webUrlServiceImpl.insertWeburlEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				webUrlServiceImpl.deleteWeburlEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("iproject/WeburlResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				Weburl weburl = webUrlServiceImpl.getWeburlEntity(ids);
				String imgurl = null;
				if (StringUtils.isNotBlank(weburl.getFileid())) {
					imgurl = fileBaseServiceImpl.getDownloadUrl(weburl.getFileid(), FileModel.IMG);
				}
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("imgurl", imgurl)
						.putAttr("entity", weburl).returnModelAndView("iproject/WeburlForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset).returnModelAndView("iproject/WeburlForm");
			}
			case (2): {// 修改

				Weburl weburl = webUrlServiceImpl.getWeburlEntity(ids);
				String imgurl = null;
				if (StringUtils.isNotBlank(weburl.getFileid())) {
					imgurl = fileBaseServiceImpl.getDownloadUrl(weburl.getFileid(), FileModel.IMG);
				}

				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("imgurl", imgurl)
						.putAttr("entity", weburl).returnModelAndView("iproject/WeburlForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("iproject/WeburlForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("iproject/WeburlForm");
		}
	}
}
