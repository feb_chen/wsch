package com.farm.iproject.service.impl;

import com.farm.iproject.domain.Pdocument;
import com.farm.iproject.domain.VisitRecord;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.iproject.dao.VisitrecordDaoInter;
import com.farm.iproject.service.PdocumentServiceInter;
import com.farm.iproject.service.PopgroupServiceInter;
import com.farm.iproject.service.VisitrecordServiceInter;
import com.farm.web.WebUtils;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：访问记录服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class VisitrecordServiceImpl implements VisitrecordServiceInter {
	@Resource
	private VisitrecordDaoInter visitrecordDaoImpl;
	@Resource
	private PdocumentServiceInter pDocumentServiceImpl;
	@Resource
	private PopgroupServiceInter popGroupServiceImpl;
	private static final Logger log = Logger.getLogger(VisitrecordServiceImpl.class);

	@Override
	@Transactional
	public VisitRecord insertVisitrecordEntity(VisitRecord entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return visitrecordDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public VisitRecord editVisitrecordEntity(VisitRecord entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		VisitRecord entity2 = visitrecordDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setImg(entity.getImg());
		entity2.setOlink(entity.getOlink());
		entity2.setAppid(entity.getAppid());
		entity2.setResultid(entity.getResultid());
		entity2.setTitle(entity.getTitle());
		entity2.setCtime(entity.getCtime());
		entity2.setCuser(entity.getCuser());
		entity2.setId(entity.getId());
		visitrecordDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteVisitrecordEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		visitrecordDaoImpl.deleteEntity(visitrecordDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public VisitRecord getVisitrecordEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return visitrecordDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createVisitrecordSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query, "WSCH_R_VISIT A LEFT JOIN ALONE_AUTH_USER B ON A.CUSER=B.ID",
				"A.ID AS ID,A.APPID AS APPID,A.RESULTID AS RESULTID,A.TITLE AS TITLE,A.CTIME AS CTIME,A.CUSER AS CUSER,B.NAME AS USERNAME");
		return dbQuery;
	}

	@Override
	@Transactional
	public void record(String appid, String projectid, String resultid, LoginUser user) throws Exception {
		Pdocument document = pDocumentServiceImpl.getDocument(appid, projectid);
		Map<String, Object> info = pDocumentServiceImpl.getDocumentInfo(document);
		VisitRecord record = new VisitRecord();
		record.setAppid(appid);
		record.setCtime(TimeTool.getTimeDate14());
		record.setProjectid(document.getProjectid());
		record.setCuser(user != null ? user.getId() : null);
		record.setResultid(resultid);
		record.setTitle(format512((String) info.get("TITLE")));
		record.setImg(format512((String) info.get("IMG")));
		record.setOlink(format512((String) info.get("OLINK")));
		visitrecordDaoImpl.insertEntity(record);
	}

	private String format512(String text) {
		if (text != null && text.length() > 500) {
			text = text.substring(0, 500);
		}
		return text;
	}

	@Override
	@Transactional
	public List<Map<String, Object>> getHotDoc(String projectid, int size, String userid) {
		List<Map<String, Object>> hots = visitrecordDaoImpl.getHotDoc(projectid);
		Set<String> userkeys = popGroupServiceImpl.getUserSearchKeys(projectid, userid);
		Collections.sort(hots, new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				return ((BigInteger) o2.get("NUM")).intValue() - ((BigInteger) o1.get("NUM")).intValue();
			}
		});
		List<Map<String, Object>> words = new ArrayList<Map<String, Object>>();
		for (int n = 0; n < hots.size() && n < size; n++) {
			String popkeys = (String) hots.get(n).get("POPKEYS");
			for (String key : WebUtils.parseIds(popkeys)) {
				if (userkeys.contains(key) || key.equals("ALLABLE")) {
					words.add(hots.get(n));
					break;
				}
			}
		}
		return words;
	}

}
