package com.farm.iproject.controller;

import com.farm.iproject.domain.Appmodel;
import com.farm.iproject.domain.Field;
import com.farm.iproject.domain.Virtualfield;
import com.farm.iproject.service.AppmodelServiceInter;
import com.farm.iproject.service.FieldServiceInter;
import com.farm.iproject.service.ProjectServiceInter;
import com.farm.iproject.service.VirtualfieldServiceInter;
import com.farm.iproject.service.VirtualfieldServiceInter.SearchType;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.DataResults;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：虚拟字段控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/virtualfield")
@Controller
public class VirtualfieldController extends WebUtils {
	private final static Logger log = Logger.getLogger(VirtualfieldController.class);
	@Resource
	private VirtualfieldServiceInter virtualFieldServiceImpl;
	@Resource
	private AppmodelServiceInter appModelServiceImpl;
	@Resource
	private ProjectServiceInter projectServiceImpl;
	@Resource
	private FieldServiceInter fieldServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(String appmodelid, DataQuery query, HttpServletRequest request) {
		try {
			if (StringUtils.isBlank(appmodelid)) {
				return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(DataResult.getInstance()))
						.returnObjMode();
			}
			query = EasyUiUtils.formatGridQuery(request, query);

			query.addRule(new DBRule("appmodelid", appmodelid, "="));

			DataResult result = virtualFieldServiceImpl.createVirtualfieldSimpleQuery(query).search();
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Virtualfield entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = virtualFieldServiceImpl.editVirtualfieldEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Virtualfield entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = virtualFieldServiceImpl.insertVirtualfieldEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				virtualFieldServiceImpl.deleteVirtualfieldEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("iproject/VirtualfieldResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(String appmodelid, RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				Virtualfield virtualfield = virtualFieldServiceImpl.getVirtualfieldEntity(ids);
				Appmodel appmodel = appModelServiceImpl.getAppmodelEntity(virtualfield.getAppmodelid());
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("appmodel", appmodel)
						.putAttr("types", SearchType.getList()).putAttr("entity", virtualfield)
						.returnModelAndView("iproject/VirtualfieldForm");
			}
			case (1): {// 新增
				Appmodel appmodel = appModelServiceImpl.getAppmodelEntity(appmodelid);
				List<Field> fields = fieldServiceImpl.getFields(appmodel.getProjectid());
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("appmodel", appmodel)
						.putAttr("types", SearchType.getList()).putAttr("fields", fields)
						.returnModelAndView("iproject/VirtualfieldForm");
			}
			case (2): {// 修改
				Virtualfield virtualfield = virtualFieldServiceImpl.getVirtualfieldEntity(ids);
				Appmodel appmodel = appModelServiceImpl.getAppmodelEntity(virtualfield.getAppmodelid());
				List<Field> fields = fieldServiceImpl.getFields(appmodel.getProjectid());
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("appmodel", appmodel)
						.putAttr("types", SearchType.getList()).putAttr("fields", fields)
						.putAttr("entity", virtualfield).returnModelAndView("iproject/VirtualfieldForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("iproject/VirtualfieldForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e)
					.returnModelAndView("iproject/VirtualfieldForm");
		}
	}
}
