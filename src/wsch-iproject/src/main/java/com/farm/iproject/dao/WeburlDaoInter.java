package com.farm.iproject.dao;

import com.farm.iproject.domain.Weburl;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;


/* *
 *功能：友情链接数据库持久层接口
 *详细：
 *
 *版本：v0.1
 *作者：Farm代码工程自动生成
 *日期：20150707114057
 *说明：
 */
public interface WeburlDaoInter  {
 /** 删除一个友情链接实体
 * @param entity 实体
 */
 public void deleteEntity(Weburl weburl) ;
 /** 由友情链接id获得一个友情链接实体
 * @param id
 * @return
 */
 public Weburl getEntity(String weburlid) ;
 /** 插入一条友情链接数据
 * @param entity
 */
 public  Weburl insertEntity(Weburl weburl);
 /** 获得记录数量
 * @return
 */
 public int getAllListNum();
 /**修改一个友情链接记录
 * @param entity
 */
 public void editEntity(Weburl weburl);
 /**获得一个session
 */
 public Session getSession();
 /**执行一条友情链接查询语句
 */
 public DataResult runSqlQuery(DataQuery query);
 /**
 * 条件删除友情链接实体，依据对象字段值(一般不建议使用该方法)
 * 
 * @param rules
 *            删除条件
 */
 public void deleteEntitys(List<DBRule> rules);

 /**
 * 条件查询友情链接实体，依据对象字段值,当rules为空时查询全部(一般不建议使用该方法)
 * 
 * @param rules
 *            查询条件
 * @return
 */
 public List<Weburl> selectEntitys(List<DBRule> rules);

 /**
 * 条件修改友情链接实体，依据对象字段值(一般不建议使用该方法)
 * 
 * @param values
 *            被修改的键值对
 * @param rules
 *            修改条件
 */
 public void updataEntitys(Map<String, Object> values, List<DBRule> rules);
 /**
 * 条件合计友情链接:count(*)
 * 
 * @param rules
 *            统计条件
 */
 public int countEntitys(List<DBRule> rules);
}