package com.farm.iproject.service.impl;

import com.farm.iproject.domain.Weburl;
import com.farm.core.time.TimeTool;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.farm.iproject.dao.WeburlDaoInter;
import com.farm.iproject.service.WeburlServiceInter;
import com.farm.sfile.enums.FileModel;
import com.farm.sfile.service.FileBaseServiceInter;
import com.farm.util.cache.FarmCacheGenerater;
import com.farm.util.cache.FarmCacheName;
import com.farm.util.cache.FarmCaches;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：友情链接服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class WeburlServiceImpl implements WeburlServiceInter {
	@Resource
	private WeburlDaoInter weburlDaoImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	private static final Logger log = Logger.getLogger(WeburlServiceImpl.class);

	@Override
	@Transactional
	public Weburl insertWeburlEntity(Weburl entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setEuser(user.getId());
		entity.setEtime(TimeTool.getTimeDate14());
		if (StringUtils.isNotBlank(entity.getFileid())) {
			try {
				fileBaseServiceImpl.submitFile(entity.getFileid());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return weburlDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Weburl editWeburlEntity(Weburl entity, LoginUser user) {
		Weburl entity2 = weburlDaoImpl.getEntity(entity.getId());
		entity2.setType(entity.getType());
		entity2.setSort(entity.getSort());
		if (StringUtils.isNotBlank(entity.getFileid())) {
			entity2.setFileid(entity.getFileid());
			try {
				fileBaseServiceImpl.submitFile(entity.getFileid());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			if (StringUtils.isNotBlank(entity2.getFileid())) {
				fileBaseServiceImpl.freeFile(entity2.getFileid());
			}
			entity2.setFileid(null);
		}
		entity2.setWebname(entity.getWebname());
		entity2.setUrl(entity.getUrl());
		entity2.setPcontent(entity.getPcontent());
		entity2.setPstate(entity.getPstate());
		entity2.setEuser(user.getId());
		entity2.setEtime(TimeTool.getTimeDate14());
		weburlDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteWeburlEntity(String id, LoginUser user) {
		Weburl weburl = weburlDaoImpl.getEntity(id);
		if (StringUtils.isNotBlank(weburl.getFileid())) {
			fileBaseServiceImpl.freeFile(weburl.getFileid());
		}
		weburlDaoImpl.deleteEntity(weburl);
	}

	@Override
	@Transactional
	public Weburl getWeburlEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return weburlDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createWeburlSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query, "WSCH_A_WEBURL",
				"ID,TYPE,SORT,FILEID,CUSER,WEBNAME,URL,PCONTENT,PSTATE,EUSER,ETIME,CTIME");
		FarmCaches.getInstance().clearCache(FarmCacheName.wschWeburls);
		return dbQuery;
	}

	@Override
	@Transactional
	public List<Map<String, Object>> getWebUrlList() {
		// IMGURL,LINKURL,NAME,TYPE
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> urls = (List<Map<String, Object>>) FarmCaches.getInstance().getCacheData("ALLWEBURL",
				new FarmCacheGenerater() {
					@Override
					public Object generateData() {
						List<Map<String, Object>> backlist = new ArrayList<Map<String, Object>>();
						List<Weburl> list = weburlDaoImpl
								.selectEntitys(DBRuleList.getInstance().add(new DBRule("PSTATE", "1", "=")).toList());
						for (Weburl node : list) {
							Map<String, Object> map = new HashMap<String, Object>();
							if (StringUtils.isNotBlank(node.getFileid())) {
								try {
									map.put("IMGURL",
											fileBaseServiceImpl.getDownloadUrl(node.getFileid(), FileModel.IMG));
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							map.put("LINKURL", node.getUrl());
							map.put("NAME", node.getWebname());
							map.put("TYPE", node.getType());
							backlist.add(map);
						}
						return backlist;
					}
				}, FarmCacheName.wschWeburls);
		return urls;
	}

}
