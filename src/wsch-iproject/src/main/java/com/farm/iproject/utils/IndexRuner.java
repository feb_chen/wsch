package com.farm.iproject.utils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.iproject.service.DoctextServiceInter;
import com.farm.iproject.service.FieldServiceInter;
import com.farm.iproject.service.PdocumentServiceInter;
import com.farm.iproject.service.ProjectServiceInter;
import com.farm.util.spring.BeanFactory;
import com.farm.wsch.index.WschIndexServerInter;
import com.farm.wsch.index.pojo.IndexDocument;

public class IndexRuner {
	private ProjectServiceInter projectServiceImpl = (ProjectServiceInter) BeanFactory.getBean("projectServiceImpl");
	private DoctextServiceInter docTextServiceImpl = (DoctextServiceInter) BeanFactory.getBean("doctextServiceImpl");
	private FieldServiceInter fieldServiceImpl = (FieldServiceInter) BeanFactory.getBean("fieldServiceImpl");
	private PdocumentServiceInter pDocumentServiceImpl = (PdocumentServiceInter) BeanFactory
			.getBean("pdocumentServiceImpl");

	private int pagesize = 500;
	private String process;

	private static IndexRuner OBJ;

	private IndexRuner() {
	}

	public static IndexRuner getInstance() {
		if (OBJ == null) {
			OBJ = new IndexRuner();
		}
		return OBJ;
	}

	public String getProcess() {
		return process;
	}

	public void run(String projectid) throws SQLException {
		// 实现runnable借口，创建多线程并启动
		if (process == null) {
			process = "准备开始";
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						int allpage = 1;
						int currentPage = 1;

						while (currentPage <= allpage) {
							DataQuery query = DataQuery.getInstance(currentPage, "APPID,TEXTID,PROJECTID,APPKEYID",
									"WSCH_P_DOCUMENT");
							query.setPagesize(pagesize);
							query.addRule(new DBRule("PROJECTID", projectid, "="));
							DataResult result;

							result = query.search();

							allpage = result.getTotalPage();
							currentPage = result.getCurrentPage() + 1;
							try {
								doMoreIndex(result.getResultList(), currentPage, result.getTotalSize(), projectid);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					} finally {
						process = null;
					}
				}
			}) {
			}.start();
		} else {
			throw new RuntimeException("正在执行任务中!");
		}
	}

	private void doMoreIndex(List<Map<String, Object>> docs, int page, int allsize, String projectid)
			throws IOException {
		synchronized (WschIndexServerInter.class) {
			WschIndexServerInter indexServer = null;
			try {
				indexServer = projectServiceImpl.getWschIndexServer(projectid);
				int n = 0;
				for (Map<String, Object> doc : docs) {
					n++;
					process = ((pagesize * (page - 1 - 1)) + n) + "/" + (allsize);
					try {
						Map<String, Object> map = docTextServiceImpl.getDocMap(
								pDocumentServiceImpl.getDocument((String) doc.get("APPID"), projectid), false);
						IndexDocument idoc = fieldServiceImpl.getIndexDocument((String) doc.get("PROJECTID"),
								(String) doc.get("APPKEYID"), map);
						indexServer.updateDocument(idoc);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} finally {
				indexServer.closeAndSubmit();
			}
		}
	}
}
