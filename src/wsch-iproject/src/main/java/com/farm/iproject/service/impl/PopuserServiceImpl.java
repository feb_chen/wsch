package com.farm.iproject.service.impl;

import com.farm.iproject.domain.PopGroup;
import com.farm.iproject.domain.PopGroupkey;
import com.farm.iproject.domain.PopUser;
import com.farm.core.time.TimeTool;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.farm.iproject.dao.PopgroupDaoInter;
import com.farm.iproject.dao.PopuserDaoInter;
import com.farm.iproject.service.PopuserServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

import com.farm.authority.domain.User;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：权限用户服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class PopuserServiceImpl implements PopuserServiceInter {
	@Resource
	private PopuserDaoInter popuserDaoImpl;
	@Resource
	private PopgroupDaoInter popgroupDaoImpl;
	@Resource
	private UserServiceInter userServiceImpl;
	private static final Logger log = Logger.getLogger(PopuserServiceImpl.class);

	@Override
	@Transactional
	public PopUser insertPopuserEntity(PopUser entity, LoginUser user) {
		// User cuser = userServiceImpl.getUserByLoginName(entity.getUserid());
		// if (cuser == null) {
		// cuser = userServiceImpl.getUserEntity(entity.getUserid());
		// }
		// if (cuser == null && !entity.getUserid().equals("ANONYMOUS")) {
		// throw new RuntimeException("用户" + entity.getUserid() + "未找到！");
		// }
		// PopGroup group = popgroupDaoImpl.getEntity(entity.getGroupid());
		// if (entity.getUserid().equals("ANONYMOUS")) {
		// entity.setUserid("ANONYMOUS");
		// } else {
		// entity.setUserid(cuser.getId());
		// }
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setProjectid(group.getProjectid());
		// entity.setPstate("1");
		return popuserDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public PopUser editPopuserEntity(PopUser entity, LoginUser user) {
		PopUser entity2 = popuserDaoImpl.getEntity(entity.getId());
		// // entity2.setEuser(user.getId());
		// // entity2.setEusername(user.getName());
		// // entity2.setEtime(TimeTool.getTimeDate14());
		// entity2.setProjectid(entity.getProjectid());
		// entity2.setGroupid(entity.getGroupid());
		// entity2.setUserid(entity.getUserid());
		// entity2.setPstate(entity.getPstate());
		// entity2.setCtime(entity.getCtime());
		// entity2.setPcontent(entity.getPcontent());
		// entity2.setId(entity.getId());
		popuserDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deletePopuserEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		popuserDaoImpl.deleteEntity(popuserDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public PopUser getPopuserEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return popuserDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createPopuserSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"WSCH_A_POPUSER a left join WSCH_A_POPGOURP b on a.groupid=b.id left join alone_auth_user c on c.id=a.userid",
				"a.ID as ID,b.groupname as groupname,c.name as username,a.PROJECTID as PROJECTID,a.GROUPID as GROUPID,C.LOGINNAME as LOGINNAME,a.USERID as USERID,a.PSTATE as PSTATE,a.CTIME as CTIME,a.PCONTENT as PCONTENT");
		return dbQuery;
	}

	@Override
	@Transactional
	public PopUser addGroupUser(String groupid, String userid) {
		if (userid.equals("ANONYMOUS")) {

		} else {
			User cuser = userServiceImpl.getUserByLoginName(userid);
			if (cuser == null) {
				cuser = userServiceImpl.getUserEntity(userid);
			}
			if (cuser == null) {
				throw new RuntimeException("用户" + userid + "未找到！");
			}
			userid = cuser.getId();
		}
		if (StringUtils.isNotBlank(groupid) && StringUtils.isNotBlank(userid)) {
			popuserDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("GROUPID", groupid, "="))
					.add(new DBRule("USERID", userid, "=")).toList());
		}
		PopGroup group = popgroupDaoImpl.getEntity(groupid);
		PopUser entity = new PopUser();
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setGroupid(groupid);
		entity.setProjectid(group.getProjectid());
		entity.setUserid(userid);
		entity.setPstate("1");
		return popuserDaoImpl.insertEntity(entity);
	}

}
