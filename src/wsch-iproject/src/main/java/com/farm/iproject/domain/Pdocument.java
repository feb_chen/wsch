package com.farm.iproject.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/* *
 *功能：业务文档类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "PDocument")
@Table(name = "wsch_p_document")
public class Pdocument implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "APPID", length = 32, nullable = false)
	private String appid;
	@Column(name = "TASKKEY", length = 64, nullable = false)
	private String taskkey;
	@Column(name = "APPKEYID", length = 64, nullable = false)
	private String appkeyid;
	@Column(name = "PROJECTID", length = 32, nullable = false)
	private String projectid;
	@Column(name = "PSTATE", length = 2, nullable = false)
	private String pstate;
	@Column(name = "PCONTENT", length = 128)
	private String pcontent;
	@Column(name = "CUSER", length = 32, nullable = false)
	private String cuser;
	@Column(name = "CTIME", length = 16, nullable = false)
	private String ctime;
	@Column(name = "TEXTID", length = 32, nullable = false)
	private String textid;
	@Column(name = "POPKEYS", length = 512, nullable = false)
	private String popkeys;

	public String getPopkeys() {
		return popkeys;
	}

	public void setPopkeys(String popkeys) {
		this.popkeys = popkeys;
	}

	public String getTextid() {
		return textid;
	}

	public void setTextid(String textid) {
		this.textid = textid;
	}

	public String getAppid() {
		return this.appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getTaskkey() {
		return this.taskkey;
	}

	public void setTaskkey(String taskkey) {
		this.taskkey = taskkey;
	}

	public String getAppkeyid() {
		return appkeyid;
	}

	public void setAppkeyid(String appkeyid) {
		this.appkeyid = appkeyid;
	}

	public String getProjectid() {
		return this.projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getPstate() {
		return this.pstate;
	}

	public void setPstate(String pstate) {
		this.pstate = pstate;
	}

	public String getPcontent() {
		return this.pcontent;
	}

	public void setPcontent(String pcontent) {
		this.pcontent = pcontent;
	}

	public String getCuser() {
		return this.cuser;
	}

	public void setCuser(String cuser) {
		this.cuser = cuser;
	}

	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}