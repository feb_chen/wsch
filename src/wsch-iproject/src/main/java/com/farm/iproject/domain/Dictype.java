package com.farm.iproject.domain;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/* *
 *功能：字典类型类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "DicType")
@Table(name = "wsch_p_dictype")
public class Dictype implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "SUPERID", length = 32, nullable = false)
	private String superid;
	@Column(name = "PROJECTID", length = 32, nullable = false)
	private String projectid;
	@Column(name = "FIELDKEY", length = 128)
	private String fieldkey;
	@Column(name = "LIKETYPE", length = 2)
	private String liketype;
	@Column(name = "KEYID", length = 32, nullable = false)
	private String keyid;
	@Column(name = "NAME", length = 32, nullable = false)
	private String name;
	@Column(name = "TREECODE", length = 256, nullable = false)
	private String treecode;
	@Column(name = "PARENTID", length = 32, nullable = false)
	private String parentid;
	@Column(name = "SORT", length = 10, nullable = false)
	private Integer sort;
	@Column(name = "PCONTENT", length = 128)
	private String pcontent;
	@Column(name = "PSTATE", length = 2, nullable = false)
	private String pstate;
	@Column(name = "CUSER", length = 32, nullable = false)
	private String cuser;
	@Column(name = "CTIME", length = 16, nullable = false)
	private String ctime;
	@Column(name = "ABLETYPE", length = 2)
	private String abletype;
	@Column(name = "SUMABLE", length = 2)
	private String sumable;

	@Transient
	private long num;
	@Transient
	private List<Map<String, Object>> treeData;
	@Transient
	private String treeDataJson;

	public String getSumable() {
		return sumable;
	}

	public void setSumable(String sumable) {
		this.sumable = sumable;
	}

	public String getSuperid() {
		return this.superid;
	}

	public void setSuperid(String superid) {
		this.superid = superid;
	}

	public String getProjectid() {
		return this.projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getFieldkey() {
		return this.fieldkey;
	}

	public void setFieldkey(String fieldkey) {
		this.fieldkey = fieldkey;
	}

	public String getLiketype() {
		return this.liketype;
	}

	public void setLiketype(String liketype) {
		this.liketype = liketype;
	}

	public String getKeyid() {
		return this.keyid;
	}

	public void setKeyid(String keyid) {
		this.keyid = keyid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTreecode() {
		return this.treecode;
	}

	public void setTreecode(String treecode) {
		this.treecode = treecode;
	}

	public String getParentid() {
		return this.parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getPcontent() {
		return this.pcontent;
	}

	public void setPcontent(String pcontent) {
		this.pcontent = pcontent;
	}

	public String getPstate() {
		return this.pstate;
	}

	public void setPstate(String pstate) {
		this.pstate = pstate;
	}

	public String getCuser() {
		return this.cuser;
	}

	public void setCuser(String cuser) {
		this.cuser = cuser;
	}

	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAbletype() {
		return this.abletype;
	}

	public void setAbletype(String abletype) {
		this.abletype = abletype;
	}

	public long getNum() {
		return num;
	}

	public void setNum(long num) {
		this.num = num;
	}

	public List<Map<String, Object>> getTreeData() {
		return treeData;
	}

	public void setTreeData(List<Map<String, Object>> treeData) {
		this.treeData = treeData;
	}

	public String getTreeDataJson() {
		return treeDataJson;
	}

	public void setTreeDataJson(String treeDataJson) {
		this.treeDataJson = treeDataJson;
	}

}