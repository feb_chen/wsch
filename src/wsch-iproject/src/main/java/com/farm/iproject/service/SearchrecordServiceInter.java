package com.farm.iproject.service;

import com.farm.iproject.domain.SearchRecord;
import com.farm.wsch.index.pojo.IndexResult;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：检索记录服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface SearchrecordServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public SearchRecord insertSearchrecordEntity(SearchRecord entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public SearchRecord editSearchrecordEntity(SearchRecord entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteSearchrecordEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public SearchRecord getSearchrecordEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createSearchrecordSimpleQuery(DataQuery query);

	/**
	 * 记录
	 * 
	 * @param result
	 * @param currentUser
	 */
	public void record(String projectid, String word, IndexResult result, LoginUser currentUser);

	/**
	 * 获得检索热词
	 * 黑名单HOTWORD_BLACK数据字典
	 * 白名单HOTWORD_WHITE数据字典
	 * (数据字典值格式：<关键字（展示名称）：项目KEY|项目ID|ALL（字典项值）>)
	 * @param projectid
	 * @param i
	 * @return
	 */
	public List<String> getHotWord(String projectid,String projectKey, int i);
}