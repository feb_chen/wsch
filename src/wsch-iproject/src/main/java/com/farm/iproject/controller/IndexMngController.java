package com.farm.iproject.controller;

import com.farm.iproject.domain.Appmodel;
import com.farm.iproject.domain.Field;
import com.farm.iproject.domain.Pdocument;
import com.farm.iproject.domain.Project;
import com.farm.iproject.service.AppmodelServiceInter;
import com.farm.iproject.service.FieldServiceInter;
import com.farm.iproject.service.PdocumentServiceInter;
import com.farm.iproject.service.ProjectServiceInter;
import com.farm.iproject.utils.JsonUtils;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import com.farm.wsch.index.WschIndexFactory;
import com.farm.wsch.index.WschSearchServerInter;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：业务类型控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/indexc")
@Controller
public class IndexMngController extends WebUtils {
	private final static Logger log = Logger.getLogger(IndexMngController.class);
	@Resource
	private AppmodelServiceInter appModelServiceImpl;
	@Resource
	private ProjectServiceInter projectServiceImpl;
	@Resource
	private FieldServiceInter fieldServiceImpl;
	@Resource
	private PdocumentServiceInter pDocumentServiceImpl;

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("iproject/IndexResult");
	}

	@RequestMapping("/creatForm")
	public ModelAndView creatForm(String projectid, HttpSession session) {
		Project project = projectServiceImpl.getProjectEntity(projectid);
		List<Field> fields = fieldServiceImpl.getFields(project.getId());
		List<Appmodel> models = appModelServiceImpl.getAppmodels(projectid);
		return ViewMode.getInstance().putAttr("fields", fields).putAttr("project", project).putAttr("models", models)
				.returnModelAndView("iproject/IndexCreatForm");
	}

	@RequestMapping("/creatSubmit")
	@ResponseBody
	public Map<String, Object> creatSubmit(String projectid, String jsonstr, HttpSession session,
			HttpServletRequest request) {
		try {
			Map<String, String> paras = JsonUtils.toMap(jsonstr);
			String taskkey = UUID.randomUUID().toString().replace("-", "");
			pDocumentServiceImpl.submitDocument(projectid, taskkey, paras, getCurrentUser(session));
			pDocumentServiceImpl.runIndex(taskkey, projectid);
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/info")
	public ModelAndView info(String projectid, HttpSession session) {
		try {
			Map<String, Object> map = WschIndexFactory.getStats(projectServiceImpl.getIndexConfig(projectid));
			Project project = projectServiceImpl.getProjectEntity(projectid);
			List<Field> fields = fieldServiceImpl.getFields(projectid);
			return ViewMode.getInstance().putAttr("info", map).putAttr("fields", fields).putAttr("project", project)
					.returnModelAndView("iproject/IndexInfos");
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnModelAndView("web-simple/simple-500");
		}
	}

	@RequestMapping("/mergeIndex")
	@ResponseBody
	public Map<String, Object> mergeIndex(String projectid, HttpSession session, HttpServletRequest request) {
		try {
			projectServiceImpl.mergeIndex(projectid, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
}
