package com.farm.iproject.service.impl;

import com.farm.iproject.domain.Appdic;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.iproject.dao.AppdicDaoInter;
import com.farm.iproject.service.AppdicServiceInter;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：业务字典服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class AppdicServiceImpl implements AppdicServiceInter {
	@Resource
	private AppdicDaoInter appdicDaoImpl;

	private static final Logger log = Logger.getLogger(AppdicServiceImpl.class);

	@Override
	@Transactional
	public Appdic insertAppdicEntity(Appdic entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return appdicDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Appdic editAppdicEntity(Appdic entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		Appdic entity2 = appdicDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setAppid(entity.getAppid());
		entity2.setDicid(entity.getDicid());
		entity2.setId(entity.getId());
		appdicDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteAppdicEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		appdicDaoImpl.deleteEntity(appdicDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Appdic getAppdicEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return appdicDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createAppdicSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WSCH_P_APPDIC a left join WSCH_P_DICTYPE b on a.DICID=b.id",
				"a.ID as ID,a.APPID as APPID,a.DICID as DICID,b.name as DICNAME");
		return dbQuery;
	}

}
