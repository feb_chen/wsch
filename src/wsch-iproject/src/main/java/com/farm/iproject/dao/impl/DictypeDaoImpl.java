package com.farm.iproject.dao.impl;

import java.math.BigInteger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.farm.iproject.domain.Dictype;
import com.farm.wsch.index.pojo.ResultDicNum;
import com.farm.wsch.index.pojo.ResultDicNum.PeekType;
import com.farm.iproject.dao.DictypeDaoInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.utils.HibernateSQLTools;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;

/* *
 *功能：字典类型持久层实现
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Repository
public class DictypeDaoImpl extends HibernateSQLTools<Dictype>implements DictypeDaoInter {
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFatory;

	@Override
	public void deleteEntity(Dictype dictype) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.delete(dictype);
	}

	@Override
	public int getAllListNum() {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery("select count(*) from farm_code_field");
		BigInteger num = (BigInteger) sqlquery.list().get(0);
		return num.intValue();
	}

	@Override
	public Dictype getEntity(String dictypeid) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		return (Dictype) session.get(Dictype.class, dictypeid);
	}

	@Override
	public Dictype insertEntity(Dictype dictype) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.save(dictype);
		return dictype;
	}

	@Override
	public void editEntity(Dictype dictype) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.update(dictype);
	}

	@Override
	public Session getSession() {
		// TODO 自动生成代码,修改后请去除本注释
		return sessionFatory.getCurrentSession();
	}

	@Override
	public DataResult runSqlQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			return query.search(sessionFatory.getCurrentSession());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void deleteEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		deleteSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public List<Dictype> selectEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		return selectSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		updataSqlFromFunction(sessionFatory.getCurrentSession(), values, rules);
	}

	@Override
	public int countEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		return countSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	public SessionFactory getSessionFatory() {
		return sessionFatory;
	}

	public void setSessionFatory(SessionFactory sessionFatory) {
		this.sessionFatory = sessionFatory;
	}

	@Override
	protected Class<?> getTypeClass() {
		return Dictype.class;
	}

	@Override
	protected SessionFactory getSessionFactory() {
		return sessionFatory;
	}

	@Override
	public List<ResultDicNum> getResultDicNums(String projectid, String appkey) {
		Session session = sessionFatory.getCurrentSession();
		String sql = null;
		if (appkey.equals("ALL")) {
			sql = "SELECT D.ID AS ID,C.FIELDKEY AS FIELDKEY, D.KEYID AS VALKEY, D. NAME AS NAME, D.PARENTID AS PARENTID, C.ABLETYPE AS ABLETYPE, C.LIKETYPE AS PEEKTYPE,D.SUPERID AS DOMAIN FROM WSCH_P_APPMODEL A LEFT JOIN WSCH_P_APPDIC B ON A.ID = B.APPID LEFT JOIN WSCH_P_DICTYPE C ON C.ID = B.DICID LEFT JOIN WSCH_P_DICTYPE D ON D.SUPERID = C.ID WHERE A.PROJECTID = ? AND D.PARENTID != 'NONE'";
		} else {
			sql = "SELECT D.ID AS ID,C.FIELDKEY AS FIELDKEY, D.KEYID AS VALKEY, D. NAME AS NAME, D.PARENTID AS PARENTID, C.ABLETYPE AS ABLETYPE, C.LIKETYPE AS PEEKTYPE,D.SUPERID AS DOMAIN FROM WSCH_P_APPMODEL A LEFT JOIN WSCH_P_APPDIC B ON A.ID = B.APPID LEFT JOIN WSCH_P_DICTYPE C ON C.ID = B.DICID LEFT JOIN WSCH_P_DICTYPE D ON D.SUPERID = C.ID WHERE A.PROJECTID = ? AND A.KEYID = ? AND D.PARENTID != 'NONE'";
		}
		SQLQuery sqlquery = session.createSQLQuery(sql);
		if (!appkey.equals("ALL")) {
			sqlquery.setString(1, appkey);
		}
		sqlquery.setString(0, projectid);
		List<ResultDicNum> dics = new ArrayList<ResultDicNum>();
		@SuppressWarnings("rawtypes")
		List list = sqlquery.list();
		for (Object obj : list) {
			// ID0,FIELDKEY1,VALKEY2,NAME3,PARENTID4,ABLETYPE5,PEEKTYPE6,DOMAIN7
			Object[] objarray = (Object[]) obj;
			ResultDicNum dic = new ResultDicNum();
			dic.setDomain((String) objarray[7]);
			dic.setFieldkey((String) objarray[1]);
			dic.setId((String) objarray[0]);
			dic.setName((String) objarray[3]);
			dic.setParentid((String) objarray[4]);
			dic.setPeektype(((String) objarray[6]).equals("1") ? PeekType.equals : PeekType.indexOfS);
			dic.setValkey((String) objarray[2]);
			dics.add(dic);
		}
		return dics;
	}
}
