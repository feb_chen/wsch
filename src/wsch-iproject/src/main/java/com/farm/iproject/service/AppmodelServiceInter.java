package com.farm.iproject.service;

import com.farm.iproject.domain.Appmodel;
import com.farm.core.sql.query.DataQuery;

import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：业务类型服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface AppmodelServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Appmodel insertAppmodelEntity(Appmodel entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Appmodel editAppmodelEntity(Appmodel entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteAppmodelEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Appmodel getAppmodelEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createAppmodelSimpleQuery(DataQuery query);

	/**
	 * 項目下的檢索域數量
	 * 
	 * @param string
	 * @return
	 */
	public Object getAppModelNum(String projectid);

	/**
	 * 获得项目检索业务
	 * 
	 * @param projectid
	 * @return
	 */
	public List<Appmodel> getAppmodels(String projectid);

	/**
	 * 获得检索业务
	 * 
	 * @param projectid
	 * @param key
	 * @return
	 */
	public Appmodel getAppmodel(String projectid, String key);

	/**
	 * 获得业务模型
	 * 
	 * @param projectid
	 * @return
	 */
	public Map<String, Appmodel> getAppmodelMap(String projectid);
}