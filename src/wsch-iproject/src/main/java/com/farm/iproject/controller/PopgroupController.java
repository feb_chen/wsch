package com.farm.iproject.controller;

import com.farm.iproject.domain.PopGroup;
import com.farm.iproject.domain.Project;
import com.farm.iproject.service.PopgroupServiceInter;
import com.farm.iproject.service.ProjectServiceInter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：权限组控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/popgroup")
@Controller
public class PopgroupController extends WebUtils {
	private final static Logger log = Logger.getLogger(PopgroupController.class);
	@Resource
	private PopgroupServiceInter popGroupServiceImpl;
	@Resource
	private ProjectServiceInter projectServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DataResult result = popGroupServiceImpl.createPopgroupSimpleQuery(query).search();
			result.runformatTime("CTIME", "yyyy-MM-dd HH:mm:ss");
			result.runDictionary("1:可用,0:禁用", "PSTATE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(PopGroup entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = popGroupServiceImpl.editPopgroupEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(PopGroup entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = popGroupServiceImpl.insertPopgroupEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				popGroupServiceImpl.deletePopgroupEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		List<Project> projects = projectServiceImpl.getProjects();
		return ViewMode.getInstance().putAttr("projects", projects).returnModelAndView("iproject/PopgroupResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			List<Project> projects = projectServiceImpl.getProjects();
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("projects", projects)
						.putAttr("entity", popGroupServiceImpl.getPopgroupEntity(ids))
						.returnModelAndView("iproject/PopgroupForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("projects", projects)
						.returnModelAndView("iproject/PopgroupForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("projects", projects)
						.putAttr("entity", popGroupServiceImpl.getPopgroupEntity(ids))
						.returnModelAndView("iproject/PopgroupForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("iproject/PopgroupForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("iproject/PopgroupForm");
		}
	}
}
