package om.wuc.client.events.domain;

/**
 * 事件关系人
 * 
 * @author macpl
 *
 */
public class WucEventUser {
	private String rolekey;// 关系人类型 必填
	private String userobjkey;// 关系人key 必填一般为登录名
	private String userobjname;// 关系人名称 必填
	private String userobjip;// 关系人IP
	private String note;// 关系备注 可空
	public String getRolekey() {
		return rolekey;
	}
	public void setRolekey(String rolekey) {
		this.rolekey = rolekey;
	}
	public String getUserobjkey() {
		return userobjkey;
	}
	public void setUserobjkey(String userobjkey) {
		this.userobjkey = userobjkey;
	}
	public String getUserobjname() {
		return userobjname;
	}
	public void setUserobjname(String userobjname) {
		this.userobjname = userobjname;
	}
	public String getUserobjip() {
		return userobjip;
	}
	public void setUserobjip(String userobjip) {
		this.userobjip = userobjip;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
}
