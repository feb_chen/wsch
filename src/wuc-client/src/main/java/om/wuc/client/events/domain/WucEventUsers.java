package om.wuc.client.events.domain;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.wuc.client.domain.EventRUser;

/**
 * 关系人组
 * 
 * @author macpl
 *
 */
public class WucEventUsers {
	private List<WucEventUser> users = new ArrayList<WucEventUser>();

	public WucEventUsers addUser(String roleKey, WucUser currentUser, String currentIp, String note) {
		if (currentUser != null) {
			WucEventUser user = new WucEventUser();
			user.setRolekey(roleKey);
			user.setUserobjkey(currentUser.getLoginname());
			user.setUserobjname(currentUser.getName());
			if (StringUtils.isNotBlank(currentIp)) {
				user.setUserobjip(currentIp);
			}
			if (StringUtils.isNotBlank(note)) {
				user.setNote(note);
			}
			users.add(user);
		}
		return this;
	}

	public WucEventUsers addUser(String roleKey, WucUser currentUser, String currentIp) {
		if (currentUser != null) {
			WucEventUser user = new WucEventUser();
			user.setRolekey(roleKey);
			user.setUserobjkey(currentUser.getLoginname());
			user.setUserobjname(currentUser.getName());
			if (StringUtils.isNotBlank(currentUser.getIp())) {
				user.setUserobjip(currentUser.getIp());
			}
			if (StringUtils.isNotBlank(currentIp)) {
				user.setUserobjip(currentIp);
			}
			users.add(user);
		} else {
			if (StringUtils.isNotBlank(currentIp)) {
				WucEventUser user = new WucEventUser();
				user.setRolekey(roleKey);
				user.setUserobjkey(currentIp);
				user.setUserobjname(currentIp);
				user.setUserobjip(currentIp);
				users.add(user);
			}
		}
		return this;
	}

	public List<EventRUser> getEventRUsers() {
		List<EventRUser> backusers = new ArrayList<EventRUser>();
		for (WucEventUser user : users) {
			EventRUser ruser = new EventRUser(user.getRolekey(), user.getUserobjname(), user.getUserobjkey(),
					user.getUserobjip(), user.getNote());
			backusers.add(ruser);
		}
		return backusers;
	}
}
