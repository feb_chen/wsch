package om.wuc.client.events;

import com.wuc.client.WucEventClient;
import com.wuc.client.domain.EventData;
import com.wuc.client.utils.TimeTool;

import om.wuc.client.events.domain.WucEventInfo;
import om.wuc.client.events.domain.WucEventUsers;

//事件记录辅助类
//WucEventRecorder.getInstance(
//			FarmParameterService.getInstance().getParameter("config.wuc.api.base.url"),
//			FarmParameterService.getInstance().getParameter("config.wuc.api.secret"),
//			FarmParameterService.getInstance().getParameter("config.wuc.api.loginname"),
//			FarmParameterService.getInstance().getParameter("config.wuc.api.password"));
//	
//}
public class WucEventRecorder {

	private WucEventClient client = null;
	private boolean able = false;

	public static WucEventRecorder getInstance(String url, String secret, String loginname, String password,
			boolean isEventRecordAble) {
		WucEventRecorder obj = new WucEventRecorder();

		obj.client = new WucEventClient(url, secret, loginname, password);
		obj.able = isEventRecordAble;
		return obj;
	}

	/**
	 * 记录事件
	 * 
	 * @param sysid
	 * @param eventKey
	 * @param eventTitle
	 * @param info
	 * @param users
	 * @param isSycn
	 */
	public void runEvent(final String sysid, final String eventKey, final String eventTitle, final WucEventInfo info,
			final WucEventUsers users, boolean isSycn) {
		if (able) {
			Thread func = new Thread(new Runnable() {
				@Override
				public void run() {
					EventData dat = new EventData(info.getObjid(), info.getObjname(), eventKey, eventTitle, null, sysid,
							TimeTool.getTimeDate14(), info.getValues());
					client.recordEvent(dat, users.getEventRUsers());

				}
			}) {
			};
			if (isSycn) {
				func.start();
			} else {
				func.run();
			}
		}
	}

}
