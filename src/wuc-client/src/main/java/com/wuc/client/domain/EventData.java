package com.wuc.client.domain;

public class EventData {

	private String objid;
	private String objname;
	private String modelkey;
	private String title;
	private String note;
	private String sysid;
	private String ctime14;
	private Values values;

	public EventData(String objid, String objname, String modelkey, String title, String note, String sysid,
			String ctime14, Values values) {
		this.objid = objid;
		this.objname = objname;
		this.modelkey = modelkey;
		this.title = title;
		this.note = note;
		this.sysid = sysid;
		this.ctime14 = ctime14;
		this.values = values;
	}

	public String getObjid() {
		return objid;
	}

	public void setObjid(String objid) {
		this.objid = objid;
	}

	public String getObjname() {
		return objname;
	}

	public void setObjname(String objname) {
		this.objname = objname;
	}

	public String getModelkey() {
		return modelkey;
	}

	public void setModelkey(String modelkey) {
		this.modelkey = modelkey;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSysid() {
		return sysid;
	}

	public void setSysid(String sysid) {
		this.sysid = sysid;
	}

	public String getCtime14() {
		return ctime14;
	}

	public void setCtime14(String ctime14) {
		this.ctime14 = ctime14;
	}

	public Values getValues() {
		return values;
	}

	public void setValues(Values values) {
		this.values = values;
	}

}
