package com.wuc.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import com.alibaba.fastjson.JSONArray;
import com.wuc.client.domain.EventData;
import com.wuc.client.domain.EventRUser;
import com.wuc.client.domain.PGroup;
import com.wuc.client.domain.Para;
import com.wuc.client.domain.Values;
import com.wuc.client.https.HttpUtils;
import com.wuc.client.utils.WtcJsonMap;

/**
 * 参数相关
 * 
 * @author macpl
 *
 */
public class WucParasClient {
	private String secret;
	private String operatorLoginname;
	private String operatorPassword;
	private String baseUrl;

	public WucParasClient(String baseUrl, String secret, String operatorLoginname, String operatorPassword) {
		super();
		this.secret = secret;
		this.baseUrl = baseUrl;
		this.operatorLoginname = operatorLoginname;
		this.operatorPassword = operatorPassword;
	}

	/**
	 * 服务是否可用
	 * 
	 * @return
	 */
	public boolean isLive() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("loginname", operatorLoginname);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/get/user.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
		return true;
	}

	/**
	 * 添加参数组
	 * 
	 * @param key
	 * @param name
	 * @param comment
	 */
	public void addGroup(String key, String name, String comment) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("name", name);
		data.put("key", key);
		data.put("comments", comment);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/paras/group/put.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
	}

	/**
	 * 添加组中的参数
	 * 
	 * @param groupkey
	 * @param key
	 * @param val
	 * @param name
	 * @param note
	 */
	public void addGroupPara(String groupkey, String key, String val, String name, String note) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("groupkey", groupkey);
		data.put("key", key);
		data.put("name", name);
		data.put("note", note);
		data.put("val", val);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/paras/para/put.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}

	}

	/**
	 * 设置组参数值
	 * 
	 * @param groupkey
	 * @param key
	 * @param val
	 */
	public void putGroupParaVal(String groupkey, String key, String val) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("groupkey", groupkey);
		data.put("key", key);
		data.put("val", val);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/paras/para/set.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
	}

	/**
	 * 查询参数组
	 * 
	 * @param keyPart 部分key，模糊匹配
	 */
	public List<PGroup> getGroups(String keyPart) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("key", keyPart);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/paras/groups/get.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
		WtcJsonMap json = new WtcJsonMap(backData.toMap());
		List<PGroup> list = new ArrayList<PGroup>();
		for (WtcJsonMap node : json.getList("DATA")) {
			PGroup group = new PGroup();
			group.setGroupkey(node.getString("groupkey"));
			group.setId(node.getString("id"));
			group.setName(node.getString("name"));
			group.setState(node.getString("state"));
			List<Para> paras = new ArrayList<Para>();
			for (WtcJsonMap para : node.getList("paras")) {
				Para paraobj = new Para();
				paraobj.setKey(para.getString("PKEY"));
				paraobj.setName(para.getString("PNAME"));
				paraobj.setVal(para.getString("PVAL"));
				paras.add(paraobj);
			}
			group.setParas(paras);
			list.add(group);
		}
		return list;
	}

}
