package com.wuc.client.domain;

public class EventRUser {
	private String rolekey;
	private String username;
	private String userkey;
	private String userip;
	private String note;

	public EventRUser(String rolekey, String username, String userkey, String userip, String note) {
		super();
		this.rolekey = rolekey;
		this.username = username;
		this.userkey = userkey;
		this.note = note;
		this.userip = userip;
	}

	public String getUserip() {
		return userip;
	}

	public void setUserip(String userip) {
		this.userip = userip;
	}

	public String getRolekey() {
		return rolekey;
	}

	public void setRolekey(String rolekey) {
		this.rolekey = rolekey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserkey() {
		return userkey;
	}

	public void setUserkey(String userkey) {
		this.userkey = userkey;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
