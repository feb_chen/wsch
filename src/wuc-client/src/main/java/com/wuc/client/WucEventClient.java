package com.wuc.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import com.alibaba.fastjson.JSONArray;
import com.wuc.client.domain.EventData;
import com.wuc.client.domain.EventRUser;
import com.wuc.client.domain.Values;
import com.wuc.client.https.HttpUtils;

/**积分、事件、消息、权限
 * @author macpl
 *
 */
public class WucEventClient {
	private String secret;
	private String operatorLoginname;
	private String operatorPassword;
	private String baseUrl;

	public WucEventClient(String baseUrl, String secret, String operatorLoginname, String operatorPassword) {
		super();
		this.secret = secret;
		this.baseUrl = baseUrl;
		this.operatorLoginname = operatorLoginname;
		this.operatorPassword = operatorPassword;
	}

	/**
	 * @param objid    事件主体对象id
	 * @param objname  事件主体对象名称
	 * @param modelkey 事件模型key
	 * @param title    事件标题
	 * @param note     事件备注
	 * @param ctime14  事件发生时间
	 * @param ctime142
	 * @param values   参数值
	 * @return 事件记录id（eventid）
	 */
	private String recordEvent(String objid, String objname, String modelkey, String title, String note, String sysid,
			String ctime14, Values values) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("objid", objid);
		data.put("objname", objname);
		data.put("modelkey", modelkey);
		data.put("title", title);
		data.put("note", note);
		data.put("sysid", sysid);
		data.put("ctime14", ctime14);
		data.put("num1", values.getNum1() == null ? null : values.getNum1().toString());
		data.put("num2", values.getNum2() == null ? null : values.getNum2().toString());
		data.put("num3", values.getNum3() == null ? null : values.getNum3().toString());
		data.put("num4", values.getNum4() == null ? null : values.getNum4().toString());
		data.put("num5", values.getNum5() == null ? null : values.getNum5().toString());
		data.put("numtitle1", values.getNumTitle1());
		data.put("numtitle2", values.getNumTitle2());
		data.put("numtitle3", values.getNumTitle3());
		data.put("numtitle4", values.getNumTitle4());
		data.put("numtitle5", values.getNumTitle5());
		data.put("text1", values.getText1());
		data.put("text2", values.getText2());
		data.put("text3", values.getText3());
		data.put("text4", values.getText4());
		data.put("text5", values.getText5());
		data.put("texttitle1", values.getTextTitle1());
		data.put("texttitle2", values.getTextTitle2());
		data.put("texttitle3", values.getTextTitle3());
		data.put("texttitle4", values.getTextTitle4());
		data.put("texttitle5", values.getTextTitle5());
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/event/on.do", data);
		if (backData.has("STATE") && backData.getInt("STATE") == 0) {
			return backData.getString("EVENTID");
		}
		throw new RuntimeException(backData.toString());
	}

	/**
	 * 绑定事件关系人
	 * 
	 * @param eventid
	 * @param objid
	 * @param objname
	 * @param modelkey
	 * @param sysid
	 * @param users
	 */
	private void bindEventUser(String eventid, String objid, String objname, String modelkey, String sysid,
			List<EventRUser> users) {
		for (EventRUser user : users) {
			if (user != null && StringUtils.isNotBlank(user.getUserkey())) {
				Map<String, String> data = new HashMap<String, String>();
				data.put("secret", secret);
				data.put("operatorLoginname", operatorLoginname);
				data.put("operatorPassword", operatorPassword);
				// -----------
				data.put("objid", objid);
				data.put("objname", objname);
				data.put("modelkey", modelkey);
				data.put("sysid", sysid);
				data.put("eventid", eventid);
				data.put("rolekey", user.getRolekey());
				data.put("userobjkey", user.getUserkey());
				data.put("userobjname", user.getUsername());
				data.put("userobjip", user.getUserip());
				data.put("note", user.getNote());
				JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/event/binduser.do", data);
				if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
					throw new RuntimeException(backData.toString());
				}
			}
		}
	}

	/**
	 * 事件提交结束，处理关联任务
	 * 
	 * @param objid
	 * @param sysid
	 * @param modelkey
	 * @param eventid
	 * @param sysid
	 * @param modelkey
	 * @param objid
	 * @param users
	 */
	private void recordEventOver(String eventid) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("eventid", eventid);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/event/over.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
	}

	/**
	 * 注册事件模型
	 * 
	 * @param key
	 * @param name
	 * @param note
	 * @param sysid
	 */
	public void registEventModel(String key, String name, String note, String sysid) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("key", key);
		data.put("sysid", sysid);
		data.put("name", name);
		data.put("note", note);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/event/regist/model.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
	}

	/**
	 * 注册事件关系人模型
	 * 
	 * @param string
	 * @param string2
	 * @param object
	 */
	public void registUserRole(String key, String name, String note, String sysid) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("key", key);
		data.put("sysid", sysid);
		data.put("name", name);
		data.put("note", note);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/event/regist/role.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
	}

	public boolean isLive() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("loginname", operatorLoginname);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/get/user.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
		return true;
	}

	/**
	 * 提交一个事件
	 * 
	 * @param eventData
	 * @param users
	 */
	public void recordEvent(EventData eventData, List<EventRUser> users) {
		String eventid = recordEvent(eventData.getObjid(), eventData.getObjname(), eventData.getModelkey(),
				eventData.getTitle(), eventData.getNote(), eventData.getSysid(), eventData.getCtime14(),
				eventData.getValues());
		bindEventUser(eventid, eventData.getObjid(), eventData.getObjname(), eventData.getModelkey(),
				eventData.getSysid(), users);
		recordEventOver(eventid);
	}

	/**
	 * 获得登录授权码
	 * 
	 * @param loginname
	 * @return
	 */
	public String getLoginCertificate(String loginname) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("loginname", loginname);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/regist/login.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
		return backData.getString("CERTIFICATE");
	}

	/**
	 * 获得用户全部积分
	 * 
	 * @param loginname
	 * @return
	 */
	public int getUserPoint(String loginname, String sysid) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("loginname", loginname);
		data.put("sysid", sysid);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/point/user.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
		return backData.getInt("POINT");
	}

	/**
	 * 查询事件记录
	 * 
	 * @param parameter
	 * @param name
	 * @param size
	 * @param loginname
	 * @param i
	 * @return
	 */
	public List<Map<String, Object>> getEventRecord(String sysid, String modelkey, String oploginname, int pagesize) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("sysid", sysid);
		data.put("modelkey", modelkey);
		data.put("oploginname", oploginname);
		data.put("pagesize", new Integer(pagesize).toString());
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/event/query.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> listObjectFir = (List<Map<String, Object>>) JSONArray
				.parse(backData.getJSONArray("DATA").toString());
		return listObjectFir;
	}

	/**
	 * 设置消息地址
	 * 
	 * @param loginname
	 * @param addrtype
	 * @param addr
	 * @param addrstate
	 */
	public void putMsgAddr(String loginname, String addrtype, String addr, String addrstate) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("loginname", loginname);
		data.put("addrtype", addrtype);
		data.put("addr", addr);
		data.put("addrstate", addrstate);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/msg/set/addr.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
	}

	/**
	 * 获得消息地址
	 * 
	 * @param loginname
	 * @param addrtype
	 */
	public Map<String, Object> getMsgAddr(String loginname, String addrtype) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("secret", secret);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		// -----------
		data.put("loginname", loginname);
		data.put("addrtype", addrtype);
		JSONObject backData = HttpUtils.httpPost(baseUrl + "/api/msg/get/addr.do", data);
		if (!backData.has("STATE") || backData.getInt("STATE") != 0) {
			throw new RuntimeException(backData.toString());
		}
		return backData.getJSONObject("DATA").toMap();
	}

}
