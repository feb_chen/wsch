package com.wsch.client.domain;

import java.util.HashMap;
import java.util.Map;

/**检索域
 * @author macpl
 *
 */
public class WDocField {

	public enum TYPES {
		TextField, StringField, FloatField
	}

	public enum TEXTTYPES {
		text("1"), html("2");

		private String val;

		public String getVal() {
			return val;
		}

		TEXTTYPES(String val) {
			this.val = val;
		}
	}

	public enum SEARCHTYPES {
		key("1"), none("0");

		private String val;

		public String getVal() {
			return val;
		}

		SEARCHTYPES(String val) {
			this.val = val;
		}
	}

	public enum SORTABLES {
		sort("1"), none("0");

		private String val;

		public String getVal() {
			return val;
		}

		SORTABLES(String val) {
			this.val = val;
		}
	}

	private String name;
	private String key;
	private boolean storeis;
	private TYPES type;
	private int sort;
	private TEXTTYPES texttype;
	private SEARCHTYPES searchtype;
	private SORTABLES sortable;
	private int boost;
	private int maxlenght;

	/**
	 * 索引字段定义
	 * 
	 * @param name       中文名称
	 * @param key        关键字字符唯一
	 * @param storeis    是否存储
	 * @param type       索引数据类型
	 * @param sort       排序号
	 * @param texttype   展示类型
	 * @param sEARCHTYPE 检索类型
	 * @param sortable   检索排序类型
	 * @param boost      检索权重
	 * @param maxlenght  最大存储长度
	 */
	public WDocField(String name, String key, boolean storeis, TYPES type, int sort, TEXTTYPES texttype,
			SEARCHTYPES searchtype, SORTABLES sortable, int boost, int maxlenght) {
		super();
		this.name = name;
		this.key = key;
		this.storeis = storeis;
		this.type = type;
		this.sort = sort;
		this.texttype = texttype;
		this.searchtype = searchtype;
		this.sortable = sortable;
		this.boost = boost;
		this.maxlenght = maxlenght;
	}

	public String getName() {
		return name;
	}

	public String getKey() {
		return key;
	}

	public boolean isStoreis() {
		return storeis;
	}

	public TYPES getType() {
		return type;
	}

	public int getSort() {
		return sort;
	}

	public TEXTTYPES getTexttype() {
		return texttype;
	}

	public SEARCHTYPES getSearchtype() {
		return searchtype;
	}

	public SORTABLES getSortable() {
		return sortable;
	}

	public int getBoost() {
		return boost;
	}

	public int getMaxlenght() {
		return maxlenght;
	}

	public Map<String, String> getMap() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("name", name);
		data.put("key", key);
		data.put("storeis", storeis ? "1" : "0");
		data.put("type", type.name());
		data.put("sort", new Integer(sort).toString());
		data.put("texttype", texttype.getVal());
		data.put("searchtype", searchtype.getVal());
		data.put("sortable", sortable.getVal());
		data.put("boost", new Integer(boost).toString());
		data.put("maxlenght", new Integer(maxlenght).toString());
		return data;
	}

}
