package com.wsch.client.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * 业务模型的虚拟字段
 * 
 * @author macpl
 *
 */
public class WDocVirtualField {

	public enum TYPES {
		NONE, TITLE, CONTENT, IMG, TAG, TIME, AUTHOR, OLINK, CATALOG
	}

	private String name;
	private TYPES type;
	private String formula;

	public WDocVirtualField(String name, TYPES type, String formula) {
		super();
		this.name = name;
		this.type = type;
		this.formula = formula;
	}

	public String getName() {
		return name;
	}

	public TYPES getType() {
		return type;
	}

	public String getFormula() {
		return formula;
	}

	public Map<String, String> getMap() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("name", name);
		data.put("type", type.name());
		data.put("formula", formula);
		return data;
	}

}
