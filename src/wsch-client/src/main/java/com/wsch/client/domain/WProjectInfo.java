package com.wsch.client.domain;

/**
 * 检索项目信息 "DATA": { "id": "4028840f788b9fca01788ba0f5800000", "keyid": "WCP",
 * "name": "WCP知识库", "pstate": "2", "sort": 1 }
 * 
 * @author macpl
 *
 */
public class WProjectInfo {
	private String id;
	private String key;
	private String name;
	private String state;
	private int sort;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

}
