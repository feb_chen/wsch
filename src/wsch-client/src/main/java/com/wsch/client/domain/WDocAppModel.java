package com.wsch.client.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务模型
 * 
 * @author macpl
 *
 */
public class WDocAppModel {
	private String name;
	private String key;
	private int sort;
	private Map<WDocVirtualField.TYPES, WDocVirtualField> appfields;

	public WDocAppModel(String name, String key, int sort, Map<WDocVirtualField.TYPES, WDocVirtualField> appfields) {
		super();
		this.name = name;
		this.key = key;
		this.sort = sort;
		this.appfields = appfields;
	}

	public String getName() {
		return name;
	}

	public String getKey() {
		return key;
	}

	public int getSort() {
		return sort;
	}

	public List<WDocVirtualField> getAppfields() {
		List<WDocVirtualField> list = new ArrayList<WDocVirtualField>();
		for (WDocVirtualField field : appfields.values()) {
			list.add(field);
		}
		return list;
	}

	public Map<String, String> getMap() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("name", name);
		data.put("key", key);
		data.put("sort", new Integer(sort).toString());
		return data;
	}

}
