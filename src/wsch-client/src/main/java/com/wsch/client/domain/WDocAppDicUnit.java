package com.wsch.client.domain;

import java.util.HashMap;
import java.util.Map;

public class WDocAppDicUnit {

	private String key;
	private String name;
	private int sort;
	private String parrentId;

	public Map<String, String> getMap() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("key", key);
		data.put("name", name);
		data.put("parrentId", parrentId);
		data.put("sort", new Integer(sort).toString());
		return data;
	}

	public WDocAppDicUnit(String key, String name, int sort, String parrentId) {
		super();
		this.key = key;
		this.name = name;
		this.sort = sort;
		this.parrentId = parrentId;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

	public int getSort() {
		return sort;
	}

	public String getParrentId() {
		return parrentId;
	}

}
