package com.farm.sfile.wdas.util;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class WSClient {
	private String operatorPassword;
	private String operatorLoginname;
	private String secret;
	private String baseUrl;
	private static final Logger log = Logger.getLogger(WSClient.class);

	public static WSClient getInstance(String operatorLoginname, String operatorPassword, String secret,
			String serviceUrl) {
		WSClient obj = new WSClient();
		obj.operatorPassword = operatorPassword;
		obj.operatorLoginname = operatorLoginname;
		obj.secret = secret;
		obj.baseUrl = serviceUrl;
		return obj;
	}

	/**
	 * 上传一个文件到远程WDAP
	 * 
	 * @param file
	 * @param path
	 * @throws IOException
	 */
	public void uploadFile(File file, String filename, String fileAppId, String fileUpdataProcessKey,
			WSFileHttpUploads.ProgressHandle handle) throws IOException {
		Map<String, String> data = new HashMap<String, String>();
		data.put("fileAppId", fileAppId);
		data.put("filename", filename);
		data.put("processkey", fileUpdataProcessKey);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		data.put("secret", secret);
		WSFileHttpUploads uploader = WSFileHttpUploads.getInstance(baseUrl.trim() + "/fileapi/file/upload.do", data);
		uploader.setRemoteProgressUrl(baseUrl.trim() + "/fileapi/file/process.do");
		uploader.setProgressHandle(handle);
		uploader.doUpload(file, filename);
	}

	/**
	 * 邏輯刪除文件
	 * 
	 * @param fileId
	 * @throws RemoteException
	 * @throws JSONException
	 */
	public void delFile(String fileAppId) throws RemoteException, JSONException {
		Map<String, String> data = new HashMap<String, String>();
		data.put("fileAppId", fileAppId);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		data.put("secret", secret);
		JSONObject obj = WSHttpUtils.httpPost(baseUrl.trim() + "/fileapi/file/del.do", data);
		validResult(obj);
	}

	/**
	 * 獲得远程副本数量
	 * 
	 * @param fileAppId
	 * @throws RemoteException
	 * @throws JSONException
	 */
	public int getFilenum(String fileAppId) throws RemoteException, JSONException {
		Map<String, String> data = new HashMap<String, String>();
		data.put("fileAppId", fileAppId);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		data.put("secret", secret);
		JSONObject obj = WSHttpUtils.httpPost(baseUrl.trim() + "/fileapi/file/count.do", data);
		validResult(obj);
		return obj.getInt("DATA");
	}

	/**
	 * 获得下載地址
	 * 
	 * @param id
	 * @return
	 * @throws JSONException
	 * @throws RemoteException
	 */
	public String getFileDownloadUrl(String fileAppId) throws RemoteException, JSONException {
		Map<String, String> data = new HashMap<String, String>();
		data.put("fileAppId", fileAppId);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		data.put("secret", secret);
		JSONObject obj = WSHttpUtils.httpPost(baseUrl.trim() + "/fileapi/url/download.do", data);
		validResult(obj);
		if (obj.isNull("url")) {
			return null;
		} else {
			return obj.get("url").toString();
		}
	}

	/**
	 * 获得下載地址
	 * 
	 * @param id
	 * @return
	 * @throws JSONException
	 * @throws RemoteException
	 */
	public String getFileViewUrl(String fileAppId) throws RemoteException, JSONException {
		Map<String, String> data = new HashMap<String, String>();
		data.put("fileAppId", fileAppId);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		data.put("secret", secret);
		JSONObject obj = WSHttpUtils.httpPost(baseUrl.trim() + "/fileapi/url/view.do", data);
		validResult(obj);
		if (obj.isNull("url")) {
			return null;
		} else {
			return obj.get("url").toString();
		}
	}

	/**
	 * 获得播放地址
	 * 
	 * @param id
	 * @return
	 * @throws JSONException
	 * @throws RemoteException
	 */
	public String getFilePlayUrl(String fileAppId) throws RemoteException, JSONException {
		Map<String, String> data = new HashMap<String, String>();
		data.put("fileAppId", fileAppId);
		data.put("operatorLoginname", operatorLoginname);
		data.put("operatorPassword", operatorPassword);
		data.put("secret", secret);
		JSONObject obj = WSHttpUtils.httpPost(baseUrl.trim() + "/fileapi/url/mediaplay.do", data);
		validResult(obj);
		if (obj.isNull("url")) {
			return null;
		} else {
			return obj.get("url").toString();
		}
	}

	private void validResult(JSONObject obj) throws RemoteException, JSONException {
		if (obj.get("STATE").toString().equals("1")) {
			throw new RemoteException(obj.get("MESSAGE").toString());
		}
	}

	public int getUoloadProcess(String processkey) throws RemoteException, JSONException {
		Map<String, String> data = new HashMap<String, String>();
		data.put("processkey", processkey);
		JSONObject obj = WSHttpUtils.httpPost(baseUrl.trim() + "/fileapi/file/process.do", data);
		validResult(obj);
		return (Integer) obj.get("PROCESS");
	}
}
