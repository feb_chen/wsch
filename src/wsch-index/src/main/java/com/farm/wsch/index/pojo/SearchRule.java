package com.farm.wsch.index.pojo;

public class SearchRule {
	private String fieldkey;
	private String val;
	private boolean isLike;
	// 是否原子（原子时不对检索词分词）
	private boolean isSmart;

	public String getFieldkey() {
		return fieldkey;
	}

	public void setFieldkey(String fieldkey) {
		this.fieldkey = fieldkey;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean isLike) {
		this.isLike = isLike;
	}

	public boolean isSmart() {
		return isSmart;
	}

	public void setSmart(boolean isSmart) {
		this.isSmart = isSmart;
	}

}
