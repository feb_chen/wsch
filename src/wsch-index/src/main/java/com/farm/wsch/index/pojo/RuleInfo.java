package com.farm.wsch.index.pojo;

public class RuleInfo {

	/**
	 * 是否新的查询组（每个组都是一个AND条件集合）
	 */
	private boolean newGroup = false;

	/**
	 * 域描述
	 */
	private String fieldTitle;
	private String fieldKey;
	private String val;
	/**
	 * 值描述
	 */
	private String ValTitle;
	/**
	 * 检索类型
	 */
	private String searchFlag;

	public String getFieldTitle() {
		return fieldTitle;
	}

	public void setFieldTitle(String fieldTitle) {
		this.fieldTitle = fieldTitle;
	}

	public String getFieldKey() {
		return fieldKey;
	}

	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public String getValTitle() {
		return ValTitle;
	}

	public void setValTitle(String valTitle) {
		ValTitle = valTitle;
	}

	public String getSearchFlag() {
		return searchFlag;
	}

	public void setSearchFlag(String searchFlag) {
		this.searchFlag = searchFlag;
	}

	public boolean isNewGroup() {
		return newGroup;
	}

	public void setNewGroup(boolean newGroup) {
		this.newGroup = newGroup;
	}

}
