package com.farm.wsch.index.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;

public class IndexResult {

	private List<Map<String, Object>> list = new ArrayList<>();
	/**
	 * 每页记录数
	 */
	private int pageSize = 10;
	private long startTime = new Date().getTime();
	private float searchTime = 0;
	// 结果集合id
	private String resultId;

	/**
	 * 条件描述
	 */
	private List<RuleInfo> ruleinfos = new ArrayList<>();

	/**
	 * 执行时间
	 */
	private List<RuntimeInfo> timeinfos = new ArrayList<>();

	/**
	 * 当前页
	 */
	private int currentPage;
	/**
	 * 总记录数
	 */
	private int totalSize;

	/**
	 * 总页数
	 */
	private int totalPage;

	/**
	 * 分页起始
	 */
	private int exampleStart;
	/**
	 * 分页结束
	 */
	private int exampleEnd;

	private IndexResult() {
	}

	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

	public IndexResult(int pagesize) {
		this.pageSize = pagesize;
	}

	public void add(Map<String, Object> document) {
		list.add(document);
	}

	public List<Map<String, Object>> getList() {
		return list;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
		this.totalPage = this.totalSize / this.pageSize + 1;
		if (this.totalSize % this.pageSize == 0) {
			this.totalPage = this.totalPage - 1;
		}
	}

	public int getTotalPage() {
		return totalPage;
	}

	/**
	 * 初始化前台分页参数(分页时枚举的可用页数，只显示最多5页)
	 */
	public void initPages() {
		int start = currentPage - 2;
		int end = currentPage + 2;
		if (start < 1) {
			end = end + (1 - start);
			start = 1;
		}
		if (end > totalPage) {
			if (start - (end - totalPage) < 1) {
				start = 1;
			} else {
				start = start - (end - totalPage);
			}
			end = totalPage;
		}
		exampleStart = start;
		exampleEnd = end;
	}

	/**
	 * 计算查询时间
	 */
	public void initSearchTime() {
		searchTime = (new Date().getTime() - startTime) / 1000.00f;
		if (searchTime == 0) {
			searchTime = 0.001f;
		}
	}

	// -------------------------

	public int getExampleStart() {
		return exampleStart;
	}

	public float getSearchTime() {
		return searchTime;
	}

	public void setSearchTime(float searchTime) {
		this.searchTime = searchTime;
	}

	public void setExampleStart(int exampleStart) {
		this.exampleStart = exampleStart;
	}

	public int getExampleEnd() {
		return exampleEnd;
	}

	public void setExampleEnd(int exampleEnd) {
		this.exampleEnd = exampleEnd;
	}

	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public List<RuleInfo> getRuleinfos() {
		return ruleinfos;
	}

	public void setRuleinfos(List<RuleInfo> ruleinfos) {
		this.ruleinfos = ruleinfos;
	}

	public void addRuntimeInfo(RuntimeInfo runtime, int resultNum) {
		runtime.runEnd(resultNum);
		timeinfos.add(runtime);
	}

	public List<RuntimeInfo> getTimeinfos() {
		return timeinfos;
	}

}
