package com.wcp.web.tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * 同步和检查配置文件，主要用于升级的配置文件制作
 * 
 * @author macpl
 *
 */
public class SyncrAndCheckXmlConfig {

	// 新版本文件目录
	private static String newVersionPath = "D:\\test\\wcp-update-xmls\\wcpConfigs-v439";
	// 旧版本文件目录
	private static String oldVersionPath = "D:\\test\\wcp-update-xmls\\wcpConfigs-v437-zjbssw";
	/**
	 * 要同步的文件列表
	 */
	private static String[] fileNames = { "WcpInterConfig.xml", "WcpWebConfig.xml" };

	public static void main(String[] args) throws IOException {
		File ofileBase = new File(newVersionPath + "-update");
		if (ofileBase.exists()) {
			// 清理結果文件夾
			for (File subFile : ofileBase.listFiles()) {
				subFile.delete();
			}
		}
		for (String filename : fileNames) {
			System.out.println("------------------------------" + filename + "------------------------------");
			// 1.加载旧配置文件
			File oldFile = new File(oldVersionPath + "\\" + filename);
			Document oldConfFile = Jsoup.parse(oldFile, "utf-8");
			Map<String, String> oldConfCache = new HashMap<>();
			System.out.println("load old parameters ...[" + oldFile.getPath() + "]");
			for (Element parameter : oldConfFile.getElementsByTag("parameter")) {
				String key = parameter.attr("name");
				String val = parameter.getElementsByTag("val").html();
				if (StringUtils.isBlank(key) || StringUtils.isBlank(val)) {
					throw new RuntimeException("参数加载错误:" + parameter.html());
				}
				oldConfCache.put(key, val);
			}
			System.out.println("参数项共：" + oldConfCache.size());
			// 2.加载新配置文件
			Document newConfFile = Jsoup.parse(new File(newVersionPath + "\\" + filename), "utf-8");
			// 3，同步和检查配置文件
			List<String> warnParas = new ArrayList<>();
			for (Element parameter : newConfFile.getElementsByTag("parameter")) {
				String key = parameter.attr("name");
				String oldVal = oldConfCache.get(key);
				if (oldVal != null) {
					// 同步參數值
					parameter.getElementsByTag("val").html(oldVal);
				} else {
					// 报告
					warnParas.add(key);
					// parameter.getElementsByTag("val").html("[NONE]");
				}
			}
			// 如果：有一致就同步参数，如果没有一致的就打印提示信息
			System.out.println("warn none paras size:" + warnParas.size());
			{
				// 写更新的配置文件

				FileWriter writer;
				ofileBase.mkdirs();
				File ofile = new File(ofileBase.getPath() + "\\" + filename);
				System.out.println("write update xml file.." + ofile.getPath());
				try {
					if (!ofile.exists()) {
						ofile.createNewFile();
					}
					writer = new FileWriter(ofile);
					writer.write(newConfFile.html());
					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			{
				// 写入警告报告
				FileWriter writer;
				ofileBase.mkdirs();
				File ofile = new File(ofileBase.getPath() + "\\" + filename + ".warn.txt");
				System.out.println("write warn file.." + ofile.getPath());
				StringBuffer warnTxt = new StringBuffer();
				for (String para : warnParas) {
					warnTxt.append(para + "\r\n");
				}
				try {
					if (!ofile.exists()) {
						ofile.createNewFile();
					}
					writer = new FileWriter(ofile);
					writer.write(warnTxt.toString());
					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
