package com.farm.wcp.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.service.OrganizationServiceInter;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.page.ViewMode;
import com.farm.parameter.FarmParameterService;
import com.farm.web.WebUtils;
import com.farm.wuc.events.WucEventModle;
import com.farm.wuc.events.WucEventRole;
import com.wuc.client.WucEventClient;

/**
 * 测试页面
 * 
 * @author wangdogn
 *
 */
@RequestMapping("/demo")
@Controller
public class DemoController extends WebUtils {

	@Resource
	private UserServiceInter userServiceImpl;
	@Resource
	private OrganizationServiceInter organizationServiceImpl;
	private static final Logger log = Logger.getLogger(DemoController.class);

	public static String getThemePath() {
		return FarmParameterService.getInstance().getParameter("config.sys.web.themes.path");
	}

	/**
	 * 测试首页
	 */
	@RequestMapping("/PubHome")
	public ModelAndView home(Integer num, String docid, HttpSession session, HttpServletRequest request) {

		if (!getCurrentUser(session).getType().equals("3")) {
			throw new RuntimeException("无权限");
		}

		return ViewMode.getInstance().returnModelAndView(getThemePath() + "/demo/index");
	}

	/**
	 * wuc接口信息
	 */
	@RequestMapping("/wucinfo")
	public ModelAndView wucinfo(HttpSession session, HttpServletRequest request) {
		if (!getCurrentUser(session).getType().equals("3")) {
			throw new RuntimeException("无权限");
		}
		try {
			boolean isAble = true;
			if (isAble) {
				WucEventClient client = new WucEventClient(
						FarmParameterService.getInstance().getParameter("config.wuc.api.base.url"),
						FarmParameterService.getInstance().getParameter("config.wuc.api.secret"),
						FarmParameterService.getInstance().getParameter("config.wuc.api.loginname"),
						FarmParameterService.getInstance().getParameter("config.wuc.api.password"));
				isAble = client.isLive();
			}
			return ViewMode.getInstance().putAttr("isAble", isAble)
					.returnModelAndView(getThemePath() + "/demo/wucInfo");
		} catch (Exception e) {
			e.printStackTrace();
			return ViewMode.getInstance().putAttr("isEventAble", false).putAttr("isAble", false)
					.putAttr("message", e.getMessage()).returnModelAndView(getThemePath() + "/demo/wucInfo");
		}
	}

	/**
	 * 同步wuc事件配置
	 */
	@RequestMapping("/registWucEvents")
	@ResponseBody
	public Map<String, Object> registWucEvents(HttpSession session, HttpServletRequest request) {
		if (!getCurrentUser(session).getType().equals("3")) {
			throw new RuntimeException("无权限");
		}
		if (!FarmParameterService.getInstance().getParameterBoolean("config.wuc.event.able")) {
			return ViewMode.getInstance()
					.setError("config.wuc.event.able is false", new RuntimeException("config.wuc.event.able is false"))
					.returnObjMode();
		}

		log.info("同步wuc事件配置");
		try {
			WucEventClient client = new WucEventClient(
					FarmParameterService.getInstance().getParameter("config.wuc.api.base.url"),
					FarmParameterService.getInstance().getParameter("config.wuc.api.secret"),
					FarmParameterService.getInstance().getParameter("config.wuc.api.loginname"),
					FarmParameterService.getInstance().getParameter("config.wuc.api.password"));
			for (WucEventModle model : WucEventModle.values()) {
				client.registEventModel(model.getKey(), model.getTitle(), model.getNote(),
						FarmParameterService.getInstance().getParameter("config.wuc.sysid"));
			}
			for (WucEventRole role : WucEventRole.values()) {
				client.registUserRole(role.getKey(), role.getTitle(), role.getNote(),
						FarmParameterService.getInstance().getParameter("config.wuc.sysid"));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
}
