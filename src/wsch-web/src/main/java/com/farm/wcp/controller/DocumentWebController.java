package com.farm.wcp.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.iproject.domain.Pdocument;
import com.farm.iproject.service.AppmodelServiceInter;
import com.farm.iproject.service.DictypeServiceInter;
import com.farm.iproject.service.FieldServiceInter;
import com.farm.iproject.service.PdocumentServiceInter;
import com.farm.iproject.service.ProjectServiceInter;
import com.farm.iproject.service.VirtualfieldServiceInter;
import com.farm.iproject.service.VisitrecordServiceInter;
import com.farm.iproject.service.VirtualfieldServiceInter.SearchType;
import com.farm.sfile.WdapFileServiceInter;
import com.farm.web.WebUtils;
import com.farm.wuc.events.WucEventModle;
import com.farm.wuc.events.WucEventRole;
import com.farm.wuc.events.WucService;

import om.wuc.client.events.WucEventRecorder;
import om.wuc.client.events.domain.WucEventInfo;
import om.wuc.client.events.domain.WucEventUsers;

/**
 * 文件
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/document")
@Controller
public class DocumentWebController extends WebUtils {
	private static final Logger log = Logger.getLogger(DocumentWebController.class);
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private ProjectServiceInter projectServiceImpl;
	@Resource
	private FieldServiceInter fieldServiceImpl;
	@Resource
	private AppmodelServiceInter appModelServiceImpl;
	@Resource
	private VirtualfieldServiceInter virtualFieldServiceImpl;
	@Resource
	private DictypeServiceInter dicTypeServiceImpl;
	@Resource
	private PdocumentServiceInter pDocumentServiceImpl;
	@Resource
	private VisitrecordServiceInter visitRecordServiceImpl;

	/***
	 * 快照
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/Pubshot")
	public ModelAndView index(String appid, String projectid, HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		try {
			Pdocument document = pDocumentServiceImpl.getDocument(appid, projectid);
			Map<String, Object> paras = pDocumentServiceImpl.getDocumentInfo(document);
			view.putAttr("paras", paras);
			return view.putAttr("document", document).returnModelAndView("web-simple/search/docSnapshot");
		} catch (Exception e) {
			return view.setError(e.getMessage(), e).returnModelAndView("web-simple/simple-500");
		}
	}

	/***
	 * 访问原文
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/Publink")
	public ModelAndView Publink(String appid, String projectid, String resultId, HttpServletRequest request,
			HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		try {

			Pdocument doc = pDocumentServiceImpl.getDocument(appid, projectid);
			Map<String, Object> app = pDocumentServiceImpl.getDocMap(doc);
			virtualFieldServiceImpl.initVirtualField(app,
					virtualFieldServiceImpl.getAllVirtualField(doc.getProjectid()));
			String olink = (String) app.get(SearchType.OLINK.getKey());
			if (StringUtils.isNotBlank(resultId)) {
				// 只要有resultID就表示从结果集合中访问，才记录点击
				visitRecordServiceImpl.record(appid, projectid, resultId, getCurrentUser(session));
				try {
					if (WucService.isEventAble()) {
						// 记录检索事件
						WucEventRecorder reorder = WucService.getRecorder();
						WucEventUsers users = new WucEventUsers();
						users.addUser(WucEventRole.OPRETOR.getKey(), WucService.getWucUser(getCurrentUser(session)),
								getCurrentIp(request));
						WucEventInfo info = new WucEventInfo(appid, (String) app.get(SearchType.TITLE.getKey()));
						reorder.runEvent(WucService.getWucSysid(), WucEventModle.WSCH_VISIT.getKey(),
								WucEventModle.WSCH_VISIT.getTitle(), info, users, true);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (StringUtils.isBlank(olink)) {
				return view.returnRedirectUrl("/document/Pubshot.do?appid=" + appid + "&projectid=" + projectid);
			} else {
				return view.returnRedirectUrl(olink.trim());
			}
		} catch (Exception e) {
			return view.setError(e.getMessage(), e).returnModelAndView("web-simple/simple-500");
		}
	}
}
