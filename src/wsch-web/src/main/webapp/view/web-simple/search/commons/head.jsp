<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header" style="padding-left: 40px;">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<!-- <a class="navbar-brand" href="<PF:defaultIndexPage/>"
				style="padding: 5px; padding-top: 7px;"> <img
				class="img-rounded" src="<PF:basePath/>webfile/Publogo.do"
				height="40" alt="WLP" align="middle" /></a> -->

		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="<PF:defaultIndexPage/>">首页<span
						class="sr-only">(current)</span></a></li>
				<PF:WebUrls var="node" type="2">
					<li><a href='${node.LINKURL}'> <c:if
								test="${!empty node.IMGURL}">
								<img class="imgMenuIcon img-circle"
									style="width: 24px; height: 24px; position: relative; top: -2px;"
									alt="${node.NAME}" src="${node.IMGURL}" />&nbsp;</c:if> ${node.NAME}
					</a></li>
				</PF:WebUrls>
				<c:forEach items="${projects}" var="node">
					<li ${cproject.id==node.id?'class="active"':'' }><a
						href="javascript:chooseProject('${node.id}');">${node.name }<span
							class="sr-only">(current)</span></a></li>
				</c:forEach>
				<PF:WebUrls var="node" type="3">
					<li><a href='${node.LINKURL}'> <c:if
								test="${!empty node.IMGURL}">
								<img class="imgMenuIcon img-circle"
									style="width: 24px; height: 24px; position: relative; top: -2px;"
									alt="${node.NAME}" src="${node.IMGURL}" />&nbsp;</c:if> ${node.NAME}
					</a></li>
				</PF:WebUrls>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<c:if test="${USEROBJ==null}">
					<li><a href="login/webPage.do"><i
							class="glyphicon glyphicon-log-in"></i>&nbsp;用户登陆</a></li>
				</c:if>
				<c:if test="${USEROBJ!=null}">
					<li class="dropdown"><a class="dropdown-toggle"
						style="min-width: 160px;" data-toggle="dropdown" role="button"
						aria-haspopup="true" aria-expanded="false"> <img alt=""
							src="download/PubPhoto.do?id=${USEROBJ.imgid}"
							style="width: 24px; height: 24px; position: relative; top: -2px;"
							class="img-circle">&nbsp;${USEROBJ.name } <span
							class="caret"></span></a>
						<ul class="dropdown-menu">
							<!-- <li><a href="#">我的空间</a></li> -->
							<c:if test="${USEROBJ.type=='3' }">
								<li class="hidden-xs hidden-sm"><a href="frame/index.do"><i
										class="glyphicon glyphicon-wrench"></i>&nbsp;管理控制台</a></li>
							</c:if>
							<li><a href="userspace/settinginfo.do"><i
									class="glyphicon glyphicon-user"></i>&nbsp;个人信息设置</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="login/webout.do"><i
									class="glyphicon glyphicon-log-out"></i>&nbsp;注销</a></li>
						</ul></li>
				</c:if>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>
<script type="text/javascript">
	function chooseProject(projectid) {
		var word = $('#shearKey').val();
		if (word && doSearch) {
			clearLimit(false);
			$('#PROJECTID_ID').val(projectid);
			doSearch(1);
		} else {
			window.location.href = '<PF:basePath/>search/Pubhome.do?projectid='
					+ projectid;
		}
	}
</script>