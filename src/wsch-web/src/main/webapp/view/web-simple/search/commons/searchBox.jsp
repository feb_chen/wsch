<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<script type="text/javascript">
	$(function() {
		if ('${appkeyid}' == 'RESULT') {
			reResultSearch();
		}
	});
	function doSearch(page) {
		$('#SEARCHPAGE_ID').val(page)
		var word = $('#shearKey').val();
		if (word.trim()) {
			$('#SEARCHWORD_ID').val(word);
			$('#SEARCHFORM_ID').submit();
		}
	}
	function checkAppKeyID(key, name, isDoSearch) {
		$('#APPKEYTITLE_ID').text(name);
		$('#APPKEYID_ID').val(key);
		if (isDoSearch) {
			doSearch(1);
		}
	}
	function checkSearchTypeKeyID(key, name, isDoSearch) {
		$('#SEARCHFIELDTITLE_ID').text(name);
		$('#SEARCHFIELDKEYID_ID').val(key);
		if (isDoSearch) {
			doSearch(1);
		}
	}
	function calEnter(e) {
		var evt = window.event || e;
		if (evt.keyCode == 13) {
			doSearch(1);
		}
	}
	function clearLimit(isDoSearch) {
		$('#APPKEYID_ID').val('ALL');
		$('#SEARCHFIELDKEYID_ID').val('ALL');
		$('#DICRULE_ID').val('');
		$('#SORTKEY_ID').val('');
		$('#SORTTYPE_ID').val('');
		$('#SORTTITLE_ID').val('');
		if (isDoSearch) {
			doSearch(1);
		}
	}
	function hotSearch(word) {
		$('#shearKey').val(word);
		$('#SEARCHWORD_ID').val(word);
		clearLimit(true);
	}
	function checkIsSmart() {
		$('#ISSMART_ID').val($("#checkboxIsSmart").is(":checked"));
	}
	//在结果中查询
	function reResultSearch() {
		$('#APPKEYTITLE_ID').text('结果中查询');
		$('#APPKEYID_ID').val("RESULT");
	}
</script>
<form action="search/Pubdo.do" method="post" id="SEARCHFORM_ID">
	<!-- 当前结果集合 -->
	<input type="hidden" value="${result.resultId}" name="resultid"
		id="RESULTID_ID">
	<!-- 业务key -->
	<input type="hidden" value="${appkeyid}" name="appkeyid"
		id="APPKEYID_ID">
	<!-- 条件key -->
	<input type="hidden" value="${fieldkeyid}" name="fieldkeyid"
		id="SEARCHFIELDKEYID_ID">
	<!-- 检索词 -->
	<input type="hidden" value="${word}" name="word" id="SEARCHWORD_ID">
	<!-- 项目id -->
	<input type="hidden" value="${cproject.id}" name="projectid"
		id="PROJECTID_ID">
	<!-- 当前页面 -->
	<input type="hidden" value="${page}" name="page" id="SEARCHPAGE_ID">
	<!-- 当前页面 -->
	<input type="hidden" value="${dicrule}" name="dicrule" id="DICRULE_ID">
	<!-- 排序字段-->
	<input type="hidden" value="${sortkey}" name="sortkey" id="SORTKEY_ID">
	<!-- 排序类型-->
	<input type="hidden" value="${sorttype}" name="sorttype"
		id="SORTTYPE_ID">
	<!-- 排序名称-->
	<input type="hidden" value="${sorttitle}" name="sorttitle"
		id="SORTTITLE_ID">
	<!-- 精确查找-->
	<input type="hidden" value="${isSmart}" name="isSmart" id="ISSMART_ID">
</form>
<div class="input-group">
	<c:if test="${!empty fields }">
		<div class="input-group-btn">
			<button type="button" class="btn btn-default dropdown-toggle"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span id="SEARCHFIELDTITLE_ID">全部</span> <span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li><a href="javascript:checkSearchTypeKeyID('ALL','全部',true)">全部</a></li>
				<li role="separator" class="divider"></li>
				<c:forEach items="${fields }" var="node">
					<c:if test="${node.keyid==fieldkeyid }">
						<script type="text/javascript">
							checkSearchTypeKeyID('${node.keyid }',
									'${node.name }');
						</script>
					</c:if>
					<li><a
						href="javascript:checkSearchTypeKeyID('${node.keyid }','${node.name }',true)">${node.name }</a></li>
				</c:forEach>
			</ul>
		</div>
	</c:if>
	<!-- /btn-group -->
	<input type="text" onkeydown="calEnter(event)" class="form-control"
		id="shearKey" value="${word}"
		aria-label="Text input with segmented button dropdown">
	<!--  -->
	<span class="input-group-addon" title="精确检索"> <input
		type="checkbox" onchange="checkIsSmart()"
		${isSmart?'checked="checked"':''} id="checkboxIsSmart"
		aria-label="精确检索">
	</span>
	<div class="input-group-btn">
		<button type="button" class="btn btn-default" onclick="doSearch(1)">
			<i class="glyphicon glyphicon-search"></i>&nbsp; <span
				id="APPKEYTITLE_ID">检索</span>
		</button>
		<button type="button" class="btn btn-default dropdown-toggle"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="caret"></span> <span class="sr-only">Toggle
				Dropdown</span>
		</button>
		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="javascript:checkAppKeyID('ALL','检索',true)">全部</a></li>
			<li role="separator" class="divider"></li>
			<c:forEach items="${appmodels }" var="node">
				<c:if test="${node.keyid==appkeyid }">
					<script type="text/javascript">
						checkAppKeyID('${node.keyid }', '${node.name }');
					</script>
				</c:if>
				<li><a
					href="javascript:checkAppKeyID('${node.keyid }','${node.name }',true)">${node.name }</a></li>
			</c:forEach>
			<li><a href="javascript:reResultSearch()">在结果中查询</a></li>
			<li role="separator" class="divider"></li>
			<li><a href="javascript:clearLimit(true)">清除附加限制</a></li>
		</ul>
	</div>

</div>

<style>
.hotword-box {
	padding: 0px;
	padding-top: 4px;
	padding-bottom: 4px;
	line-height: 1.8em;
}

.hotword-box a {
	text-decoration: none;
}

.hotword-box .label {
	padding-left: 8px;
	padding-bottom: 3px;
	padding-right: 8px;
	padding-top: 2px;
	margin-right: 2px;
	font-size: 14px;
	font-weight: 200;
	margin-right: 2px;
}
</style>
<div>
	<div class="hotword-box">
		<c:forEach items="${hotwords }" var="node">
			<a class="hotWordsearch" href="javascript:hotSearch('${node}')">
				<span class="label label-danger">${node}</span>
			</a>
		</c:forEach>
	</div>
</div>