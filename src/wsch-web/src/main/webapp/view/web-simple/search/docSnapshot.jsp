<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${paras.TITLE}-快照</title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<!-- 导航 -->
			<jsp:include page="commons/head.jsp"></jsp:include>
		</div>
	</div>
	<!-- 表头TITLE,正文CONTENT,图片地址IMG,标签TAG,时间TIME,作者AUTHOR,原文地址OLINK,分类CATALOG -->
	<div class="container-fluid"
		style="padding-top: 70px; padding-bottom: 20px;">
		<div class="container containerbox">
			<div class="row">
				<div class="col-sm-12"
					style="margin-top: 50px; margin-bottom: 20px;">
					<h3>${paras.TITLE}</h3>
				</div>
			</div>
			<div class="row">
				<c:if test="${ !empty paras.CONTENT}">
					<div class="col-sm-8">
						<div
							style="text-align: left; font-size: 14px; line-height: 2.0em;">${paras.CONTENT}</div>
					</div>
				</c:if>
				<div class="col-sm-4">
					<div
						style="border: 1px solid #dddddd; border-top: 0px; font-size: 12px;">
						<table class="table table-striped" style="margin-bottom: 0px;">
							<tbody>
								<c:if test="${!empty paras.IMG}">
									<tr>
										<td colspan="2"
											style="font-weight: 700; text-align: right; text-align: center;">
											<img src="${paras.IMG}" style="max-height: 150px;" alt="..."
											class="img-rounded">
										</td>
									</tr>
								</c:if>
								<c:forEach items="${paras}" var="item">
									<c:if
										test="${!fn:endsWith(item.key, '_HL')&&item.key!='CONTENT'&&item.key!='TITLE'}">
										<tr>
											<td style="font-weight: 700; text-align: right;">${item.key}:</td>
											<td
												style="text-align: left; word-break: break-all; word-wrap: break-word;">
												<c:if test="${fn:length(item.value)>150}">
													${fn:substring(item.value,0,150)}...
												</c:if> <c:if test="${fn:length(item.value)<=150}">
													${item.value}
												</c:if>
											</td>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="padding: 0px;">
		<!-- 页脚 -->
		<jsp:include page="../commons/foot.jsp"></jsp:include>
	</div>
</body>
</html>