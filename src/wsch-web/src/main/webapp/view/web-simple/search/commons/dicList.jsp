<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div
	style="border: 1px solid #dddddd; color: #bbbbbb; font-size: 12px; border-radius: 12px; margin-bottom: 20px; padding-top: 20px;">
	<div
		style="padding-left: 20px; padding-right: 20px; padding-bottom: 20px;">
		<b>检索用时：</b> <br />
		<c:forEach items="${result.timeinfos }" var="node">
			<hr style="margin: 6px; margin-left: 0px; margin-right: 0px;" />
			${node.title }${node.resultNum }条,&nbsp;&nbsp;${node.runtime }秒 <br />
		</c:forEach>
		<hr style="margin: 6px; margin-left: 0px; margin-right: 0px;" />
		<b>总用时${result.searchTime}秒</b>
	</div>
</div>
<c:forEach items="${dicunits}" var="node">
	<div
		style="border: 2px dashed #cccccc; border-radius: 12px; margin-bottom: 20px; padding-top: 20px;">
		<div id="tree${node.id}"></div>
		<script type="text/javascript">
			$(function() {
				$('#tree${node.id}').treeview({
					data : JSON.parse('${node.treeDataJson}'),
					showBorder : false,
					color : "#666666",
					highlightSelected : false,
					onNodeSelected : function(event, data) {
						callTreeselectBackFun(event, data);
					}
				});
			});
			function callTreeselectBackFun(event, data) {
				try {
					$('#DICRULE_ID').val(data.id);
					doSearch(1);
				} catch (e) {
					alert('请实现方法	chooseClassTypeHandle(typekey,event, data);');
				}
			}
		</script>
	</div>
</c:forEach>
<div
	style="border: 1px solid #dddddd; color: #bbbbbb; font-size: 12px; border-radius: 12px; margin-bottom: 20px; padding-top: 20px;">
	<div
		style="padding-left: 20px; padding-right: 20px; padding-bottom: 20px; word-break: break-all;">
		<b>检索条件：</b>
		<c:forEach items="${result.ruleinfos }" var="node">
			<c:if test="${node.newGroup }">
				<hr style="margin: 6px; margin-left: 0px; margin-right: 0px;" />
				<div
					style="float: right; font-weight: 700; font-size: 10px; color: #65b3a3;">AND</div>
			</c:if>
			<div>
				${node.fieldTitle }&nbsp;&nbsp;<span style="color: #ed7b7b;">${node.searchFlag}</span>
				&nbsp;&nbsp;${node.val }
			</div>
		</c:forEach>
	</div>
</div>