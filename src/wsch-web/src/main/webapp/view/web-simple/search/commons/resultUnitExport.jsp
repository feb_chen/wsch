<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="wsch-result">
	<!-- NONE("无"), TITLE("标题"), CONTENT("正文"),OLINK("原文地址"), IMG("展示图")-->
	<!--, TAG("标签"), TIME("时间"), AUTHOR("作者") ; -->
	<div class="wsch-result-title">
		<a data-link="${node.OLINK}" target="_blank"
			href="document/Publink.do?appid=${node.APPID}&projectid=${projectid}&resultId=${result.resultId}">${node.TITLE}</a>
		<a style="font-size: 12px; text-decoration: none;"
			href="document/Pubshot.do?appid=${node.APPID}&projectid=${projectid}"
			target="_blank">查看快照</a>
	</div>
	<div>
		<div class="media">
			<c:if test="${!empty node.IMG}">
				<div class="media-left">
					<img style="max-width: 128px; border: 1px solid #dddddd;"
						class="media-object img-rounded" src="${node.IMG}" alt="检索图片">
				</div>
			</c:if>
			<div class="media-body">
				<div class="wsch-result-content">${node.CONTENT}</div>
				<c:if test="${!empty node.TAG||!empty node.APPTITLE}">
					<div class="wsch-result-tags">
						<c:if test="${!empty node.APPTITLE}">
							<span class="label label-primary">${node.APPTITLE}</span>
						</c:if>
						<c:if test="${!empty node.TAG}">
							<c:forEach items="${fn:split(node.TAG, ',')}" var="tag">
								<span class="label label-default">${tag }</span>
							</c:forEach>
						</c:if>
					</div>
				</c:if>
				<div class="row">
					<div class="col-sm-6" style="text-align: left;">
						<c:if test="${!empty node.CATALOG}">
							<span class="wsch-result-catalog">${node.CATALOG}</span>
						</c:if>

					</div>
					<div class="col-sm-6" style="text-align: right;">
						<c:if test="${!empty node.AUTHOR}">
							<span class="wsch-result-author">${node.AUTHOR}</span>
						</c:if>
						<c:if test="${!empty node.TIME}">
							<span class="wsch-result-time"><PF:FormatTime
									date="${node.TIME}" yyyyMMddHHmmss="yyyy-MM-dd HH:mm" /></span>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
