<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="index,follow">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<jsp:include page="atext/include-web.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<!-- 导航 -->
			<jsp:include page="commons/head.jsp"></jsp:include>
		</div>
	</div>
	<div class="container-fluid"
		style="padding-top: 70px; padding-bottom: 20px;">
		<div class="container containerbox">
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<div style="text-align: center; padding: 20px; padding-top: 50px;">
						<img alt="" style="text-align: center; max-width: 196px;"
							src="webfile/PubHomelogo.do">
					</div>
					<div class="list-group">
						<c:forEach items="${projects}" var="node">
							<a href="search/Pubhome.do?projectid=${node.id }" class="list-group-item">${node.name }</a>
						</c:forEach>
					</div>
				</div>
				<div class="col-sm-3"></div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6"
					style="text-align: center; padding-top: 150px;">
					<img alt="" style="max-width: 256px; max-height: 256px;"
						src="home/PubQRCode.do">
				</div>
				<div class="col-sm-3"></div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="padding: 0px;">
		<!-- 页脚 -->
		<jsp:include page="commons/foot.jsp"></jsp:include>
	</div>
</body>
</html>