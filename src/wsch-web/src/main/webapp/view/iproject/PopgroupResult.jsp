<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>权限组数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchPopgroupForm">
			<table class="editTable">
				<tr>
					<td class="title">项目:</td>
					<td><select name="A.PROJECTID:=">
							<option value="">~全部~</option>
							<c:forEach items="${projects}" var="node">
								<option value="${node.id}">${node.name }</option>
							</c:forEach>
					</select></td>
					<td class="title">组名称:</td>
					<td><input name="A.GROUPNAME:like" type="text"></td>
					<td class="title">权限key:</td>
					<td><input name="C.POPKEYID:like" type="text"></td>
					<td class="title">用户登陆名:</td>
					<td><input name="D.LOGINNAME:like" type="text"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="8"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataPopgroupGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="GROUPNAME" data-options="sortable:true" width="40">组名称</th>
					<th field="PROJECTNAME" data-options="sortable:true" width="40">项目</th>
					<th field="CTIME" data-options="sortable:true" width="40">创建时间</th>
					<th field="PSTATE" data-options="sortable:true" width="20">状态</th>
					<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionPopgroup = "popgroup/del.do";//删除URL
	var url_formActionPopgroup = "popgroup/form.do";//增加、修改、查看URL
	var url_searchActionPopgroup = "popgroup/query.do";//查询URL
	var title_windowPopgroup = "权限组管理";//功能名称
	var gridPopgroup;//数据表格对象
	var searchPopgroup;//条件查询组件对象
	var toolBarPopgroup = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataPopgroup
	}, {
		id : 'add',
		text : '新增组',
		iconCls : 'icon-add',
		handler : addDataPopgroup
	}, {
		id : 'edit',
		text : '修改组',
		iconCls : 'icon-edit',
		handler : editDataPopgroup
	}, {
		id : 'del',
		text : '删除组',
		iconCls : 'icon-remove',
		handler : delDataPopgroup
	}, {
		id : 'add',
		text : '权限管理',
		iconCls : 'icon-bestseller',
		handler : popMge
	}, {
		id : 'add',
		text : '用户管理',
		iconCls : 'icon-hire-me',
		handler : userMge
	} ];
	$(function() {
		//初始化数据表格
		gridPopgroup = $('#dataPopgroupGrid').datagrid({
			url : url_searchActionPopgroup,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarPopgroup,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchPopgroup = $('#searchPopgroupForm').searchForm({
			gridObj : gridPopgroup
		});
	});
	//权限管理
	function popMge() {
		var selectedArray = $(gridPopgroup).datagrid('getSelections');
		if (selectedArray.length >= 1) {
			var url = 'popgroupkey/list.do?groupids='
					+ $.farm.getCheckedIds(gridPopgroup, 'ID');
			$.farm.openWindow({
				id : 'winpopMge',
				width : 800,
				height : 400,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	//用户管理
	function userMge() {
		var selectedArray = $(gridPopgroup).datagrid('getSelections');
		if (selectedArray.length >= 1) {
			var url = 'popuser/list.do?groupids='
					+ $.farm.getCheckedIds(gridPopgroup, 'ID');
			$.farm.openWindow({
				id : 'winuserMge',
				width : 800,
				height : 400,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//查看
	function viewDataPopgroup() {
		var selectedArray = $(gridPopgroup).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPopgroup + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPopgroup',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataPopgroup() {
		var url = url_formActionPopgroup + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winPopgroup',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataPopgroup() {
		var selectedArray = $(gridPopgroup).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPopgroup + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPopgroup',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataPopgroup() {
		var selectedArray = $(gridPopgroup).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridPopgroup).datagrid('loading');
					$.post(url_delActionPopgroup + '?ids='
							+ $.farm.getCheckedIds(gridPopgroup, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridPopgroup).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridPopgroup).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>