<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchFieldForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataFieldGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="30">索引域名称</th>
					<th field="KEYID" data-options="sortable:true" width="30">KEYID</th>
					<th field="SORT" data-options="sortable:true" width="20">排序</th>
					<th field="TYPE" data-options="sortable:true" width="30">索引类型</th>
					<th field="TEXTTYPE" data-options="sortable:true" width="30">展示类型</th>
					<th field="SEARCHTYPE" data-options="sortable:true" width="35">检索类型</th>	
					<th field="BOOST" data-options="sortable:true" width="35">检索权重</th>	
					<th field="STOREIS" data-options="sortable:true" width="30">存储</th>
					<th field="SORTABLE" data-options="sortable:true" width="30">排序</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionField = "field/del.do";//删除URL
	var url_formActionField = "field/form.do";//增加、修改、查看URL
	var url_searchActionField = "field/query.do?projectid=${projectid}";//查询URL
	var title_windowField = "项目字段管理";//功能名称
	var gridField;//数据表格对象
	var searchField;//条件查询组件对象
	var toolBarField = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataField
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataField
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataField
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataField
	} ];
	$(function() {
		//初始化数据表格
		gridField = $('#dataFieldGrid').datagrid({
			url : url_searchActionField,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarField,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchField = $('#searchFieldForm').searchForm({
			gridObj : gridField
		});
	});
	//查看
	function viewDataField() {
		var selectedArray = $(gridField).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionField + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winField',
				width : 600,
				height : 400,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataField() {
		var url = url_formActionField + '?projectid=${projectid}&operateType='
				+ PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winField',
			width : 600,
			height : 400,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataField() {
		var selectedArray = $(gridField).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionField + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winField',
				width : 600,
				height : 400,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataField() {
		var selectedArray = $(gridField).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridField).datagrid('loading');
					$.post(url_delActionField + '?ids='
							+ $.farm.getCheckedIds(gridField, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridField).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridField).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>