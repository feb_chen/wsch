<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--项目字段表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formField">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">项目名称:</td>
					<td colspan="3">${project.name }<input type="hidden"
						id="entity_projectid" name="projectid" value="${project.id}"></td>
				</tr>
				<tr>
					<td class="title">域名称:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_name" name="name" value="${entity.name}"></td>
					<td class="title">KEYID:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_keyid" name="keyid" value="${entity.keyid}"></td>
				</tr>
				<tr>
					<td class="title">存储:</td>
					<td><select style="width: 120px;" id="entity_storeis"
						name="storeis" class="easyui-validatebox" val="${entity.storeis}"
						data-options="required:true"><option value="1">是</option>
							<option value="0">否</option></select></td>
					<td class="title">索引类型:</td>
					<td><select style="width: 120px;" id="entity_type" name="type"
						class="easyui-validatebox" val="${entity.type}"
						data-options="required:true">
							<c:forEach items="${types }" var="node">
								<option value="${node.key }">${node.title }</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td class="title">排序:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
					<td class="title">展示类型:</td>
					<td><select style="width: 120px;" id="entity_texttype"
						name="texttype" class="easyui-validatebox"
						val="${entity.texttype}" data-options="required:true">
							<option value="1">纯文本</option>
							<option value="2">HTML</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">检索类型:</td>
					<td><select style="width: 120px;" id="entity_searchtype"
						name="searchtype" class="easyui-validatebox"
						val="${entity.searchtype}" data-options="required:true">
							<option value="0">无</option>
							<option value="1">关键字检索</option>
					</select></td>
					<td class="title">排序类型:</td>
					<td><select style="width: 120px;" id="entity_sortable"
						name="sortable" class="easyui-validatebox"
						val="${entity.sortable}" data-options="required:true">
							<option value="0">无</option>
							<option value="1">支持排序</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">检索权重:</td>
					<td ><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="validType:['integer','maxLength[5]']"
						id="entity_boost" name="boost" value="${entity.boost}"></td>
					<td class="title">长度限制:</td>
					<td ><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="validType:['integer','maxLength[5]']"
						id="entity_maxlenght" name="maxlenght" value="${entity.maxlenght}"></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea style="width: 443px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityField" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityField" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formField" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionField = 'field/add.do';
	var submitEditActionField = 'field/edit.do';
	var currentPageTypeField = '${pageset.operateType}';
	var submitFormField;
	$(function() {
		//表单组件对象
		submitFormField = $('#dom_formField').SubmitForm({
			pageType : currentPageTypeField,
			grid : gridField,
			currentWindowId : 'winField'
		});
		//关闭窗口
		$('#dom_cancle_formField').bind('click', function() {
			$('#winField').window('close');
		});
		//提交新增数据
		$('#dom_add_entityField').bind('click', function() {
			submitFormField.postSubmit(submitAddActionField);
		});
		//提交修改数据
		$('#dom_edit_entityField').bind('click', function() {
			submitFormField.postSubmit(submitEditActionField);
		});
	});
//-->
</script>