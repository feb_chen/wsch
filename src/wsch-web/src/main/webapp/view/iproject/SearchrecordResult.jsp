<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>检索记录数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<!-- <div data-options="region:'north',border:false">
		<form id="searchSearchrecordForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div> -->
	<div data-options="region:'center',border:false">
		<table id="dataSearchrecordGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="WORD" data-options="sortable:true" width="50">关键字</th>
					<th field="SIZE" data-options="sortable:true" width="30">结果量</th>
					<th field="SEARCHTIME" data-options="sortable:true" width="30">用时</th>
					<th field="CTIME" data-options="sortable:true" width="30">时间</th>
					<th field="USERNAME" data-options="sortable:true" width="30">用户</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionSearchrecord = "searchrecord/del.do";//删除URL
	var url_formActionSearchrecord = "searchrecord/form.do";//增加、修改、查看URL
	var url_searchActionSearchrecord = "searchrecord/query.do";//查询URL
	var title_windowSearchrecord = "检索记录管理";//功能名称
	var gridSearchrecord;//数据表格对象
	var searchSearchrecord;//条件查询组件对象
	var toolBarSearchrecord = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataSearchrecord
	}, 
	//{
	//	id : 'add',
	//	text : '新增',
	//	iconCls : 'icon-add',
	//	handler : addDataSearchrecord
	//}, {
	//	id : 'edit',
	//	text : '修改',
	//	iconCls : 'icon-edit',
	//	handler : editDataSearchrecord
	//}, 
	{
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataSearchrecord
	} ];
	$(function() {
		//初始化数据表格
		gridSearchrecord = $('#dataSearchrecordGrid').datagrid({
			url : url_searchActionSearchrecord,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarSearchrecord,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		//searchSearchrecord = $('#searchSearchrecordForm').searchForm({
		//	gridObj : gridSearchrecord
		//});
	});
	//查看
	function viewDataSearchrecord() {
		var selectedArray = $(gridSearchrecord).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionSearchrecord + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winSearchrecord',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataSearchrecord() {
		var url = url_formActionSearchrecord + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winSearchrecord',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataSearchrecord() {
		var selectedArray = $(gridSearchrecord).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionSearchrecord + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winSearchrecord',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataSearchrecord() {
		var selectedArray = $(gridSearchrecord).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridSearchrecord).datagrid('loading');
					$.post(url_delActionSearchrecord + '?ids='
							+ $.farm.getCheckedIds(gridSearchrecord, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridSearchrecord).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridSearchrecord).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>