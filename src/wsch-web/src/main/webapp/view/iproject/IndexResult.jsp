<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>检索项目数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',border:false" style="width: 400px;">
		<table id="dataProjectGrid">
			<thead>
				<tr>
					<th field="NAME" data-options="sortable:true" width="40">项目名称</th>
					<th field="KEYID" data-options="sortable:true" width="50">KEYID</th>
					<th field="FIELDNUM" data-options="sortable:false" width="50">索引域</th>
				</tr>
			</thead>
		</table>
	</div>
	<div data-options="region:'center',title:'索引信息',border:false"
		style="padding: 5px; background: #eee;" id="infobox"></div>
</body>
<script type="text/javascript">
	$(function() {
		//初始化数据表格
		var gridProject = $('#dataProjectGrid').datagrid({
			url : "project/query.do",
			fit : true,
			fitColumns : true,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true,
			singleSelect : true
		});
		$('#dataProjectGrid').datagrid({
			onSelect : function(rowIndex, rowData) {
				$('#infobox').load('indexc/info.do?projectid=' + rowData.ID);
			}
		});
	});
</script>
</html>