<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchPopuserForm">
			<table class="editTable">
				<tr>
					<td class="title">用户:</td>
					<td><input name="C.NAME:like" type="text"></td>
					<td class="title">备注:</td>
					<td><input name="A.PCONTENT:like" type="text"></td>
					<td colspan="2"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataPopuserGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="USERNAME" data-options="sortable:true" width="40">用户</th>
					<th field="LOGINNAME" data-options="sortable:true" width="40">用户登陆名</th>
					<th field="GROUPNAME" data-options="sortable:true" width="40">组</th>
					<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionPopuser = "popuser/del.do";//删除URL
	var url_formActionPopuser = "popuser/form.do";//增加、修改、查看URL
	var url_searchActionPopuser = "popuser/query.do?groupids=${groupids}";//查询URL
	var title_windowPopuser = "权限用户管理";//功能名称
	var gridPopuser;//数据表格对象
	var searchPopuser;//条件查询组件对象
	var toolBarPopuser = [
	//                     {
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//	handler : viewDataPopuser
	//}, 
	{
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataPopuser
	}
	//, {
	//	id : 'edit',
	//	text : '修改',
	//	iconCls : 'icon-edit',
	//	handler : editDataPopuser
	//}
	, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataPopuser
	} ];
	$(function() {
		//初始化数据表格
		gridPopuser = $('#dataPopuserGrid').datagrid({
			url : url_searchActionPopuser,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarPopuser,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchPopuser = $('#searchPopuserForm').searchForm({
			gridObj : gridPopuser
		});
	});
	//查看
	function viewDataPopuser() {
		var selectedArray = $(gridPopuser).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPopuser + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPopuser',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataPopuser() {
		var url = url_formActionPopuser + '?groupids=${groupids}&operateType='
				+ PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winPopuser',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataPopuser() {
		var selectedArray = $(gridPopuser).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPopuser + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPopuser',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataPopuser() {
		var selectedArray = $(gridPopuser).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridPopuser).datagrid('loading');
					$.post(url_delActionPopuser + '?ids='
							+ $.farm.getCheckedIds(gridPopuser, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridPopuser).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridPopuser).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>