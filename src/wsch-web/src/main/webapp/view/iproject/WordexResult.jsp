<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>扩展字典数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchWordexForm">
			<table class="editTable">
				<tr>
					<td class="title">类型:</td>
					<td><select name="TYPE:=">
							<option value=""></option>
							<option value="1">扩展词</option>
							<option value="2">停止词</option>
					</select></td>
					<td class="title">词:</td>
					<td><input name="WORD:like" type="text"></td>
					<td colspan="1"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataWordexGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="TYPE" data-options="sortable:true" width="40">类型</th>
					<th field="WORD" data-options="sortable:true" width="40">词</th>
					<th field="PSTATE" data-options="sortable:true" width="60">状态</th>
					<th field="CTIME" data-options="sortable:true" width="50">创建时间</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionWordex = "wordex/del.do";//删除URL
	var url_formActionWordex = "wordex/form.do";//增加、修改、查看URL
	var url_searchActionWordex = "wordex/query.do";//查询URL
	var title_windowWordex = "扩展字典管理";//功能名称
	var gridWordex;//数据表格对象
	var searchWordex;//条件查询组件对象
	var toolBarWordex = [ {
		id : 'add',
		text : '新增:扩展词',
		iconCls : 'icon-add',
		handler : addDataWordexByTypeEx
	}, {
		id : 'add',
		text : '新增:停止词',
		iconCls : 'icon-add',
		handler : addDataWordexByTypeStop
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataWordex
	}, {
		id : 'pubAll',
		text : '部署词典',
		iconCls : '	icon-blog--arrow',
		handler : pubAll
	} //, {
	//	id : 'pubAdd',
	//	text : '增量部署词典',
	//	iconCls : '	icon-blog--arrow',
	//	handler : pubAdd
	//} 
	];
	$(function() {
		//初始化数据表格
		gridWordex = $('#dataWordexGrid').datagrid({
			url : url_searchActionWordex,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarWordex,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchWordex = $('#searchWordexForm').searchForm({
			gridObj : gridWordex
		});
	});
	//查看
	function viewDataWordex() {
		var selectedArray = $(gridWordex).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionWordex + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winWordex',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	function addDataWordexByTypeEx() {
		addDataWordex(1);
	}
	function addDataWordexByTypeStop() {
		addDataWordex(2);
	}

	//新增
	function addDataWordex(type) {
		var url = url_formActionWordex + '?type=' + type + '&operateType='
				+ PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winWordex',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataWordex() {
		var selectedArray = $(gridWordex).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionWordex + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winWordex',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataWordex() {
		var selectedArray = $(gridWordex).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridWordex).datagrid('loading');
					$.post(url_delActionWordex + '?ids='
							+ $.farm.getCheckedIds(gridWordex, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridWordex).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridWordex).datagrid('reload');
									$.messager.alert(MESSAGE_PLAT.SUCCESS,
											"重新启动服务后加载词库", 'info');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//全部部署
	function pubAll() {
		// 有数据执行操作
		$.messager.confirm(MESSAGE_PLAT.PROMPT, "确认重置全部词典？", function(flag) {
			if (flag) {
				$(gridWordex).datagrid('loading');
				$.post('wordex/allReload.do', {}, function(flag) {
					var jsonObject = JSON.parse(flag, null);
					$(gridWordex).datagrid('loaded');
					if (jsonObject.STATE == 0) {
						$(gridWordex).datagrid('reload');
					} else {
						var str = MESSAGE_PLAT.ERROR_SUBMIT
								+ jsonObject.MESSAGE;
						$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
					}
				});
			}
		});
	}
	//增量部署
	function pubAdd() {
		var selectedArray = $(gridWordex).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + "条词典，将被部署是否继续？";
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridWordex).datagrid('loading');
					$.post('wordex/addload.do?ids='
							+ $.farm.getCheckedIds(gridWordex, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridWordex).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridWordex).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>