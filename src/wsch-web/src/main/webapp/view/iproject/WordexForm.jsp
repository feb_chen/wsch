<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--扩展字典表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formWordex">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">类型:</td>
					<td colspan="3"><select name="type" id="entity_type"
						val="${entity.type}">
							<c:if test="${type==null||type=='1' }">
								<option value="1">扩展词</option>
							</c:if>
							<c:if test="${type==null||type=='2' }">
								<option value="2">停止词</option>
							</c:if>
					</select></td>
				</tr>
				<tr>
					<td class="title">词:</td>
					<td colspan="3"><textarea rows="2" style="width: 360px;"
							class="easyui-validatebox"
							data-options="required:true"
							id="entity_word" name="word">${entity.word}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityWordex" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityWordex" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formWordex" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionWordex = 'wordex/add.do';
	var submitEditActionWordex = 'wordex/edit.do';
	var currentPageTypeWordex = '${pageset.operateType}';
	var submitFormWordex;
	$(function() {
		//表单组件对象
		submitFormWordex = $('#dom_formWordex').SubmitForm({
			pageType : currentPageTypeWordex,
			grid : gridWordex,
			currentWindowId : 'winWordex'
		});
		//关闭窗口
		$('#dom_cancle_formWordex').bind('click', function() {
			$('#winWordex').window('close');
		});
		//提交新增数据
		$('#dom_add_entityWordex').bind('click', function() {
			submitFormWordex.postSubmit(submitAddActionWordex);
		});
		//提交修改数据
		$('#dom_edit_entityWordex').bind('click', function() {
			submitFormWordex.postSubmit(submitEditActionWordex);
		});
	});
//-->
</script>