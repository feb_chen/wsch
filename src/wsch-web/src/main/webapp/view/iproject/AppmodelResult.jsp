<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'west',border:false" style="width: 230px;">
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'center',border:false">
				<table id="dataAppmodelGrid">
					<thead>
						<tr>
							<th field="KEYID" data-options="sortable:true" width="50">KEYID</th>
							<th field="NAME" data-options="sortable:true" width="50">业务名称</th>
							<th field="SORT" data-options="sortable:true" width="20">排序</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div data-options="region:'center',title:'虚拟检索域管理',border:false"
		style="padding: 5px; background: #eee;">
		<jsp:include page="VirtualfieldResult.jsp"></jsp:include>
	</div>
	<div data-options="region:'east',title:'检索字典',border:false"
		style="width: 150px;">
		<jsp:include page="AppdicResult.jsp"></jsp:include>
	</div> 
</div>
<script type="text/javascript">
	var url_delActionAppmodel = "appmodel/del.do";//删除URL
	var url_formActionAppmodel = "appmodel/form.do";//增加、修改、查看URL
	var url_searchActionAppmodel = "appmodel/query.do?projectid=${projectid}";//查询URL
	var title_windowAppmodel = "业务类型管理";//功能名称
	var gridAppmodel;//数据表格对象
	var searchAppmodel;//条件查询组件对象
	var toolBarAppmodel = [ //{
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//	handler : viewDataAppmodel
	//},
	{
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataAppmodel
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataAppmodel
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataAppmodel
	} ];
	$(function() {
		//初始化数据表格
		gridAppmodel = $('#dataAppmodelGrid').datagrid({
			url : url_searchActionAppmodel,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarAppmodel,
			pagination : false,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			singleSelect : true,
			rownumbers : true,
			ctrlSelect : true
		});
		$('#dataAppmodelGrid').datagrid({
			onSelect : function(rowIndex, rowData) {
				$(gridVirtualfield).datagrid('load', {
					appmodelid : rowData.ID
				});
				$(gridAppdic).datagrid('load', {
					appmodelid : rowData.ID
				});
			}
		});
	});
	//查看
	function viewDataAppmodel() {
		var selectedArray = $(gridAppmodel).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionAppmodel + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winAppmodel',
				width : 400,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataAppmodel() {
		var url = url_formActionAppmodel
				+ '?projectid=${projectid}&operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winAppmodel',
			width : 400,
			height : 300,
			modal : true,
			url : url,
			title : '新增业务'
		});
	}
	//修改
	function editDataAppmodel() {
		var selectedArray = $(gridAppmodel).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionAppmodel + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winAppmodel',
				width : 400,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataAppmodel() {
		var selectedArray = $(gridAppmodel).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridAppmodel).datagrid('loading');
					$.post(url_delActionAppmodel + '?ids='
							+ $.farm.getCheckedIds(gridAppmodel, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridAppmodel).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridAppmodel).datagrid('reload');
									$(gridVirtualfield).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>