<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--业务类型表单-->
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'center'" style="padding: 10px;">
		<form id="vformid">
			<table>
				<c:forEach items="${fields }" var="node">
					<tr>
						<td style="text-align: right;">${node.name}:</td>
						<td style="padding: 10px;"><c:if
								test="${node.keyid=='APPKEYID'}">
								<select class="easyui-validatebox" data-options="required:true"
									name="${node.keyid}">
									<option value="">~请选择~</option>
									<c:forEach items="${models}" var="model">
										<option value="${model.keyid}">${model.name}</option>
									</c:forEach>
								</select>
							</c:if> <c:if test="${node.keyid!='APPKEYID'}">
								<c:if test="${node.type=='TextField'}">
									<textarea name="${node.keyid}" style="width: 400px;"
										class="easyui-validatebox"></textarea>
								</c:if>
								<!--  -->
								<c:if test="${node.type!='TextField'}">
									<input name="${node.keyid}" style="width: 400px;"
										class="easyui-validatebox">
								</c:if>
							</c:if></td>
					</tr>
				</c:forEach>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<a id="dom_add_entityCreatIndex" href="javascript:void(0)"
				iconCls="icon-save" class="easyui-linkbutton">提交</a> <a
				id="dom_cancle_formCreatIndex" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		//关闭窗口
		$('#dom_cancle_formCreatIndex').bind('click', function() {
			$('#winAppmodel').window('close');
		});
		//提交新增数据
		$('#dom_add_entityCreatIndex').bind('click', function() {
			if ($('#vformid').form('validate')) {
				var formObject = {};
				var formArray = $("#vformid").serializeArray();
				$.each(formArray, function(i, item) {
					formObject[item.name] = item.value;
				});
				var formJson = JSON.stringify(formObject);
				$.post('indexc/creatSubmit.do', {
					jsonstr : formJson,
					projectid : '${project.id}'
				}, function(flag) {
					if (flag.STATE == '0') {
						alert("创建成功!");
					} else {
						alert(flag.MESSAGE);
					}
				}, 'json');
			}
		});
	});
//-->
</script>