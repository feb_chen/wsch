<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--组权限表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formPopgroupkey">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<input type="hidden" id="entity_gourpids" name="groupid"
				value="${groupids}">
			<table class="editTable">
				<tr>
					<td class="title">权限KEY:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[32]']"
						id="entity_popkeyid" name="popkeyid" value="${entity.popkeyid}"><br/>
						<span style="color: green;">ALLABLE:表示拥有全部权限</span></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityPopgroupkey" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityPopgroupkey" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formPopgroupkey" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionPopgroupkey = 'popgroupkey/add.do';
	var submitEditActionPopgroupkey = 'popgroupkey/edit.do';
	var currentPageTypePopgroupkey = '${pageset.operateType}';
	var submitFormPopgroupkey;
	$(function() {
		//表单组件对象
		submitFormPopgroupkey = $('#dom_formPopgroupkey').SubmitForm({
			pageType : currentPageTypePopgroupkey,
			grid : gridPopgroupkey,
			currentWindowId : 'winPopgroupkey'
		});
		//关闭窗口
		$('#dom_cancle_formPopgroupkey').bind('click', function() {
			$('#winPopgroupkey').window('close');
		});
		//提交新增数据
		$('#dom_add_entityPopgroupkey').bind('click', function() {
			submitFormPopgroupkey.postSubmit(submitAddActionPopgroupkey);
		});
		//提交修改数据
		$('#dom_edit_entityPopgroupkey').bind('click', function() {
			submitFormPopgroupkey.postSubmit(submitEditActionPopgroupkey);
		});
	});
//-->
</script>