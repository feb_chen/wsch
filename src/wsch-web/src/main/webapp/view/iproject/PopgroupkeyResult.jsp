<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchPopgroupkeyForm">
			<table class="editTable">
				<tr>
					<td class="title">权限KEY:</td>
					<td><input name="a.POPKEYID:like" type="text"></td>
					<td class="title">备注:</td>
					<td><input name="a.PCONTENT:like" type="text"></td>
					<td colspan="2"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table> 
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataPopgroupkeyGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="GROUPNAME" data-options="sortable:true" width="30">组</th>
					<th field="POPKEYID" data-options="sortable:true" width="50">权限KEY</th>
					<th field="PCONTENT" data-options="sortable:true" width="20">备注</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionPopgroupkey = "popgroupkey/del.do";//删除URL
	var url_formActionPopgroupkey = "popgroupkey/form.do";//增加、修改、查看URL
	var url_searchActionPopgroupkey = "popgroupkey/query.do?groupids=${groupids}";//查询URL
	var title_windowPopgroupkey = "组权限管理";//功能名称
	var gridPopgroupkey;//数据表格对象
	var searchPopgroupkey;//条件查询组件对象
	var toolBarPopgroupkey = [
	//{
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//	handler : viewDataPopgroupkey
	//}, 
	{
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataPopgroupkey
	}
	//, {
	//	id : 'edit',
	//	text : '修改',
	//	iconCls : 'icon-edit',
	//	handler : editDataPopgroupkey
	//}
	, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataPopgroupkey
	} ];
	$(function() {
		//初始化数据表格
		gridPopgroupkey = $('#dataPopgroupkeyGrid').datagrid({
			url : url_searchActionPopgroupkey,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarPopgroupkey,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchPopgroupkey = $('#searchPopgroupkeyForm').searchForm({
			gridObj : gridPopgroupkey
		});
	});
	//查看
	function viewDataPopgroupkey() {
		var selectedArray = $(gridPopgroupkey).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPopgroupkey + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPopgroupkey',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataPopgroupkey() {
		var url = url_formActionPopgroupkey
				+ '?groupids=${groupids}&operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winPopgroupkey',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataPopgroupkey() {
		var selectedArray = $(gridPopgroupkey).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPopgroupkey + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPopgroupkey',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataPopgroupkey() {
		var selectedArray = $(gridPopgroupkey).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridPopgroupkey).datagrid('loading');
					$.post(url_delActionPopgroupkey + '?ids='
							+ $.farm.getCheckedIds(gridPopgroupkey, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridPopgroupkey).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridPopgroupkey).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>