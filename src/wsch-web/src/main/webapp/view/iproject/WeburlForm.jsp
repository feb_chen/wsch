<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--友情链接表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formWeburl">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">名称:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[32]']"
						id="entity_webname" name="webname" value="${entity.webname}">
					</td>
					<td class="title">类型:</td>
					<td><select name="type" id="entity_type" val="${entity.type}"><option
								value="1">底部</option>
							<option value="2">顶部前</option>
							<option value="3">顶部后</option></select></td>
				</tr>
				<tr>
					<td class="title">排序:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
					<td colspan="2" rowspan="2"
						style="border-left: 1px solid #cccccc; text-align: center;"><img
						style="width: 32px; height: 32px;" alt="" id="logoImgId"
						class="img-thumbnail"
						src="${imgurl==null?'webfile/Publogo.do':imgurl}">
						<c:if test="${pageset.operateType!=0}">
							<br />
							<span style="font-size: 12px;">宽64*高64</span>
							<br />
							<input type="hidden" name="fileid" id="imgInputId"
								value="${entity.fileid}">
							<jsp:include
								page="/view/web-simple/commons/fileUploadCommons.jsp">
								<jsp:param value="imgId" name="appkey" />
								<jsp:param value="IMG" name="type" />
								<jsp:param value="上传图标" name="title" />
							</jsp:include>
							<a href="javascript:delImg('imgInputId','logoImgId')">删除</a>
						</c:if></td>
				</tr>
				<tr>
					<td class="title">状态:</td>
					<td><select name="pstate" id="entity_pstate"
						val="${entity.pstate}"><option value="0">禁用</option>
							<option value="1">可用</option></select></td>
				</tr>
				<tr>
					<td class="title">URL:</td>
					<td colspan="3"><textarea rows="2" style="width: 360px;"
							class="easyui-validatebox"
							data-options="required:true,validType:[,'maxLength[256]']"
							id="entity_url" name="url">${entity.url}</textarea></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea rows="2" style="width: 360px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityWeburl" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityWeburl" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formWeburl" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionWeburl = 'weburl/add.do';
	var submitEditActionWeburl = 'weburl/edit.do';
	var currentPageTypeWeburl = '${pageset.operateType}';
	var submitFormWeburl;
	$(function() {
		//表单组件对象
		submitFormWeburl = $('#dom_formWeburl').SubmitForm({
			pageType : currentPageTypeWeburl,
			grid : gridWeburl,
			currentWindowId : 'winWeburl'
		});
		//关闭窗口
		$('#dom_cancle_formWeburl').bind('click', function() {
			$('#winWeburl').window('close');
		});
		//提交新增数据
		$('#dom_add_entityWeburl').bind('click', function() {
			submitFormWeburl.postSubmit(submitAddActionWeburl);
		});
		//提交修改数据
		$('#dom_edit_entityWeburl').bind('click', function() {
			submitFormWeburl.postSubmit(submitEditActionWeburl);
		});
	});
	//图片提交成功
	function fileUploadHandle(appkey, url, id, fileName) {
		if ('imgId' == appkey) {
			$('#logoImgId').attr('src', url);
			$('#imgInputId').val(id);
		}
	}
	//删除表单图片
	function delImg(inputid,imgid){
		$('#'+inputid).val('');
		$('#'+imgid).attr('src','webfile/Publogo.do');
	}
//-->
</script>