<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--虚拟字段表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formVirtualfield">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">检索业务:</td>
					<td colspan="2">${appmodel.name }<input type="hidden"
						id="entity_appmodelid" name="appmodelid" value="${appmodel.id}"></td>
					<td rowspan="4" style="border-left: 1px solid #cccccc;"><b>可用变量:</b>
						<div style="overflow: auto;height: 250px;"> 
							<c:forEach items="${fields}" var="node">
								<div>
									${node.name }:<br />
									<span style="color: green; font-weight: 700;">$[${node.keyid}]</span><br />
									<span style="color: green; font-weight: 700;">$[${node.keyid}_HL]</span>
								</div>
							</c:forEach>
						</div></td>
				</tr>
				<tr>
					<td class="title">检索域名称:</td>
					<td colspan="2"><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_name" name="name" value="${entity.name}"></td>
				</tr>
				<tr>
					<td class="title">展示类型:</td>
					<td colspan="2"><select class="easyui-validatebox"
						id="entity_type" name="type" val="${entity.type}"
						data-options="required:true">
							<option value="">~请选择~</option>
							<c:forEach items="${types }" var="node">
								<option value="${node.key }">${node.title}[${node.key}]</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td class="title">域虚拟值:</td>
					<td colspan="2">
						<div>
							<textarea style="width: 283px; height: 150px;"
								class="easyui-validatebox"
								data-options="required:true,validType:[,'maxLength[512]']"
								id="entity_formula" name="formula">${entity.formula}</textarea>

						</div>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityVirtualfield" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityVirtualfield" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formVirtualfield" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionVirtualfield = 'virtualfield/add.do';
	var submitEditActionVirtualfield = 'virtualfield/edit.do';
	var currentPageTypeVirtualfield = '${pageset.operateType}';
	var submitFormVirtualfield;
	$(function() {
		//表单组件对象
		submitFormVirtualfield = $('#dom_formVirtualfield').SubmitForm({
			pageType : currentPageTypeVirtualfield,
			grid : gridVirtualfield,
			currentWindowId : 'winVirtualfield'
		});
		//关闭窗口
		$('#dom_cancle_formVirtualfield').bind('click', function() {
			$('#winVirtualfield').window('close');
		});
		//提交新增数据
		$('#dom_add_entityVirtualfield').bind('click', function() {
			submitFormVirtualfield.postSubmit(submitAddActionVirtualfield);
		});
		//提交修改数据
		$('#dom_edit_entityVirtualfield').bind('click', function() {
			submitFormVirtualfield.postSubmit(submitEditActionVirtualfield);
		});
	});
//-->
</script>