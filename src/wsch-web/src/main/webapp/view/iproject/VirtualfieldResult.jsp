<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false">
		<table id="dataVirtualfieldGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">名称</th>
					<th field="TYPE" data-options="sortable:true" width="40">展示类型</th>
					<th field="FORMULA" data-options="sortable:true" width="70">域虚拟值</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionVirtualfield = "virtualfield/del.do";//删除URL
	var url_formActionVirtualfield = "virtualfield/form.do";//增加、修改、查看URL
	var url_searchActionVirtualfield = "virtualfield/query.do";//查询URL
	var title_windowVirtualfield = "虚拟字段管理";//功能名称
	var gridVirtualfield;//数据表格对象
	var searchVirtualfield;//条件查询组件对象
	var toolBarVirtualfield = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataVirtualfield
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataVirtualfield
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataVirtualfield
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataVirtualfield
	} ];
	$(function() {
		//初始化数据表格
		gridVirtualfield = $('#dataVirtualfieldGrid').datagrid({
			url : url_searchActionVirtualfield,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarVirtualfield,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
	});
	//查看
	function viewDataVirtualfield() {
		var selectedArray = $(gridVirtualfield).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionVirtualfield + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winVirtualfield',
				width : 600,
				height : 400,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataVirtualfield() {
		var selectedApp = $(gridAppmodel).datagrid('getSelections');
		if(!selectedApp[0]){
			alert('请选择左侧业务!');			
		}
		var appid=selectedApp[0].ID
		var url = url_formActionVirtualfield + '?appmodelid='+appid+'&operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winVirtualfield',
			width : 600,
			height : 400,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataVirtualfield() {
		var selectedArray = $(gridVirtualfield).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionVirtualfield + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winVirtualfield',
				width : 600,
				height : 400,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataVirtualfield() {
		var selectedArray = $(gridVirtualfield).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridVirtualfield).datagrid('loading');
					$.post(url_delActionVirtualfield + '?ids='
							+ $.farm.getCheckedIds(gridVirtualfield, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridVirtualfield).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridVirtualfield).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>