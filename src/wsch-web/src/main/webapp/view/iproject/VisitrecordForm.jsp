<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--访问记录表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formVisitrecord">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">原文地址:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[256]']" id="entity_olink"
						name="olink" value="${entity.olink}"></td>
				</tr>
				<tr>
					<td class="title">APPID:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[32]']"
						id="entity_appid" name="appid" value="${entity.appid}"></td>
				</tr>
				<tr>
					<td class="title">标题:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[256]']" id="entity_title"
						name="title" value="${entity.title}"></td>
				</tr>
				<tr>
					<td class="title">图片:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[256]']" id="entity_img"
						name="img" value="${entity.img}"></td>
				</tr>
				<tr>
					<td class="title">时间:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[8]']"
						id="entity_ctime" name="ctime" value="${entity.ctime}"></td>
				</tr>
				<tr>
					<td class="title">用户:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_cuser" name="cuser" value="${entity.cuser}"></td>
				</tr>
				<tr>
					<td class="title">RESULTID:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[32]']"
						id="entity_resultid" name="resultid" value="${entity.resultid}">
					</td>
				</tr>

			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityVisitrecord" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityVisitrecord" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formVisitrecord" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionVisitrecord = 'visitrecord/add.do';
	var submitEditActionVisitrecord = 'visitrecord/edit.do';
	var currentPageTypeVisitrecord = '${pageset.operateType}';
	var submitFormVisitrecord;
	$(function() {
		//表单组件对象
		submitFormVisitrecord = $('#dom_formVisitrecord').SubmitForm({
			pageType : currentPageTypeVisitrecord,
			grid : gridVisitrecord,
			currentWindowId : 'winVisitrecord'
		});
		//关闭窗口
		$('#dom_cancle_formVisitrecord').bind('click', function() {
			$('#winVisitrecord').window('close');
		});
		//提交新增数据
		$('#dom_add_entityVisitrecord').bind('click', function() {
			submitFormVisitrecord.postSubmit(submitAddActionVisitrecord);
		});
		//提交修改数据
		$('#dom_edit_entityVisitrecord').bind('click', function() {
			submitFormVisitrecord.postSubmit(submitEditActionVisitrecord);
		});
	});
//-->
</script>