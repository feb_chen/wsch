<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchPdocumentForm">
			<table class="editTable">
				<tr>
					<td class="title">APPID:</td>
					<td><input name="APPID:like" type="text"></td>
					<td class="title">批次:</td>
					<td><input name="TASKKEY:like" type="text"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataPdocumentGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="PROJECTNAME" data-options="sortable:true" width="50">项目名称</th>
					<th field="APPKEYID" data-options="sortable:true" width="50">业务KEYID</th>
					<th field="APPID" data-options="sortable:true" width="50">文档ID</th>
					<th field="PSTATE" data-options="sortable:true" width="30">状态</th>
					<th field="USERNAME" data-options="sortable:true" width="30">创建人</th>
					<th field="CTIME" data-options="sortable:true" width="50">创建时间</th>
					<th field="TASKKEY" data-options="sortable:true" width="80">任务批次</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div id="win" title="索引进度">
	<div style="text-align: center; margin-top: 50px;">
		当前进度：<span id="runProcessId"></span>
	</div>
</div>
<script type="text/javascript">
	var url_delActionPdocument = "pdocument/del.do";//删除URL
	var url_formActionPdocument = "pdocument/form.do";//增加、修改、查看URL
	var url_searchActionPdocument = "pdocument/query.do?projectid=${projectid}";//查询URL
	var title_windowPdocument = "业务文档管理";//功能名称
	var gridPdocument;//数据表格对象
	var searchPdocument;//条件查询组件对象
	var toolBarPdocument = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataPdocument
	}, {
		id : 'run',
		text : '批量执行批次任务',
		iconCls : 'icon-application_osx_terminal',
		handler : runPdocumentIndexMore
	}, {
		id : 'run',
		text : '单个执行批次任务',
		iconCls : 'icon-application_osx_terminal',
		handler : runPdocumentIndexSingle
	}, {
		id : 'run',
		text : '执行全部索引',
		iconCls : 'icon-application_osx_terminal',
		handler : runAll
	},
	//{
	//	id : 'add',
	//	text : '新增',
	//	iconCls : 'icon-add',
	//	handler : addDataPdocument
	//}, {
	///	id : 'edit',
	//	text : '修改',
	//	iconCls : 'icon-edit',
	//	handler : editDataPdocument
	//}, 
	{
		id : 'del',
		text : '删除记录和索引',
		iconCls : 'icon-remove',
		handler : delDataPdocument
	}, {
		id : 'del',
		text : '删除全部索引',
		iconCls : 'icon-remove',
		handler : delAllPdocument
	} ];
	$(function() {
		//初始化数据表格
		gridPdocument = $('#dataPdocumentGrid').datagrid({
			url : url_searchActionPdocument,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarPdocument,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchPdocument = $('#searchPdocumentForm').searchForm({
			gridObj : gridPdocument
		});
	});
	//查看
	function viewDataPdocument() {
		var selectedArray = $(gridPdocument).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPdocument + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPdocument',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataPdocument() {
		var url = url_formActionPdocument + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winPdocument',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataPdocument() {
		var selectedArray = $(gridPdocument).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionPdocument + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winPdocument',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//删除全部
	function delAllPdocument() {
		$.messager.confirm(MESSAGE_PLAT.PROMPT, "是否删除全部索引?", function(flag) {
			if (flag) {
				$(gridPdocument).datagrid('loading');
				$.post("pdocument/delall.do?projectid=${projectid}", {}, function(flag) {
					var jsonObject = JSON.parse(flag, null);
					$(gridPdocument).datagrid('loaded');
					if (jsonObject.STATE == 0) {
						$(gridPdocument).datagrid('reload');
					} else {
						var str = MESSAGE_PLAT.ERROR_SUBMIT
								+ jsonObject.MESSAGE;
						$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
					}
				});
			}
		});
	}

	//删除
	function delDataPdocument() {
		var selectedArray = $(gridPdocument).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridPdocument).datagrid('loading');
					$.post(url_delActionPdocument + '?ids='
							+ $.farm.getCheckedIds(gridPdocument, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridPdocument).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridPdocument).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	function runPdocumentIndexMore() {
		runPdocumentIndex(true);
	}
	function runPdocumentIndexSingle() {
		runPdocumentIndex(false);
	}

	//批量执行索引
	function runPdocumentIndex(isMore) {
		var selectedArray = $(gridPdocument).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + "数据将被创建索引，是否继续？";
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridPdocument).datagrid('loading');
					$.post('pdocument/runindex.do?more=' + isMore + '&ids='
							+ $.farm.getCheckedIds(gridPdocument, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridPdocument).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridPdocument).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//执行全部索引
	function runAll() {
		// 有数据执行操作
		$.messager.confirm(MESSAGE_PLAT.PROMPT, "是否立即创建全部索引", function(flag) {
			if (flag) {
				$(gridPdocument).datagrid('loading');
				$.post('pdocument/runAllindex.do?projectid=${projectid}', {},
						function(flag) {
							var jsonObject = JSON.parse(flag, null);
							$(gridPdocument).datagrid('loaded');
							if (jsonObject.STATE == 0) {
								$('#win').window({
									width : 200,
									height : 150,
									modal : true
								});
								loadProcess();
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ jsonObject.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						});
			}
		});
	}
	function loadProcess() {
		$.post('pdocument/getRunProcess.do', {}, function(flag) {
			if (flag.process) {
				$('#runProcessId').text(flag.process);
				window.setTimeout(loadProcess(), 1000);
			} else {
				$('#win').window('close');
			}
		}, 'json');
	}
</script>