<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>检索项目数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchProjectForm">
			<table class="editTable">
				<tr>
					<td class="title">KEYID:</td>
					<td><input name="KEYID:like" type="text"></td>
					<td class="title">项目名称:</td>
					<td><input name="NAME:like" type="text"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataProjectGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">项目名称</th>
					<th field="KEYID" data-options="sortable:true" width="50">KEYID</th>
					<th field="SORT" data-options="sortable:true" width="50">排序</th>
					<th field="CTIME" data-options="sortable:true" width="50">创建时间</th>
					<th field="FIELDNUM" data-options="sortable:false" width="50">索引域</th>
					<th field="APPNUM" data-options="sortable:false" width="50">检索业务</th>
					<th field="PSTATE" data-options="sortable:false" width="50">状态</th>
				</tr>
			</thead>
		</table>
	</div>
	<div id="projectToolbar">
			<a class="easyui-linkbutton"
				data-options="iconCls:'icon-tip',plain:true,onClick:viewDataProject">查看
			</a> 
			<a class="easyui-linkbutton"
				data-options="iconCls:'icon-add',plain:true,onClick:addDataProject">新增
			</a> 
			<a href="javascript:void(0)" id="mb" class="easyui-menubutton"
				data-options="menu:'#mm6',iconCls:'icon-edit'">編輯</a>
			<div id="mm6" style="width: 150px;">
				<div  data-options="iconCls:'icon-edit'" onclick="editDataProject()">修改</div>
				<div class="menu-sep"></div>
				<div  data-options="iconCls:'icon-remove'" onclick="delDataProject()">删除</div>
			</div>
			<a class="easyui-linkbutton"
				data-options="iconCls:'icon-issue',plain:true,onClick:editProjectField">索引域管理
			</a> 
			<a class="easyui-linkbutton"
				data-options="iconCls:'icon-address-book',plain:true,onClick:editProjectDicType">检索字典管理
			</a> 
			<a class="easyui-linkbutton"
				data-options="iconCls:'icon-invoice',plain:true,onClick:editProjectAppModel">检索业务管理
			</a> 
		</div>
</body>
<script type="text/javascript">
	var url_delActionProject = "project/del.do";//删除URL
	var url_formActionProject = "project/form.do";//增加、修改、查看URL
	var url_searchActionProject = "project/query.do";//查询URL
	var title_windowProject = "检索项目管理";//功能名称
	var gridProject;//数据表格对象
	var searchProject;//条件查询组件对象
	$(function() {
		//初始化数据表格
		gridProject = $('#dataProjectGrid').datagrid({
			url : url_searchActionProject,
			fit : true,
			fitColumns : true,
			'toolbar' : '#projectToolbar',
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProject = $('#searchProjectForm').searchForm({
			gridObj : gridProject
		});
	});
	//查看
	function viewDataProject() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProject + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProject',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataProject() {
		var url = url_formActionProject + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winProject',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataProject() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProject + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProject',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//索引域管理
	function editProjectField() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = 'field/list.do?projectid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProject',
				width : 800,
				height : 400,
				modal : true,
				url : url,
				title : '索引域管理'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//检索字典管理
	function editProjectDicType() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = 'dictype/list.do?projectid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjectDics',
				width : 800,
				height : 450,
				modal : true,
				url : url,
				title : '检索字典管理'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//业务管理
	function editProjectAppModel() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = 'appmodel/list.do?projectid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProject',
				width : 850,
				height : 400,
				modal : true,
				url : url,
				title : '业务管理'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataProject() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProject).datagrid('loading');
					$.post(url_delActionProject + '?ids='
							+ $.farm.getCheckedIds(gridProject, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProject).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridProject).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>