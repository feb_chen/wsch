<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<h1>注册一级业务字典</h1>
<p class="protocol">${CURL}/index/regist/appdic.do</p>
<p class="lead">通过post方式提交</p>
<h3>参数</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>属性</th>
			<th>描述</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">projectKey</th>
			<td>项目key</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">keyid</th>
			<td>字典key</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">fieldkey</th>
			<td>索引域key</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">liketype</th>
			<td>匹配模式</td>
			<td class="demo">必填,1.完全匹配，2字典为字段子集</td>
		</tr>
		<tr>
			<th scope="row">abletype</th>
			<td>启用类型</td>
			<td class="demo">必填,1.结果启用，2全局启用</td>
		</tr>
		<tr>
			<th scope="row">sumable</th>
			<td>域虚拟值</td>
			<td class="demo">必填，1.合计，2不合计</td>
		</tr>
		<tr>
			<th scope="row">name</th>
			<td>名称</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">sort</th>
			<td>排序</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">appkeys</th>
			<td>检索业务key</td>
			<td class="demo">多个间逗号分隔</td>
		</tr>
		<tr>
			<th scope="row">secret</th>
			<td>权限码</td>
			<td class="demo">必填,通过知识库配置文件预先配置</td>
		</tr>
		<tr>
			<th scope="row">operatorLoginname</th>
			<td>操作用户登陆名称</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">operatorPassword</th>
			<td>操作用户登陆密码</td>
			<td class="demo">必填</td>
		</tr>
	</tbody>
</table>
<h3>返回值</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>参数</th>
			<th>值</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">STATE</th>
			<td>状态</td>
			<td class="demo">0成功,1失败</td>
		</tr>
		<tr>
			<th scope="row">DATA</th>
			<td>业务字典ID</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">MESSAGE</th>
			<td>错误信息</td>
			<td class="demo"></td>
		</tr>
	</tbody>
</table>