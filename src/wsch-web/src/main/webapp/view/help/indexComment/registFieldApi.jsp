<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<h1>注册索引域</h1>
<p class="protocol">${CURL}/index/regist/field.do</p>
<p class="lead">通过post方式提交</p>
<h3>参数</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>属性</th>
			<th>描述</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">projectKey</th>
			<td>项目key</td>
			<td class="demo">必填</td>
		</tr>

		<tr>
			<th scope="row">name</th>
			<td>域名称</td>
			<td class="demo">必填</td>
		</tr>

		<tr>
			<th scope="row">key</th>
			<td>域key</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">storeis</th>
			<td>域是否存储</td>
			<td class="demo">必填，1是0否</td>
		</tr>
		<tr>
			<th scope="row">type</th>
			<td>域索引类型</td>
			<td class="demo">必填，TextField, StringField, FloatField</td>
		</tr>
		<tr>
			<th scope="row">sort</th>
			<td>域排序</td>
			<td class="demo">必填，整数</td>
		</tr>
		<tr>
			<th scope="row">texttype</th>
			<td>域展示类型</td>
			<td class="demo">必填，1文本,2超文本</td>
		</tr>
		<tr>
			<th scope="row">searchtype</th>
			<td>域检索类型</td>
			<td class="demo">必填，0不支持1关键字检索</td>
		</tr>


		<tr>
			<th scope="row">sortable</th>
			<td>域支持排序</td>
			<td class="demo">必填，0无，1页面排序</td>
		</tr>
		<tr>
			<th scope="row">boost</th>
			<td>域检索权重</td>
			<td class="demo">必填，整数</td>
		</tr>
		<tr>
			<th scope="row">maxlenght</th>
			<td>域最大长度</td>
			<td class="demo">必填，整数</td>
		</tr>



		<tr>
			<th scope="row">secret</th>
			<td>权限码</td>
			<td class="demo">必填,通过知识库配置文件预先配置</td>
		</tr>
		<tr>
			<th scope="row">operatorLoginname</th>
			<td>操作用户登陆名称</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">operatorPassword</th>
			<td>操作用户登陆密码</td>
			<td class="demo">必填</td>
		</tr>
	</tbody>
</table>
<h3>返回值</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>参数</th>
			<th>值</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">STATE</th>
			<td>状态</td>
			<td class="demo">0成功,1失败</td>
		</tr>
		<tr>
			<th scope="row">DATA</th>
			<td>项目信息</td>
			<td class="demo">"DATA": {<br /> "id":
				"4028840f788b9fca01788ba0f5800000",<br /> "keyid": "WCP",<br />
				"name": "WCP知识库",<br /> "pstate": "2",<br /> "sort": 1 }
			</td>
		</tr>
		<tr>
			<th scope="row">MESSAGE</th>
			<td>错误信息</td>
			<td class="demo"></td>
		</tr>
	</tbody>
</table>