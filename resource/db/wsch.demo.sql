/*
Navicat MySQL Data Transfer

Source Server         : local3306
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : wsch-trunk

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2022-04-07 17:36:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `alone_app_version`
-- ----------------------------
DROP TABLE IF EXISTS `alone_app_version`;
CREATE TABLE `alone_app_version` (
  `version` varchar(32) NOT NULL DEFAULT '',
  `update_time` varchar(32) DEFAULT NULL,
  `update_user` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_app_version
-- ----------------------------
INSERT INTO `alone_app_version` VALUES ('v0.1.1', '2022-02-21 16:07:36', 'USERNAME');

-- ----------------------------
-- Table structure for `alone_applog`
-- ----------------------------
DROP TABLE IF EXISTS `alone_applog`;
CREATE TABLE `alone_applog` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `DESCRIBES` varchar(1024) NOT NULL,
  `APPUSER` varchar(32) NOT NULL,
  `LEVELS` varchar(32) DEFAULT NULL,
  `METHOD` varchar(128) DEFAULT NULL,
  `CLASSNAME` varchar(128) DEFAULT NULL,
  `IP` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_9` (`APPUSER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_applog
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_action`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_action`;
CREATE TABLE `alone_auth_action` (
  `ID` varchar(32) NOT NULL,
  `AUTHKEY` varchar(128) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `CHECKIS` char(1) NOT NULL,
  `LOGINIS` char(1) NOT NULL COMMENT '默认所有ACTION都要登录',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_action';

-- ----------------------------
-- Records of alone_auth_action
-- ----------------------------
INSERT INTO `alone_auth_action` VALUES ('40288b854a38408e014a384de88a0005', 'action/list', '权限管理_权限定义', null, '20141211154357', '20141211154357', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38b3a6700017', 'actiontree/list', '权限管理_权限构造', null, '20141211173505', '20141211173505', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38b93d0a0022', 'log/list', '系统配置_系统日志', '', '20141211174111', '20150831184926', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38ba24f90024', 'parameter/list', '系统配置_参数定义', '', '20141211174210', '20150831185312', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bafb830026', 'parameter/editlist', '系统配置_系统参数', '', '20141211174305', '20151016154936', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bb431b0028', 'parameter/userelist', '系统配置_个性化参数', '', '20141211174324', '20151016154959', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bd26a0002a', 'dictionary/list', '系统配置_数据字典', '', '20141211174527', '20150831185552', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38beae79002d', 'user/list', '组织用户管理_用户管理', '', '20141211174708', '20150831185712', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bf10fc002f', 'organization/list', '组织用户管理_组织机构管理', '', '20141211174733', '20150831185753', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50dbdc460002', 'HhYWConsole', '系统配置_数据库管理', null, '20141216100953', '20141216100953', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50dcfa580005', 'qzTrigger/list', '任务调度_触发器定义', '', '20141216101106', '20171211230511', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50ddb7400007', 'qzTask/list', '任务调度_任务定义', '', '20141216101155', '20171211230536', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50def0aa0009', 'qzScheduler/list', '任务调度_调度实例', null, '20141216101315', '20141216101315', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402888a850bcceeb0150bcd7f9780009', 'usermessage/list', '用户消息', '', '20151031154122', '20151031154122', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('8a2831b3573c10d0015741b2273c0018', 'user/online', '系统配置_在线用户', null, '20160919170617', '20160919170617', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f7878df8da10178e2ad9b770004', 'wordex/list', '索引管理_扩展词典', null, '20210418095211', '20210418095211', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f7878d432740178d4347c1e0000', 'popgroup/list', '索引管理_检索权限', '', '20210415142512', '20210420093239', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('8af5c60d5c215c54015c216201ce0002', 'outuser/list', '用户管理_外部账户', null, '20170519234450', '20170519234450', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402889ac5da5d50f015da5d736060000', 'toolweb/sysbackup', '系统配置_系统备份向导', null, '20170803100531', '20170803100531', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f78788254120178825655fe0001', 'indexc/list', '索引管理_创建索引', null, '20210330165319', '20210330165319', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028840f78592bfb0178593613520000', 'filebase/list', '附件管理', '', '20210322171339', '20210322171339', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f787880b770017880b9b2c90001', 'project/list', '索引管理_项目管理', null, '20210330092236', '20210330092236', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402889ac5fc36ab9015fc3794fea0003', 'helper/readme', '系统配置_系统接口API', '', '20171116141708', '20181016161716', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288918646e990f01646ea253fb0001', 'messagemodel/list', '用户消息模板', null, '20180706160809', '20180706160809', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f7878e4d40e0178e4e011470001', 'searchrecord/list', '检索记录_检索记录', null, '20210418200633', '20210418200633', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f7878e4d40e0178e4e11b650003', 'visitrecord/list', '检索记录_访问记录', null, '20210418200741', '20210418200741', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402882867f29e426017f29e795ee0001', 'weburl/list', '运维管理_友情链接', null, '20220224120517', '20220224120517', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b352fa8016b3660a1a601cc', 'user/chooseUser', '选择用户', '', '20190608171710', '20190608171710', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366111016b3664ebb30002', 'user/query', '查询用户', '', '20190608172152', '20190608172152', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366557016b3666da4b002a', 'organization/chooseOrg', '选择组织机构', '', '20190608172358', '20190608172358', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366557016b3667b48a002b', 'organization/organizationTree', '查询组织机构树', '', '20190608172454', '20190608172454', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366557016b36682d08002c', 'post/choosePost', '选择岗位', '', '20190608172525', '20190608172525', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816b366557016b3668a27c002d', 'post/query', '查询岗位', '', '20190608172555', '20190608172555', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '0', '1');
INSERT INTO `alone_auth_action` VALUES ('40289f0e770e500201770e51f5bc0001', 'post/list', '用户管理_岗位管理', null, '20210117110948', '20210117110948', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');

-- ----------------------------
-- Table structure for `alone_auth_actiontree`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_actiontree`;
CREATE TABLE `alone_auth_actiontree` (
  `ID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) NOT NULL COMMENT '分类、菜单、权限',
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `UUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `ACTIONID` varchar(32) DEFAULT NULL,
  `DOMAIN` varchar(64) NOT NULL,
  `ICON` varchar(64) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  `PARAMS` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_7` (`ACTIONID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`action`) REFER `alone/alone_action`';

-- ----------------------------
-- Records of alone_auth_actiontree
-- ----------------------------
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38b93d0a0023', '10', '8a2831b35ac74f63015ae4710d6a005b', '系统日志', '8a2831b35ac74f63015ae4710d6a005b40288b854a38974f014a38b93d0a0023', '', '2', '20141211174111', '20141211174111', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38b93d0a0022', 'alone', 'icon-tip', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38b3a6700018', '2', '40288b854a38408e014a384c4f3c0002', '权限构造', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38b3a6700018', '', '2', '20141211173505', '20141211173505', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38b3a6700017', 'alone', 'icon-category', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38408e014a384c4f3c0002', '8', 'NONE', '系统配置', '40288b854a38408e014a384c4f3c0002', '', '1', '20141211154212', '20200612190530', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-sprocket_dark', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bafb830027', '3', '40288b854a38408e014a384c4f3c0002', '参数设置', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bafb830027', '', '2', '20141211174305', '20151016155834', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bafb830026', 'alone', 'icon-sprocket_dark', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bd26a0002b', '5', '40288b854a38408e014a384c4f3c0002', '数据字典', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bd26a0002b', '', '2', '20141211174527', '20141211174527', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bd26a0002a', 'alone', 'icon-address-book', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38be1805002c', '7', 'NONE', '用户管理', '40288b854a38974f014a38be1805002c', '', '1', '20141211174629', '20200612190559', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-group_green_edit', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38beae79002e', '1', '40288b854a38974f014a38be1805002c', '用户管理', '40288b854a38974f014a38be1805002c40288b854a38974f014a38beae79002e', '', '2', '20141211174708', '20141211174708', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38beae79002d', 'alone', 'icon-hire-me', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bf10fc0030', '2', '40288b854a38974f014a38be1805002c', '组织管理', '40288b854a38974f014a38be1805002c40288b854a38974f014a38bf10fc0030', '', '2', '20141211174733', '20170319103800', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bf10fc002f', 'alone', 'icon-customers', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888ac506eb41b01506fa9b0a30027', '1', '40288b854a38408e014a384c4f3c0002', '注册参数', '40288b854a38408e014a384c4f3c0002402888ac506eb41b01506fa9b0a30027', '', '2', '20151016160003', '20151016160003', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38ba24f90024', 'alone', 'icon-edit', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a50d918014a50dc82980004', '11', '40288b854a38408e014a384c4f3c0002', '任务调度', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc82980004', '', '1', '20141216101036', '20141216101036', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-application_osx_terminal', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a50d918014a50def0aa000a', '3', '40288b854a50d918014a50dc82980004', '调度实例', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc8298000440288b854a50d918014a50def0aa000a', '', '2', '20141216101315', '20141216101315', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a50d918014a50def0aa0009', 'alone', 'icon-future-projects', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402894ca4a9a155d014a9a1790810004', '1', '8a2831b35ac74f63015ae4710d6a005b', '附件管理', '8a2831b35ac74f63015ae4710d6a005b402894ca4a9a155d014a9a1790810004', '', '2', '20141230152723', '20210322171351', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028840f78592bfb0178593613520000', 'alone', 'icon-save', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888a84f835c36014f835f72cb000e', '1', '40288b854a38408e014a384c4f3c0002', '权限定义', '40288b854a38408e014a384c4f3c0002402888a84f835c36014f835f72cb000e', '', '2', '20150831184834', '20150831184834', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38408e014a384de88a0005', 'alone', 'icon-communication', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f78788254120178825655ff0002', '2', '40289f787880b770017880b926b60000', '索引管理', '40289f787880b770017880b926b6000040289f78788254120178825655ff0002', '', '2', '20210330165319', '20210330170128', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40289f78788254120178825655fe0001', 'alone', 'icon-database', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('8a2831b3573c10d0015741b2273c0019', '6', '8a2831b35ac74f63015ae4710d6a005b', '在线用户', '8a2831b35ac74f63015ae4710d6a005b8a2831b3573c10d0015741b2273c0019', '', '2', '20160919170617', '20160919170617', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '8a2831b3573c10d0015741b2273c0018', 'alone', 'icon-group_green_new', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('8a2831b35ac74f63015ae4710d6a005b', '9', 'NONE', '运维管理', '8a2831b35ac74f63015ae4710d6a005b', '', '1', '20170319104138', '20200612190510', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-showreel', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f787880b770017880b926b60000', '1', 'NONE', '索引管理', '40289f787880b770017880b926b60000', '', '1', '20210330092201', '20210330092201', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-database', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402889ac5da5d50f015da5d736110001', '15', '8a2831b35ac74f63015ae4710d6a005b', '系统备份向导', '8a2831b35ac74f63015ae4710d6a005b402889ac5da5d50f015da5d736110001', '', '2', '20170803100531', '20170803100623', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402889ac5da5d50f015da5d736060000', 'alone', 'icon-save', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f787880b770017880b9b2c90002', '1', '40289f787880b770017880b926b60000', '项目管理', '40289f787880b770017880b926b6000040289f787880b770017880b9b2c90002', '', '2', '20210330092236', '20210331101609', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40289f787880b770017880b9b2c90001', 'alone', 'icon-consulting', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880ef6046149d0160461a366b0001', '1', '40288b854a50d918014a50dc82980004', '任务定义', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc82980004402880ef6046149d0160461a366b0001', '', '2', '20171211230331', '20171211230331', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a50d918014a50ddb7400007', 'alone', 'icon-business-contact', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880ef6046149d0160461aac480002', '2', '40288b854a50d918014a50dc82980004', '触发器定义', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc82980004402880ef6046149d0160461aac480002', '', '2', '20171211230401', '20171211230401', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a50d918014a50dcfa580005', 'alone', 'icon-full-time', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f7878d432740178d4347c280001', '3', '40289f787880b770017880b926b60000', '检索权限', '40289f787880b770017880b926b6000040289f7878d432740178d4347c280001', '', '2', '20210415142512', '20210415142512', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40289f7878d432740178d4347c1e0000', 'alone', 'icon-group_green_new', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f7878df8da10178e2ad9b780005', '4', '40289f787880b770017880b926b60000', '扩展词典', '40289f787880b770017880b926b6000040289f7878df8da10178e2ad9b780005', '', '2', '20210418095211', '20210418095211', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40289f7878df8da10178e2ad9b770004', 'alone', 'icon-library', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f7878e4d40e0178e4df1fba0000', '3', 'NONE', '检索记录', '40289f7878e4d40e0178e4df1fba0000', '', '1', '20210418200531', '20210418200531', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-cv', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f7878e4d40e0178e4e011480002', '1', '40289f7878e4d40e0178e4df1fba0000', '检索记录', '40289f7878e4d40e0178e4df1fba000040289f7878e4d40e0178e4e011480002', '', '2', '20210418200633', '20210418200633', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40289f7878e4d40e0178e4e011470001', 'alone', 'icon-future-projects', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f7878e4d40e0178e4e11b650004', '2', '40289f7878e4d40e0178e4df1fba0000', '访问记录', '40289f7878e4d40e0178e4df1fba000040289f7878e4d40e0178e4e11b650004', '', '2', '20210418200741', '20210418200741', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40289f7878e4d40e0178e4e11b650003', 'alone', 'icon-finished-work', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402882867f29e426017f29e795ee0002', '3', '8a2831b35ac74f63015ae4710d6a005b', '友情链接', '8a2831b35ac74f63015ae4710d6a005b402882867f29e426017f29e795ee0002', '', '2', '20220224120517', '20220224120517', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402882867f29e426017f29e795ee0001', 'alone', 'icon-link', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f0e768e323c01768e33f7700000', '2', '8a2831b35ac74f63015ae4710d6a005b', 'AIP接口说明', '8a2831b35ac74f63015ae4710d6a005b40289f0e768e323c01768e33f7700000', '', '2', '20201223140539', '20201223140539', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402889ac5fc36ab9015fc3794fea0003', 'alone', 'icon-communication', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40289f0e770e500201770e51f5bd0002', '3', '40288b854a38974f014a38be1805002c', '岗位管理', '40288b854a38974f014a38be1805002c40289f0e770e500201770e51f5bd0002', '', '2', '20210117110948', '20210117162035', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40289f0e770e500201770e51f5bc0001', 'alone', 'icon-special-offer', null, '');

-- ----------------------------
-- Table structure for `alone_auth_organization`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_organization`;
CREATE TABLE `alone_auth_organization` (
  `ID` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `NAME` varchar(64) NOT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `STATE` char(1) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `SORT` int(11) DEFAULT NULL,
  `TYPE` char(1) NOT NULL COMMENT '组织类型：1科室、2班组、3队组、0其他',
  `APPID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='组织类型：科室、班组、队组、其他';

-- ----------------------------
-- Records of alone_auth_organization
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_outuser`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_outuser`;
CREATE TABLE `alone_auth_outuser` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `ACCOUNTID` varchar(64) NOT NULL,
  `ACCOUNTNAME` varchar(64) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_54` (`USERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_outuser
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_pop`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_pop`;
CREATE TABLE `alone_auth_pop` (
  `ID` varchar(32) NOT NULL,
  `POPTYPE` varchar(1) NOT NULL COMMENT '1人、2组织机构、3岗位',
  `OID` varchar(32) NOT NULL COMMENT '人ID、组织机构ID、岗位ID',
  `ONAME` varchar(128) NOT NULL COMMENT '人NAME、组织机构NAME、岗位NAME',
  `TARGETTYPE` varchar(64) NOT NULL COMMENT '权限业务类型',
  `TARGETID` varchar(32) NOT NULL COMMENT '权限业务ID',
  `TARGETNAME` varchar(128) DEFAULT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_pop
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_post`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_post`;
CREATE TABLE `alone_auth_post` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `ORGANIZATIONID` varchar(32) DEFAULT NULL,
  `NAME` varchar(64) NOT NULL,
  `EXTENDIS` varchar(2) NOT NULL COMMENT '0:否1:是（默认否）',
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_51` (`ORGANIZATIONID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_post
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_postaction`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_postaction`;
CREATE TABLE `alone_auth_postaction` (
  `ID` varchar(32) NOT NULL,
  `MENUID` varchar(32) NOT NULL,
  `POSTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_8` (`MENUID`),
  KEY `FK_Reference_9` (`POSTID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`actionid`) REFER `alone/alone_actio';

-- ----------------------------
-- Records of alone_auth_postaction
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_user`;
CREATE TABLE `alone_auth_user` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL COMMENT 'MD5(password+loginname)',
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) DEFAULT NULL COMMENT '1:系统用户:2其他3超级用户',
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `LOGINNAME` varchar(64) NOT NULL,
  `LOGINTIME` varchar(14) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_user';

-- ----------------------------
-- Records of alone_auth_user
-- ----------------------------
INSERT INTO `alone_auth_user` VALUES ('40288b854a329988014a329a12f30002', '系统管理员', '6C832EDB4410D94088918F240949593F', '', '3', '20141210130925', '20220222145641', 'userId', '2c948a84768ead2601768f12f8930000', '1', 'sysadmin', '20220407172502', null);
INSERT INTO `alone_auth_user` VALUES ('2c948a84768ead2601768f12f8930000', 'apiUser', 'D593292F60361316D8BDF7997262E814', '', '9', '20201223180913', '20220222145644', '40288b854a329988014a329a12f30002', '2c948a84768ead2601768f12f8930000', '1', 'apiuser', null, null);

-- ----------------------------
-- Table structure for `alone_auth_userorg`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_userorg`;
CREATE TABLE `alone_auth_userorg` (
  `ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `ORGANIZATIONID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `DOC_USERID` (`USERID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`organizationid`) REFER `alone/alone';

-- ----------------------------
-- Records of alone_auth_userorg
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_userpost`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_userpost`;
CREATE TABLE `alone_auth_userpost` (
  `ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `POSTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_userpost
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_dictionary_entity`
-- ----------------------------
DROP TABLE IF EXISTS `alone_dictionary_entity`;
CREATE TABLE `alone_dictionary_entity` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL DEFAULT '',
  `NAME` varchar(128) NOT NULL,
  `ENTITYINDEX` varchar(128) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB';

-- ----------------------------
-- Records of alone_dictionary_entity
-- ----------------------------
INSERT INTO `alone_dictionary_entity` VALUES ('03d878673859473bb0c45c15550fd3a0', '201911152230', '201911152230', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'byg0DQRjkhP0vllQ+zLvCzdg1Pdg+YKn', 'V6iir0l6ivY=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('1f2717f390fa4031ab94edfa905dddda', '201912031851', '201912031851', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'byg0DQRjkhP0vllQ+zLvCzdg1Pdg+YKn', 'c0HaCJMNIxQ=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('38d88c1fe0ff49138978919861797b4a', '202004021006', '202004021006', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'byg0DQRjkhP0vllQ+zLvCzdg1Pdg+YKn', 'wYmfCS4enGo=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('402880e778ea8e510178eaa49a9e002c', '202104192259', '202104192303', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '热词白名单', 'HOTWORD_WHITE', '{知识库:4028840f788b9fca01788ba0f5800000, 学习:4028840f788b9fca01788ba0f5800000}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('402880e778ea8e510178eaa4ebc2002d', '202104192259', '202104192303', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '热词黑名单', 'HOTWORD_BLACK', '{999:4028840f788b9fca01788ba0f5800000}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('402894ca49600f600149602141820000', '201410301617', '201410301617', '12312323123123', '12312323123123', '1', '系统菜单域', 'ALONE_MENU_DOMAIN', '{前台:HOME}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('6578fd8259db44b8ad7e711f7ec78b17', '201905052103', '201905052103', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'niHH8X7aa8Hns5ZUIlJ6Xh5Cxuc+BbqE', '9zmyZ8IsSXs=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('7263206837704f3389782fdddf33d550', '201811122059', '201811122059', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'niHH8X7aa8Hns5ZUIlJ6Xh5Cxuc+BbqE', 'dLRC0l1Fl/Q=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('9086ce27f89e4be7ba38cc1fb054eb1f', '202002102105', '202002102105', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'byg0DQRjkhP0vllQ+zLvCzdg1Pdg+YKn', '9wx62yld1Q0=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('9c157c0818ef4784aa620eb3854d0f17', '201810161509', '201810161509', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'rYGht6RSk5hudeZx88NZ/907OP6ufvcY', '+nQHCpAShLw=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('a736bf99b8a1404fa00d4df05b783fd2', '202202221214', '202202221214', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', '13111058226', 'wsch-C641E', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('e9c306a8afaf454cb5e6c9bfff5d5496', '202003010807', '202003010807', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'byg0DQRjkhP0vllQ+zLvCzdg1Pdg+YKn', 'I82OGeY6zPA=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('f1c8290d909f4ab5beeb5455e9c76ec1', '201902091940', '201902091940', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'niHH8X7aa8Hns5ZUIlJ6Xh5Cxuc+BbqE', 'JXAVJSQ//As=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('f326be256a1c41a69ee9e53b2aa4e4bf', '202001031615', '202001031615', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'byg0DQRjkhP0vllQ+zLvCzdg1Pdg+YKn', 'l0JuMxgazxY=', '{none}', '1');
INSERT INTO `alone_dictionary_entity` VALUES ('f342d1d9b8774d91a20bd4154ca36585', '202005051713', '202005051713', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '0', 'byg0DQRjkhP0vllQ+zLvCzdg1Pdg+YKn', 'Es7FB194xeo=', '{none}', '1');

-- ----------------------------
-- Table structure for `alone_dictionary_type`
-- ----------------------------
DROP TABLE IF EXISTS `alone_dictionary_type`;
CREATE TABLE `alone_dictionary_type` (
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `ENTITYTYPE` varchar(128) NOT NULL,
  `ENTITY` varchar(32) NOT NULL,
  `ID` varchar(32) NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TREECODE` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_8` (`ENTITY`) USING BTREE,
  CONSTRAINT `alone_dictionary_type_ibfk_1` FOREIGN KEY (`ENTITY`) REFERENCES `alone_dictionary_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_dictionary_type';

-- ----------------------------
-- Records of alone_dictionary_type
-- ----------------------------
INSERT INTO `alone_dictionary_type` VALUES ('202104192305', '202104192313', '40288b854a329988014a329a12f30002', '系统管理员', '1', '知识库', '', '4028840f788b9fca01788ba0f5800000', '402880e778ea8e510178eaa49a9e002c', '402880e778ea8e510178eaaab4470034', '1', 'NONE', '402880e778ea8e510178eaaab4470034');
INSERT INTO `alone_dictionary_type` VALUES ('202104192314', '202104192314', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '学习', '', '4028840f788b9fca01788ba0f5800000', '402880e778ea8e510178eaa49a9e002c', '402880e778ea8e510178eab2a36e0036', '2', 'NONE', '402880e778ea8e510178eab2a36e0036');
INSERT INTO `alone_dictionary_type` VALUES ('202104192324', '202104192324', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '999', '', '4028840f788b9fca01788ba0f5800000', '402880e778ea8e510178eaa4ebc2002d', '402880e778ea8e510178eabbc0790058', '1', 'NONE', '402880e778ea8e510178eabbc0790058');
INSERT INTO `alone_dictionary_type` VALUES ('201410301617', '201410301617', '12312323123123', '12312323123123', '1', '前台', '', 'HOME', '402894ca49600f600149602141820000', '402894ca49600f6001496021a7020001', '1', 'NONE', '402894ca49600f6001496021a7020001');

-- ----------------------------
-- Table structure for `alone_parameter`
-- ----------------------------
DROP TABLE IF EXISTS `alone_parameter`;
CREATE TABLE `alone_parameter` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `STATE` char(1) NOT NULL,
  `PKEY` varchar(64) NOT NULL,
  `PVALUE` varchar(2048) NOT NULL,
  `RULES` varchar(256) DEFAULT NULL,
  `DOMAIN` varchar(64) DEFAULT NULL,
  `COMMENTS` varchar(256) DEFAULT NULL,
  `VTYPE` char(1) NOT NULL COMMENT ' 文本：1 枚举：2',
  `USERABLE` varchar(1) NOT NULL COMMENT '0否，1是',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; InnoDB free: 12288 kB alone_parameter';

-- ----------------------------
-- Records of alone_parameter
-- ----------------------------
INSERT INTO `alone_parameter` VALUES ('2c948a837f2e8609017f2e8776ea0004', '202202250938', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wuc.api.loginname', '1', 'config.wuc.api.loginname', 'wucApi', '', 'WUC配置', '接口用户登录名', '1', '0');
INSERT INTO `alone_parameter` VALUES ('2c948a837f2e8609017f2e8776860002', '202202250938', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wuc.api.base.url', '1', 'config.wuc.api.base.url', 'http://127.0.0.1/wuc', '', 'WUC配置', 'WUC根地址：如http://IP:PORT/APPKEY', '1', '0');
INSERT INTO `alone_parameter` VALUES ('2c948a837f2e8609017f2e8776cd0003', '202202250938', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wuc.api.secret', '1', 'config.wuc.api.secret', 'F59BD6', '', 'WUC配置', 'WUC的secret', '1', '0');
INSERT INTO `alone_parameter` VALUES ('2c948a837f2e8609017f2e8776720001', '202202250938', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wuc.api.password', '1', 'config.wuc.api.password', '111111', '', 'WUC配置', '接口用户登录密码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('2c948a837f2e8609017f2e8776490000', '202202250938', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wuc.event.able', '1', 'config.wuc.event.able', 'false', '', 'WUC配置', '是否启用WUC事件记录', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f7878db40dc0178db465d170001', '202104162322', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.index.search.top.num', '1', 'config.index.search.top.num', '2000', '', '索引文件', '最多检索多少条记录', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f7878db40dc0178db465c670000', '202104162322', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.index.search.content.size', '1', 'config.index.search.content.size', '120', '', '索引文件', '展示文字最大长度', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b4a002c', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.secret.key', '1', 'config.restful.secret.key', 'F59BD65F7EDAFB087A81D4DCA06C4910', '', 'restful接口', '系统通用的接口密码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b45002b', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.login.default.url', '1', 'config.login.default.url', 'default', '', '登录,注册', '默认的登陆页面,可选项:default[默认内部登陆窗口],/ldap/PubLogin.html[ldap登陆窗口]', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b3e002a', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.img.upload.length.max', '1', 'config.doc.img.upload.length.max', '10485760', '', '文件', '上传图片允许文件大小,单位是字节', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b2f0029', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.local.http.port', '1', 'config.local.http.port', '8080', '', '本地服務器', '本地http端口', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b2b0028', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.media.upload.types', '1', 'config.doc.media.upload.types', 'mp4,mp3', '', '文件', '上传多媒体允许的后缀名、文件类型', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b260027', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.verifycode.able', '1', 'config.sys.verifycode.able', 'true', '', '登录,注册', '是否启用用户登录验证码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b1f0026', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.firstlogin.message', '1', 'config.sys.firstlogin.message', '欢迎使用本系统，如果您的密码是系统自动生成的请及时重置密码!', '', '登录,注册', '用户首次正常登录时的提示信息', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b1b0025', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.firstBind.message', '1', 'config.sys.firstBind.message', '欢迎使用本系统!', '', '登录,注册', '用户首次通过外部账号绑定到系统登录后收到的提示信息', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b0e0024', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.foot', '1', 'config.sys.foot', 'WSCH鉴权检索平台', '', '文字标记/通用配置', '系统页面最下方显示', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b0a0023', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.state', '1', 'config.restful.state', 'true', '', 'restful接口', '是否启用restful接口', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836afe0021', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.img.url', '1', 'config.doc.img.url', 'webfile/Publoadimg.do?id=', '', '文件', '', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836b060022', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.upload.length.max', '1', 'config.doc.upload.length.max', '1073741824', '', '文件', '上传文件允许文件大小,单位是字节', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836afb0020', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.whitelist.ips', '1', 'config.restful.whitelist.ips', '127.0.0.1,192.*.*.*', '', 'restful接口', '允许访问的白名单，多个ip间用逗号分隔支持通配符192.168.1.1,192.168.*.2,192.168.3.17-192.168.3.38', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836af6001f', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.link.newwindow.target', '1', 'config.sys.link.newwindow.target', '_blank', '', '文字标记/通用配置', '是否允许系统在新窗口打开页面_self（在本窗口打开页面）/_blank（在新窗口打开页面）', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836af1001e', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.logout.plus.url', '1', 'config.logout.plus.url', 'NONE', '', '登录,注册', '外部注销地址，本地注銷完成后將跳轉到此地址,可选项:NONE[无外部注销地址],http://127.0.0.1:8080/cas/login/logout.do', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836ae3001b', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.whitelist.state', '1', 'config.restful.whitelist.state', 'true', '', 'restful接口', '是否启用ip白名单', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836ae7001c', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.show.out.regist.able', '1', 'config.show.out.regist.able', 'true', '', '登录,注册', '是否允许外部账户绑定新用户', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836aec001d', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.registed.audit', '1', 'config.registed.audit', 'false', '', '登录,注册', '用户前台注册后是否为待审核状态,true为需要审核,false直接为可用状态', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836adb001a', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.secret.type', '1', 'config.restful.secret.type', 'complex', '', 'restful接口', '权限校验模式complex[使用安全码的同时必须带入操作人信息]|simple[简单模式直接使用安全码访问]|none[不校验验证码，只校验ip白名单]', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836acb0017', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.show.local.regist.able', '1', 'config.show.local.regist.able', 'true', '', '登录,注册', '是否允许本地注册新用户', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836ad10018', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.index.dir', '1', 'config.index.dir', 'D:\\wschServer\\resource\\index', '', '索引文件', '索引文件夹地址', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836ad60019', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.type.collapse.level', '1', 'config.sys.type.collapse.level', '2', '', '文字标记/通用配置', '前台页面知识分类页面中，树形分类在第几级开始折叠,可选1/2/3', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836abd0016', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.none.photo.path', '1', 'config.doc.none.photo.path', 'text/img/photo.png', '', '文件', '如果头像文件不存在时，默认的图片', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836ab70015', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.password.provider.type', '1', 'config.password.provider.type', 'SAFE', '', '密码', '用户密碼的实现类型[SIMPLE:简单类型(4.3.1之前得版本使用),SAFE:安全类型(4.3.1以及之后初装版本可以使用)]', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836aa30013', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.dir', '1', 'config.doc.dir', 'default:D:\\wschServer\\resource\\file', '', '文件', '附件文件夹地址', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836ab00014', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.img.compress.length.max', '1', 'config.doc.img.compress.length.max', '400', '', '文件', '图片自动压缩时允许最大图片大小,单位是kb', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a9e0012', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.enforce.password.update', '1', 'config.sys.enforce.password.update', 'false', '', '密码', '是否强制当前用户修改默认密码(true强制修改),在当前用户密码和默认密码一致时（没有修改过默认密码）。', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a8e0011', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.local.http.ip', '1', 'config.local.http.ip', '127.0.0.1', '', '本地服務器', '本地IP', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a81000f', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.download.url', '1', 'config.doc.download.url', 'download/Pubfile.do?id=', '', '文件', '', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a870010', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.useredit.showName', '1', 'config.useredit.showName', 'true', '', '用户权限，用户信息修改 ', '前台编辑当前用户信息时，是否可以修改用户名', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a7a000e', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.none.default.path', '1', 'config.doc.none.default.path', 'text/img/none.png', '', '文件', '如果文件不存在时，默认的下載文件', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a74000d', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.verifycode.checknum', '1', 'config.sys.verifycode.checknum', '3', '', '登录,注册', '用户免验证码登录失败次数，超过该数量就启用验证码，为0时总是启用验证码，外部免登绑定账户时直接启用验证码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a69000b', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wuc.sso.url', '1', 'config.wuc.sso.url', 'url', '', '单点登陆', 'wuc地址url', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a65000a', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.password.update.regex', '1', 'config.sys.password.update.regex', '(.*)', '', '密码', '当前用户前台修改密码时,密码验证-正则表达式,如:密码规则为字母加数字至少 6位:^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,32}$,如:密码不包含换行符:(.*)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a6e000c', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.restful.debug', '1', 'config.restful.debug', 'false', '', 'restful接口', '是否启用接口调试模式', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a600009', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.link.docview.target', '1', 'config.sys.link.docview.target', '_blank', '', '文字标记/通用配置', '附件预览时是否在新窗口打开预览页面auto(自动)/_blank(强制新窗口)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a5b0008', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.img.upload.types', '1', 'config.doc.img.upload.types', 'png,jpg,jpeg,gif', '', '文件', '上传图片允许的后缀名、文件类型', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a4f0006', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wuc.sso.able', '1', 'config.wuc.sso.able', 'false', '', '单点登陆', '是否启用单点登陆', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a540007', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.perfect.userinfo.able', '1', 'config.sys.perfect.userinfo.able', 'false', '', '登录,注册', '用户登录后是否必须先完善个人信息,true时，如果用户没有完善个人信息直接跳到个人信息编辑页面(isCompleteUserInfo(LoginUser currentUser)方法判断用户信息是否完善)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a390004', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.doc.upload.types', '1', 'config.doc.upload.types', 'png,jpg,jpeg,zip,doc,docx,xls,xlsx,pdf,ppt,pptx,web,rar,txt,flv,mp3,mp4,dcr', '', '文件', '上传文件允许的后缀名、文件类型', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a410005', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.regist.showOrg', '1', 'config.regist.showOrg', 'true', '', '登录,注册', '注册用户时是否要求选择组织机构', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a340003', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.password.update.tip', '1', 'config.sys.password.update.tip', '密码不能包含换行符', '', '密码', '当前用户前台修改密码时,密码验证-正则表达式验证失败提示信息', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a2c0001', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.url.free.path.prefix', '1', 'config.url.free.path.prefix', 'Pub', '', '登录,注册', '是否允许用户不登录就访问知识页面，可选项：NONEPAGE(不允许访问)Pub(允许访问)', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a310002', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.default.password', '1', 'config.default.password', '111111', '', '密码', '用户创建时的默认密码', '1', '0');
INSERT INTO `alone_parameter` VALUES ('40289f787887793e017887836a160000', '202103311700', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.sys.title', '1', 'config.sys.title', 'WSCH', '', '文字标记/通用配置', '系统标题', '1', '0');
INSERT INTO `alone_parameter` VALUES ('2c948a837f2e8609017f2e8776f60005', '202202250938', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wdas.able', '1', 'config.wdas.able', 'false', '', 'WDAS', 'wdas服务启用状态', '1', '0');
INSERT INTO `alone_parameter` VALUES ('2c948a837f2e8609017f2e87771e0006', '202202250938', '202202251213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'config.wuc.sysid', '1', 'config.wuc.sysid', '2c94', '', 'WUC配置', '子系统ID', '1', '0');

-- ----------------------------
-- Table structure for `alone_parameter_local`
-- ----------------------------
DROP TABLE IF EXISTS `alone_parameter_local`;
CREATE TABLE `alone_parameter_local` (
  `ID` varchar(32) NOT NULL,
  `PARAMETERID` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PVALUE` varchar(2048) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_50` (`PARAMETERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_parameter_local
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_qz_scheduler`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_scheduler`;
CREATE TABLE `farm_qz_scheduler` (
  `ID` varchar(32) NOT NULL,
  `AUTOIS` varchar(2) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `TASKID` varchar(32) NOT NULL,
  `TRIGGERID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_18` (`TASKID`),
  KEY `FK_Reference_19` (`TRIGGERID`),
  CONSTRAINT `FK_Reference_18` FOREIGN KEY (`TASKID`) REFERENCES `farm_qz_task` (`ID`),
  CONSTRAINT `FK_Reference_19` FOREIGN KEY (`TRIGGERID`) REFERENCES `farm_qz_trigger` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_scheduler
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_qz_task`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_task`;
CREATE TABLE `farm_qz_task` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `JOBCLASS` varchar(128) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `JOBPARAS` varchar(1024) DEFAULT NULL,
  `JOBKEY` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_task
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_qz_trigger`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_trigger`;
CREATE TABLE `farm_qz_trigger` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `DESCRIPT` varchar(128) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_trigger
-- ----------------------------

-- ----------------------------
-- Table structure for `wlp_f_file`
-- ----------------------------
DROP TABLE IF EXISTS `wlp_f_file`;
CREATE TABLE `wlp_f_file` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(1) NOT NULL COMMENT '0.临时1.持久2永久9删除',
  `EXNAME` varchar(32) NOT NULL,
  `RELATIVEPATH` varchar(1024) NOT NULL COMMENT 'relative',
  `SECRET` varchar(32) NOT NULL COMMENT '200m以下為MD5校驗碼',
  `FILENAME` varchar(128) NOT NULL,
  `TITLE` varchar(256) NOT NULL,
  `FILESIZE` int(11) NOT NULL,
  `SYSNAME` varchar(128) NOT NULL,
  `RESOURCEKEY` varchar(32) NOT NULL,
  `APPID` varchar(32) DEFAULT NULL,
  `LOADNUM` int(11) DEFAULT NULL,
  `LOADSTATE` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wlp_f_file
-- ----------------------------

-- ----------------------------
-- Table structure for `wsch_a_popgourp`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_a_popgourp`;
CREATE TABLE `wsch_a_popgourp` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `GROUPNAME` varchar(64) NOT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_a_popgourp
-- ----------------------------
INSERT INTO `wsch_a_popgourp` VALUES ('40289ff48002452e0180031b9add000b', '20220407161942', '默认', '40289ff48002452e018002a4f31c0000', '', '1');

-- ----------------------------
-- Table structure for `wsch_a_popgourpkey`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_a_popgourpkey`;
CREATE TABLE `wsch_a_popgourpkey` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `GROUPID` varchar(32) NOT NULL,
  `POPKEYID` varchar(64) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WSCH_A_P_REFERENCE_WSCH_KEY` (`GROUPID`),
  CONSTRAINT `FK_WSCH_A_P_REFERENCE_WSCH_KEY` FOREIGN KEY (`GROUPID`) REFERENCES `wsch_a_popgourp` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_a_popgourpkey
-- ----------------------------
INSERT INTO `wsch_a_popgourpkey` VALUES ('40289ff48002452e0180031bca42000c', '20220407161954', null, '40289ff48002452e0180031b9add000b', 'ALLABLE', '1');

-- ----------------------------
-- Table structure for `wsch_a_popuser`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_a_popuser`;
CREATE TABLE `wsch_a_popuser` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `GROUPID` varchar(64) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WSCH_A_P_REFERENCE_WSCH_USER` (`GROUPID`),
  CONSTRAINT `FK_WSCH_A_P_REFERENCE_WSCH_USER` FOREIGN KEY (`GROUPID`) REFERENCES `wsch_a_popgourp` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_a_popuser
-- ----------------------------
INSERT INTO `wsch_a_popuser` VALUES ('40289ff48002452e0180031bf455000d', '20220407162005', null, '40289ff48002452e018002a4f31c0000', '40289ff48002452e0180031b9add000b', 'ANONYMOUS', '1');

-- ----------------------------
-- Table structure for `wsch_a_weburl`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_a_weburl`;
CREATE TABLE `wsch_a_weburl` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `URL` varchar(512) NOT NULL,
  `WEBNAME` varchar(64) NOT NULL,
  `SORT` int(11) NOT NULL,
  `FILEID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_a_weburl
-- ----------------------------

-- ----------------------------
-- Table structure for `wsch_p_appdic`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_p_appdic`;
CREATE TABLE `wsch_p_appdic` (
  `ID` varchar(32) NOT NULL,
  `APPID` varchar(32) NOT NULL,
  `DICID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WSCH_P_A_REFERENCE_WSCH_P_A` (`APPID`),
  KEY `FK_WSCH_P_A_REFERENCE_WSCH_P_D` (`DICID`),
  CONSTRAINT `FK_WSCH_P_A_REFERENCE_WSCH_P_A` FOREIGN KEY (`APPID`) REFERENCES `wsch_p_appmodel` (`ID`),
  CONSTRAINT `FK_WSCH_P_A_REFERENCE_WSCH_P_D` FOREIGN KEY (`DICID`) REFERENCES `wsch_p_dictype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_p_appdic
-- ----------------------------

-- ----------------------------
-- Table structure for `wsch_p_appmodel`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_p_appmodel`;
CREATE TABLE `wsch_p_appmodel` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(256) NOT NULL,
  `KEYID` varchar(64) NOT NULL,
  `SORT` int(11) NOT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WSCH_P_A_REFERENCE_WSCH_P_P` (`PROJECTID`),
  CONSTRAINT `FK_WSCH_P_A_REFERENCE_WSCH_P_P` FOREIGN KEY (`PROJECTID`) REFERENCES `wsch_p_project` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_p_appmodel
-- ----------------------------
INSERT INTO `wsch_p_appmodel` VALUES ('40289ff48002452e018002a572110004', '默认', 'DEFAULT', '1', '40289ff48002452e018002a4f31c0000');

-- ----------------------------
-- Table structure for `wsch_p_dictype`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_p_dictype`;
CREATE TABLE `wsch_p_dictype` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `NAME` varchar(32) NOT NULL,
  `KEYID` varchar(32) NOT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `FIELDKEY` varchar(128) DEFAULT NULL,
  `LIKETYPE` varchar(2) DEFAULT NULL,
  `SUPERID` varchar(32) NOT NULL,
  `ABLETYPE` varchar(2) DEFAULT NULL,
  `SUMABLE` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WSCH_P_D_REFERENCE_P_P` (`PROJECTID`),
  CONSTRAINT `FK_WSCH_P_D_REFERENCE_P_P` FOREIGN KEY (`PROJECTID`) REFERENCES `wsch_p_project` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_p_dictype
-- ----------------------------

-- ----------------------------
-- Table structure for `wsch_p_doctext`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_p_doctext`;
CREATE TABLE `wsch_p_doctext` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `TEXT` text NOT NULL,
  `OTEXT` text,
  `PROJECTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_p_doctext
-- ----------------------------
INSERT INTO `wsch_p_doctext` VALUES ('40289ff48003570a0180036079b10004', '20220407173455', '系统管理员', '1', null, '{\"APPID\":\"1\",\"APPKEYID\":\"DEFAULT\",\"CONTENT\":\"test\",\"POPKEYS\":\"1\",\"TITLE\":\"test\"}', '{\"APPID\":\"1\",\"APPKEYID\":\"DEFAULT\",\"CONTENT\":\"test\",\"POPKEYS\":\"1\",\"TITLE\":\"test\"}', '40289ff48002452e018002a4f31c0000');

-- ----------------------------
-- Table structure for `wsch_p_document`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_p_document`;
CREATE TABLE `wsch_p_document` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `APPKEYID` varchar(64) NOT NULL,
  `TASKKEY` varchar(32) NOT NULL,
  `APPID` varchar(64) NOT NULL,
  `TEXTID` varchar(32) NOT NULL,
  `POPKEYS` varchar(512) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WSCH_P_D_REFERENCE_WSCH_P_P` (`PROJECTID`),
  KEY `FK_WSCH_P_D_REFERENCE_TEXT` (`TEXTID`),
  CONSTRAINT `FK_WSCH_P_D_REFERENCE_TEXT` FOREIGN KEY (`TEXTID`) REFERENCES `wsch_p_doctext` (`ID`),
  CONSTRAINT `FK_WSCH_P_D_REFERENCE_WSCH_P_P` FOREIGN KEY (`PROJECTID`) REFERENCES `wsch_p_project` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_p_document
-- ----------------------------
INSERT INTO `wsch_p_document` VALUES ('40289ff48003570a0180036079b10005', '20220407173455', '40288b854a329988014a329a12f30002', null, '3', '40289ff48002452e018002a4f31c0000', 'DEFAULT', '0ab43363afff470ba16dc8df3e84a00e', '1', '40289ff48003570a0180036079b10004', '1');

-- ----------------------------
-- Table structure for `wsch_p_field`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_p_field`;
CREATE TABLE `wsch_p_field` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `NAME` varchar(128) NOT NULL,
  `KEYID` varchar(128) NOT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `TYPE` varchar(32) NOT NULL,
  `STOREIS` varchar(1) NOT NULL,
  `SORT` int(11) NOT NULL,
  `SEARCHTYPE` varchar(2) NOT NULL,
  `BOOST` int(11) DEFAULT NULL,
  `TEXTTYPE` varchar(2) NOT NULL,
  `MAXLENGHT` int(11) DEFAULT NULL,
  `SORTABLE` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WSCH_P_F_REFERENCE_WSCH_P_P` (`PROJECTID`),
  CONSTRAINT `FK_WSCH_P_F_REFERENCE_WSCH_P_P` FOREIGN KEY (`PROJECTID`) REFERENCES `wsch_p_project` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_p_field
-- ----------------------------
INSERT INTO `wsch_p_field` VALUES ('40289ff48002452e018002a4f32c0001', '20220407141006', '40288b854a329988014a329a12f30002', '1', null, '文档ID', 'APPID', '40289ff48002452e018002a4f31c0000', 'StringField', '1', '1', '0', null, '1', null, '0');
INSERT INTO `wsch_p_field` VALUES ('40289ff48002452e018002a4f32c0002', '20220407141006', '40288b854a329988014a329a12f30002', '1', null, '业务KEYID', 'APPKEYID', '40289ff48002452e018002a4f31c0000', 'StringField', '1', '2', '0', null, '1', null, '0');
INSERT INTO `wsch_p_field` VALUES ('40289ff48002452e018002a4f32c0003', '20220407141006', '40288b854a329988014a329a12f30002', '1', null, '权限KEY', 'POPKEYS', '40289ff48002452e018002a4f31c0000', 'StringField', '1', '3', '0', null, '1', null, '0');
INSERT INTO `wsch_p_field` VALUES ('40289ff48003570a0180035da0c40000', '20220407173149', '40288b854a329988014a329a12f30002', '1', '', '标题', 'TITLE', '40289ff48002452e018002a4f31c0000', 'TextField', '1', '1', '1', null, '1', null, '1');
INSERT INTO `wsch_p_field` VALUES ('40289ff48003570a0180035e2a4f0001', '20220407173224', '40288b854a329988014a329a12f30002', '1', '', '正文', 'CONTENT', '40289ff48002452e018002a4f31c0000', 'TextField', '1', '2', '1', null, '1', null, '0');

-- ----------------------------
-- Table structure for `wsch_p_project`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_p_project`;
CREATE TABLE `wsch_p_project` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `NAME` varchar(128) NOT NULL,
  `KEYID` varchar(128) NOT NULL,
  `SORT` int(11) NOT NULL,
  `MINIMGID` varchar(32) DEFAULT NULL,
  `MAXIMGID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_p_project
-- ----------------------------
INSERT INTO `wsch_p_project` VALUES ('40289ff48002452e018002a4f31c0000', '20220407141006', '40288b854a329988014a329a12f30002', '2', '', '检索演示', 'TEST', '1', null, null);

-- ----------------------------
-- Table structure for `wsch_p_virtualfield`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_p_virtualfield`;
CREATE TABLE `wsch_p_virtualfield` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `FORMULA` varchar(1024) NOT NULL,
  `TYPE` varchar(64) NOT NULL,
  `APPMODELID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WSCH_P_V_REFERENCE_WSCH_P_A` (`APPMODELID`),
  CONSTRAINT `FK_WSCH_P_V_REFERENCE_WSCH_P_A` FOREIGN KEY (`APPMODELID`) REFERENCES `wsch_p_appmodel` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_p_virtualfield
-- ----------------------------
INSERT INTO `wsch_p_virtualfield` VALUES ('40289ff48002452e018002a6c76e0006', '标题', '$[TITLE_HL]', 'TITLE', '40289ff48002452e018002a572110004');
INSERT INTO `wsch_p_virtualfield` VALUES ('40289ff48003570a0180035ecc900002', '内容', '$[CONTENT_HL]', 'CONTENT', '40289ff48002452e018002a572110004');

-- ----------------------------
-- Table structure for `wsch_r_search`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_r_search`;
CREATE TABLE `wsch_r_search` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) DEFAULT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `SIZE` int(11) DEFAULT NULL,
  `WORD` varchar(512) DEFAULT NULL,
  `RULES` varchar(2048) DEFAULT NULL,
  `TIMES` varchar(2048) DEFAULT NULL,
  `SEARCHTIME` float DEFAULT NULL,
  `RESULTID` varchar(64) DEFAULT NULL,
  `PROJECTID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_r_search
-- ----------------------------
INSERT INTO `wsch_r_search` VALUES ('40289ff48002452e0180031c43cf000e', '20220407162025', '40288b854a329988014a329a12f30002', '1', '0', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"TITLE\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":1,\"runtime\":0.015,\"title\":\"索引检索\"},{\"resultNum\":0,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":0,\"runtime\":0.001,\"title\":\"数据装载\"}]', '0.015', '0fbbaa5c-e4db-4338-9cf7-20b87ccb7093', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff48002452e0180031cb28e000f', '20220407162054', '40288b854a329988014a329a12f30002', '1', '0', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"TITLE\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":1,\"runtime\":0.001,\"title\":\"索引检索\"},{\"resultNum\":0,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":0,\"runtime\":0.001,\"title\":\"数据装载\"}]', '0.016', 'f3d97614-d137-4bb7-9805-6c3535ae999e', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff48002452e0180031ddb9e0010', '20220407162210', null, '1', '1', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"TITLE\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":1,\"runtime\":0.001,\"title\":\"索引检索\"},{\"resultNum\":1,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":1,\"runtime\":0.031,\"title\":\"数据装载\"}]', '0.031', 'c45d4150-2e1c-4bcb-aea0-15672f23edc7', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff4800330e3018003349a8e0000', '20220407164700', '40288b854a329988014a329a12f30002', '1', '0', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"TITLE\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":1,\"runtime\":0.017,\"title\":\"索引检索\"},{\"resultNum\":0,\"runtime\":0.007,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":0,\"runtime\":0.001,\"title\":\"数据装载\"}]', '0.37', '263dcfba-9daf-4cb8-a69c-761b6a8251d2', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff4800330e30180033fefbc0001', '20220407165923', '40288b854a329988014a329a12f30002', '1', '1', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"TITLE\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":1,\"runtime\":0.001,\"title\":\"索引检索\"},{\"resultNum\":1,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":1,\"runtime\":0.038,\"title\":\"数据装载\"}]', '0.041', '62ce91d4-3a4d-4d56-a31c-6c3e1f46570b', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff4800330e30180033ff4b10002', '20220407165924', '40288b854a329988014a329a12f30002', '1', '1', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"TITLE\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":1,\"runtime\":0.001,\"title\":\"索引检索\"},{\"resultNum\":1,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":1,\"runtime\":0.004,\"title\":\"数据装载\"}]', '0.006', 'a1b40de3-b14e-4c01-80e5-7c4acdfbf1a7', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff48003570a0180035fed5f0003', '20220407173419', '40288b854a329988014a329a12f30002', '1', '0', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"标题\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"},{\"fieldKey\":\"CONTENT\",\"fieldTitle\":\"正文\",\"newGroup\":false,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":0,\"runtime\":0.011,\"title\":\"索引检索\"},{\"resultNum\":0,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":0,\"runtime\":0.001,\"title\":\"数据装载\"}]', '0.011', '5d4c6566-b867-432c-94bf-3671a33355f3', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff48003570a0180036089330006', '20220407173459', '40288b854a329988014a329a12f30002', '1', '1', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"标题\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"},{\"fieldKey\":\"CONTENT\",\"fieldTitle\":\"正文\",\"newGroup\":false,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":1,\"runtime\":0.015,\"title\":\"索引检索\"},{\"resultNum\":1,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":1,\"runtime\":0.032,\"title\":\"数据装载\"}]', '0.047', '51a27200-f904-4d73-9037-f852e411eb17', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff48003570a01800360c52a0007', '20220407173515', '40288b854a329988014a329a12f30002', '1', '1', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"标题\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"},{\"fieldKey\":\"CONTENT\",\"fieldTitle\":\"正文\",\"newGroup\":false,\"searchFlag\":\"likex1.0\",\"val\":\"test\"},{\"fieldKey\":\"APPKEYID\",\"fieldTitle\":\"业务KEYID\",\"newGroup\":true,\"searchFlag\":\"=\",\"val\":\"DEFAULT\"}]', '[{\"resultNum\":1,\"runtime\":0.001,\"title\":\"索引检索\"},{\"resultNum\":1,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":1,\"runtime\":0.001,\"title\":\"数据装载\"}]', '0.001', '3279c93e-07be-4663-9b39-d0c7194fb43d', '40289ff48002452e018002a4f31c0000');
INSERT INTO `wsch_r_search` VALUES ('40289ff48003570a01800360cc0a0008', '20220407173517', '40288b854a329988014a329a12f30002', '1', '1', 'test', '[{\"fieldKey\":\"TITLE\",\"fieldTitle\":\"标题\",\"newGroup\":true,\"searchFlag\":\"likex1.0\",\"val\":\"test\"},{\"fieldKey\":\"CONTENT\",\"fieldTitle\":\"正文\",\"newGroup\":false,\"searchFlag\":\"likex1.0\",\"val\":\"test\"}]', '[{\"resultNum\":1,\"runtime\":0.001,\"title\":\"索引检索\"},{\"resultNum\":1,\"runtime\":0.001,\"title\":\"权限过滤/分布统计\"},{\"resultNum\":1,\"runtime\":0.001,\"title\":\"数据装载\"}]', '0.001', '1f1e5914-583b-406c-bf93-7d148b9c8ba5', '40289ff48002452e018002a4f31c0000');

-- ----------------------------
-- Table structure for `wsch_r_visit`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_r_visit`;
CREATE TABLE `wsch_r_visit` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) DEFAULT NULL,
  `APPID` varchar(64) NOT NULL,
  `RESULTID` varchar(64) NOT NULL,
  `TITLE` varchar(512) DEFAULT NULL,
  `OLINK` varchar(512) DEFAULT NULL,
  `IMG` varchar(512) DEFAULT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_r_visit
-- ----------------------------

-- ----------------------------
-- Table structure for `wsch_s_wordex`
-- ----------------------------
DROP TABLE IF EXISTS `wsch_s_wordex`;
CREATE TABLE `wsch_s_wordex` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `WORD` varchar(64) NOT NULL,
  `TYPE` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wsch_s_wordex
-- ----------------------------
